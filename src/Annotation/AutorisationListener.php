<?php
namespace App\Annotation;
/**
 * Created by PhpStorm.
 * User: Snowy
 * Date: 05/02/2019
 * Time: 12:16
 */

use App\Controller\UtilisateurController;
use App\Entity\Utilisateur;
use App\EventListener\ExceptionListener;
use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use ReflectionObject;
use Symfony\Component\HttpFoundation\JsonResponse;

class AutorisationListener
{

    private $reader;
    private $tokenStorage;
    private $em;

    public function __construct(Reader $reader, TokenStorageInterface $tokenStorage)
    {
        $this->reader = $reader;
        $this->tokenStorage = $tokenStorage;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        $controller = $event->getController();
        if (!is_array($controller)) {
            return;
        }
        list($controllerObject, $methodName) = $controller;
        $controllerReflectionObject = new ReflectionObject($controllerObject);
        $reflectionMethod = $controllerReflectionObject->getMethod($methodName);
        $methodAnnotation = $this->reader->getMethodAnnotation($reflectionMethod, 'App\Annotation\Autorisation');
        if (!$methodAnnotation)
            return;

        $autorisationName = $methodAnnotation->autorisation;
        $user = $this->tokenStorage->getToken()->getUser();
        $hasRight  = false;
        foreach($user->getUtilRoles() as $role)
        {
            foreach ($role->getAutorisations() as $autorisation)
            {
                if ($autorisationName == $autorisation->getAutoNom())
                {
                    $hasRight = true;
                    break;
                }
            }
        }
        if (!$hasRight)
        {
            $message = 'User ' . $user->getUtilPseudo() . ' has no right for autorisation ' . $autorisationName;
            return $event->setController(function () use ($message) {
                $data['message'] = $message;
                return new JsonResponse(json_encode($data),Response::HTTP_FORBIDDEN, [], true);
            });
        }
    }
}