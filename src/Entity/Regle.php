<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\INormalizable as EntityNormalizable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Regle
 *
 * @ORM\Table(name="REGLE")
 * @ORM\Entity
 */
class Regle implements EntityNormalizable
{
    const NB_POINTS_BONUS = "bonus";
    const NB_POINTS_MIN_BADGE = "pointsMinimumBadge";
    /**
     * @var int
     * @Groups({"Regle", "Organisation"})
     *
     * @Assert\Type(
     *     type="int",
     *     message="The rule id : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="REGL_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $reglId;

    /**
     * @var string
     * @Groups({"Regle", "Organisation"})
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Type(
     *     type="string",
     *     message="The rule name {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 40,
     *      minMessage = "The rule name is too short, at least {{ limit }} characters.",
     *      maxMessage = "The rule name is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="REGL_NOM", type="string", length=40, nullable=false)
     */
    private $reglNom;

    /**
     * @var string
     * @Groups("Regle")
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Type(
     *     type="string",
     *     message="The rule value {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 256,
     *      minMessage = "The rule value is too short, at least {{ limit }} characters.",
     *      maxMessage = "The rule value is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="REGL_VALEUR", type="string", length=256, nullable=false)
     */
    private $reglValeur;

    /**
     * @var string
     * @Groups("Regle")
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Type(
     *     type="string",
     *     message="The rule type {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 1,
     *      minMessage = "The rule type is too short, at least {{ limit }} characters.",
     *      maxMessage = "The rule type is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="REGL_TYPE", type="string", length=1, nullable=false, options={"fixed"=true})
     */
    private $reglType;

    /**
     * @var array
     *
     * @Groups("Regle")
     *
     * @ORM\ManyToMany(targetEntity="Organisation", inversedBy="regles")
     * @ORM\JoinTable(name="DEFINIR_REGLE",
     *   joinColumns={
     *     @ORM\JoinColumn(name="REGL_ID", referencedColumnName="REGL_ID")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ORGA_ID", referencedColumnName="ORGA_ID")
     *   }
     * )
     */
    private $organisations;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->organisations = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getReglId(): int
    {
        return $this->reglId;
    }

    /**
     * @param int $reglId
     */
    public function setReglId(int $reglId): void
    {
        $this->reglId = $reglId;
    }

    /**
     * @return string
     */
    public function getReglNom(): string
    {
        return $this->reglNom;
    }

    /**
     * @param string $reglNom
     */
    public function setReglNom(string $reglNom): void
    {
        $this->reglNom = $reglNom;
    }

    /**
     * @return string
     */
    public function getReglValeur(): string
    {
        return $this->reglValeur;
    }

    /**
     * @param string $reglValeur
     */
    public function setReglValeur($reglValeur): void
    {
        $this->reglValeur = $reglValeur;
    }

    /**
     * @return string
     */
    public function getReglType(): string
    {
        return $this->reglType;
    }

    /**
     * @param string $reglType
     */
    public function setReglType(string $reglType): void
    {
        $this->reglType = $reglType;
    }

    /**
     * @return array
     */
    public function getOrganisations()
    {
        return $this->organisations;
    }

    /**
     * @param array $orga
     */
    public function setOrganisations(array $organisations): void
    {
        $this->organisations = $organisations;
    }

    public function addOrganisation($organisation): void
    {
        if(!$this->organisations->contains($organisation))
            $this->organisations->add($organisation);
    }

    public function removeOrganisation($organisation): void
    {
        if($this->organisations->contains($organisation))
            $this->organisations->removeElement($organisation);
    }

    public static function getGroup(): string
    {
        return 'Regle';
    }
}
