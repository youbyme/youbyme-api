<?php
/**
 * Created by PhpStorm.
 * User: I341379
 * Date: 17/01/2019
 * Time: 11:33
 */

namespace App\Entity;
use App\Util\Constants;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * @ORM\Table(name="POSSEDER_BADGE")
 * @ORM\Entity(repositoryClass="App\Repository\BadgeAssignationRepository")
 */
class BadgeAssignation implements INormalizable
{

    /**
     * @Groups("BadgeAssignation")
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Utilisateur", inversedBy="badges")
     * @ORM\JoinColumn(name="utilisateur_id", referencedColumnName="UTIL_ID", nullable=false)
     */
    private $utilisateur;

    /**
     * @Groups("BadgeAssignation")
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Badge", inversedBy="attributionsUtilisateur", fetch="EAGER")
     * @ORM\JoinColumn(name="badge_id", referencedColumnName="BADG_ID", nullable=false)
     */
    private $badge;

    /**
     * @Groups("BadgeAssignation")
     *
     * @ORM\Column(name="DATE", type="date", nullable=false)
     */
    private $date;

    /**
     * @Groups("BadgeAssignation")
     *
     * @ORM\Column(name="VALIDE", type="boolean", nullable=true)
     *
     * false par défaut jusqu'à l'approbation d'un utilisateur autorisé
     */
    private $valide = false;


    public function getUtilisateur()
    {
        return $this->utilisateur;
    }


    public function setUtilisateur(Utilisateur $utilisateur): void
    {
        $this->utilisateur = $utilisateur;
    }


    public function getBadge()
    {
        return $this->badge;
    }


    public function setBadge(Badge $badge): void
    {
        $this->badge = $badge;
    }


    public function getDate()
    {
        return $this->date->format(Constants::$DATE_FORMAT);
    }


    public function setDate($stringDate): void
    {
        $this->date = new \DateTime($stringDate);
    }


    public function isValide()
    {
        return $this->valide;
    }

    public function setValide(bool $valide): void
    {
        $this->valide = $valide;
    }

    public static function getGroup(): string
    {
        return 'BadgeAssignation';
    }
}