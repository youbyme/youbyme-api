<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Entity\INormalizable as EntityNormalizable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Util\Constants;

/**
 * Organisation
 *
 * @ORM\Table(name="ORGANISATION", indexes={@ORM\Index(name="FK_APPARTENIR_ORGANISATION", columns={"ORG_ORGA_ID"}), @ORM\Index(name="FK_DEFINIR_TYPE", columns={"TYPEORGA_ID"})})
 * @ORM\Entity
 */
class Organisation implements EntityNormalizable
{
    /**
     * @var int
     * @Groups({"Utilisateur", "Competence", "Organisation", "Regle", "HistoriquePeriode"})
     *
     * @Assert\Type(
     *     type="int",
     *     message="The organization id : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="ORGA_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $orgaId;

    /**
     * @var string
     * @Groups({"Utilisateur", "Organisation", "Competence", "Regle", "HistoriquePeriode"})
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Type(
     *     type="string",
     *     message="The organization name {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 40,
     *      minMessage = "The organization name is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization name is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ORGA_NOM", type="string", length=40, nullable=false)
     */
    private $orgaNom;

    /**
     * @var string|null
     * @Groups({"Utilisateur", "Organisation", "Competence", "Regle", "Vote", "HistoriquePeriode"})
     *

     * @Assert\Type(
     *     type="string",
     *     message="The organization code analytics {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 40,
     *      minMessage = "The organization code analytics is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization code analytics is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ORGA_CODE_ANALYTICS", type="string", length=40, nullable=true)
     */
    private $orgaCodeAnalytics;

    /**
     * @var string|null
     * @Groups("Organisation")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The organization street {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 40,
     *      minMessage = "The organization street is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization street is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ORGA_ADRE_RUE", type="string", length=40, nullable=true)
     */
    private $orgaAdreRue;

    /**
     * @var string|null
     * @Groups("Organisation")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The organization street number {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 40,
     *      minMessage = "The organization street number is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization street number is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ORGA_ADRE_NUMERO", type="string", length=10, nullable=true)
     */
    private $orgaAdreNumero;

    /**
     * @var string|null
     * @Groups("Organisation")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The organization additional address {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 40,
     *      minMessage = "The organization additional address is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization additional address is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ORGA_ADRE_COMPLEMENT", type="string", length=40, nullable=true)
     */
    private $orgaAdreComplement;

    /**
     * @var string|null
     * @Groups("Organisation")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The organization city {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 40,
     *      minMessage = "The organization city is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization city is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ORGA_ADRE_VILLE", type="string", length=40, nullable=true)
     */
    private $orgaAdreVille;

    /**
     * @var string|null
     * @Groups("Organisation")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The organization zip code {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 6,
     *      minMessage = "The organization zip code is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization zip code is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ORGA_ADRE_CP", type="string", length=6, nullable=true)
     */
    private $orgaAdreCp;

    /**
     * @var string|null
     * @Groups("Organisation")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The organization country {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 40,
     *      minMessage = "The organization country is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization country is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ORGA_ADRE_PAYS", type="string", length=40, nullable=true)
     */
    private $orgaAdrePays;

    /**
     * @var string|null
     * @Groups("Organisation")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The organization phone number {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 10,
     *      minMessage = "The organization phone number is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization phone number is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ORGA_TEL", type="string", length=10, nullable=true)
     */
    private $orgaTel;

    /**
     * @var string|null
     * @Groups("Organisation")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The organization email {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 40,
     *      minMessage = "The organization email is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization email is too long, at most {{ limit }} characters."
     * )
     *
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     *
     * @ORM\Column(name="ORGA_MAIL", type="string", length=40, nullable=true)
     */
    private $orgaMail;

    /**
     * @var string|null
     * @Groups("Organisation")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The organization siret {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 14,
     *      minMessage = "The organization siret is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization siret is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ORGA_SIRET", type="string", length=14, nullable=true)
     */
    private $orgaSiret;

    /**
     * @var Organisation
     * @Groups("Organisation")
     *
     *
     * @ORM\ManyToOne(targetEntity="Organisation", inversedBy="orgaEnfants")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ORG_ORGA_ID", referencedColumnName="ORGA_ID", nullable=true)
     * })
     */
    private $orgaParent;

    /**
     * @Groups("Organisation")
     * @ORM\OneToMany(targetEntity="Organisation", mappedBy="orgaParent")
     */
    private $orgaEnfants;

    /**
     * @var TypeOrga
     * @Groups("Organisation")
     *
     *
     * @ORM\ManyToOne(targetEntity="TypeOrga")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="TYPEORGA_ID", referencedColumnName="TYPEORGA_ID", nullable=false)
     * })
     */
    private $typeOrga;

    /**
     * @var array
     * @Groups("Organisation")
     *
     *
     * @ORM\ManyToMany(targetEntity="Regle", mappedBy="organisations")
     */
    private $regles;

    /**
     * @var array
     * @Groups("Organisation")
     *
     * @ORM\ManyToMany(targetEntity="Competence", mappedBy="organisations")
     */
    private $competences;

    /**
     * @Groups("Organisation")
     *
     * @ORM\OneToMany(targetEntity="Utilisateur", mappedBy="organisation")
     */
    private $utilisateurs;

    /**
     * @var array
     * @Groups("Organisation")
     *
     * @ORM\ManyToMany(targetEntity="Utilisateur", mappedBy="organisationsGeres")
     */
    private $administrateurs;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ORGA_DATE_RESET", type="date", nullable=false)
     */
    private $dernierReset;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HistoriquePeriode", mappedBy="organisation", cascade={"persist", "remove"})
     */
    private $historique;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->regles = new ArrayCollection();
        $this->competences = new ArrayCollection();
        $this->utilisateurs = new ArrayCollection();
        $this->administrateurs = new ArrayCollection();
        $this->orgaEnfants = new ArrayCollection();
        $this->historique = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getOrgaId(): int
    {
        return $this->orgaId;
    }

    /**
     * @param int $orgaId
     */
    public function setOrgaId(int $orgaId): void
    {
        $this->orgaId = $orgaId;
    }

    /**
     * @return string
     */
    public function getOrgaNom(): string
    {
        return $this->orgaNom;
    }

    /**
     * @param string $orgaNom
     */
    public function setOrgaNom(string $orgaNom): void
    {
        $this->orgaNom = $orgaNom;
    }

    /**
     * @return string
     */
    public function getOrgaCodeAnalytics()
    {
        return $this->orgaCodeAnalytics;
    }

    /**
     * @param string $orgaCodeAnalytics
     */
    public function setOrgaCodeAnalytics($orgaCodeAnalytics): void
    {
        $this->orgaCodeAnalytics = $orgaCodeAnalytics;
    }

    /**
     * @return null|string
     */
    public function getOrgaAdreRue(): ?string
    {
        return $this->orgaAdreRue;
    }

    /**
     * @param null|string $orgaAdreRue
     */
    public function setOrgaAdreRue(?string $orgaAdreRue): void
    {
        $this->orgaAdreRue = $orgaAdreRue;
    }

    /**
     * @return null|string
     */
    public function getOrgaAdreNumero(): ?string
    {
        return $this->orgaAdreNumero;
    }

    /**
     * @param null|string $orgaAdreNumero
     */
    public function setOrgaAdreNumero(?string $orgaAdreNumero): void
    {
        $this->orgaAdreNumero = $orgaAdreNumero;
    }

    /**
     * @return null|string
     */
    public function getOrgaAdreComplement(): ?string
    {
        return $this->orgaAdreComplement;
    }

    /**
     * @param null|string $orgaAdreComplement
     */
    public function setOrgaAdreComplement(?string $orgaAdreComplement): void
    {
        $this->orgaAdreComplement = $orgaAdreComplement;
    }

    /**
     * @return null|string
     */
    public function getOrgaAdreVille(): ?string
    {
        return $this->orgaAdreVille;
    }

    /**
     * @param null|string $orgaAdreVille
     */
    public function setOrgaAdreVille(?string $orgaAdreVille): void
    {
        $this->orgaAdreVille = $orgaAdreVille;
    }

    /**
     * @return null|string
     */
    public function getOrgaAdreCp(): ?string
    {
        return $this->orgaAdreCp;
    }

    /**
     * @param null|string $orgaAdreCp
     */
    public function setOrgaAdreCp(?string $orgaAdreCp): void
    {
        $this->orgaAdreCp = $orgaAdreCp;
    }

    /**
     * @return null|string
     */
    public function getOrgaAdrePays(): ?string
    {
        return $this->orgaAdrePays;
    }

    /**
     * @param null|string $orgaAdrePays
     */
    public function setOrgaAdrePays(?string $orgaAdrePays): void
    {
        $this->orgaAdrePays = $orgaAdrePays;
    }

    /**
     * @return null|string
     */
    public function getOrgaTel(): ?string
    {
        return $this->orgaTel;
    }

    /**
     * @param null|string $orgaTel
     */
    public function setOrgaTel(?string $orgaTel): void
    {
        if (isset($orgaTel))
        {
            $orgaTel = preg_replace("/[^0-9]/", "", $orgaTel);
        }

        $this->orgaTel = $orgaTel;
    }

    /**
     * @return null|string
     */
    public function getOrgaMail(): ?string
    {
        return $this->orgaMail;
    }

    /**
     * @param null|string $orgaMail
     */
    public function setOrgaMail(?string $orgaMail): void
    {
        $this->orgaMail = $orgaMail;
    }

    /**
     * @return null|string
     */
    public function getOrgaSiret(): ?string
    {
        return $this->orgaSiret;
    }

    /**
     * @param null|string $orgaSiret
     */
    public function setOrgaSiret(?string $orgaSiret): void
    {
        $this->orgaSiret = $orgaSiret;
    }

    /**
     * @return ?Organisation
     */
    public function getOrgaParent(): ?Organisation
    {
        return $this->orgaParent;
    }

    /**
     * @param ?Organisation $orgaParent
     */
    public function setOrgaParent(?Organisation $orgaParent): void
    {
        $this->orgaParent = $orgaParent;
    }

    /**
     * @return Typeorga
     */
    public function getTypeOrga(): TypeOrga
    {
        return $this->typeOrga;
    }

    /**
     * @param Typeorga $typeorga
     */
    public function setTypeOrga(TypeOrga $typeOrga): void
    {
        $this->typeOrga = $typeOrga;
    }

    /**
     * @return ArrayCollection
     */
    public function getRegles()
    {
        return $this->regles;
    }

    public function removeRegles($regles): void
    {
        if($this->regles->contains($regles))
            $this->regles->removeElement($regles);
    }

    /**
     * @param ArrayCollection $regles
     */
    public function setRegles(array $regles): void
    {
        $this->regles = new ArrayCollection($regles);
    }

    public function addRegle(Regle $regle)
    {
        if (!$this->regles->contains($regle))
            $this->regles->add($regle);
    }

    public function getRegle($ruleName)
    {
        $ruleReturned = null;
        foreach ($this->regles as $rule)
        {
            if ($rule->getReglNom() == $ruleName)
            {
                $ruleReturned = $rule;
                break;
            }
        }

        return ($ruleReturned !== null ? $ruleReturned->getReglValeur() : $ruleReturned);
    }

    /**
     * @return ArrayCollection
     */
    public function getCompetences()
    {
        return $this->competences;
    }

    /**
     * @param ArrayCollection $comp
     */
    public function setCompetences(array $competences): void
    {
        $this->competences = new ArrayCollection($competences);
    }

    public function addCompetence(Competence $competence)
    {
        if (!$this->competences->contains($competence))
            $this->competences->add($competence);
    }

    /**
     * @return ArrayCollection
     */
    public function getUtilisateurs()
    {
        return $this->utilisateurs;
    }

    /**
     * @param ArrayCollection $util
     */
    public function setUtilisateurs(array $utilisateurs): void
    {
        $this->utilisateurs = $utilisateurs;
    }

    public function getAdministrateurs()
    {
        return $this->administrateurs;
    }

    public function setAdministrateurs(array $administrateurs)
    {
        $this->administrateurs = $administrateurs;
    }

    public function addAdministrateur($admin)
    {
        if (!$this->administrateurs->contains($admin))
            $this->administrateurs->add($admin);
    }

    public function removeAdministrateur($admin)
    {
        if ($this->administrateurs->contains($admin))
            $this->administrateurs->removeElement($admin);
    }

    public function getOrgaEnfants()
    {
        return $this->orgaEnfants;
    }

    public function setOrgaEnfants($orgas)
    {
        //not relevant
    }

    public function setDernierReset($resetDate)
    {
        if (is_string($resetDate))
            $resetDate = new \DateTime($resetDate);

        $this->dernierReset = $resetDate;

        $periode = new HistoriquePeriode($this, 'now');
        $this->addInHistorique($periode);
    }

    public function getDernierReset()
    {
        if ($this->dernierReset)
            return $this->dernierReset->format(Constants::$DATE_FORMAT);
        return null;
    }

    public function addInHistorique($periode)
    {
        if (!$this->getHistorique()->contains($periode))
            $this->getHistorique()->add($periode);
    }

    public function getHistorique()
    {
        return $this->historique;
    }

    public static function getGroup(): string
    {
        return 'Organisation';
    }


}
