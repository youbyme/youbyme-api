<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\INormalizable as EntityNormalizable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Competence
 *
 * @ORM\Table(name="COMPETENCE", indexes={@ORM\Index(name="FK_COMPOSER_COMPETENCE", columns={"COM_COMP_ID"})})
 * @ORM\Entity
 */
class Competence implements EntityNormalizable
{
    /**
     * @var int
     * @Groups({"Organisation", "Badge", "Vote", "Competence"})
     *
     * @Assert\Type(
     *     type="int",
     *     message="The skill id : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="COMP_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $compId;

    /**
     * @var string
     * @Groups({"Competence", "Vote", "Badge", "Organisation"})
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Type(
     *     type="string",
     *     message="The skill name {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 40,
     *      minMessage = "The skill name is too short, at least {{ limit }} characters.",
     *      maxMessage = "The skill name is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="COMP_NOM", type="string", length=40, nullable=false)
     */
    private $compNom;

    /**
     * @var string|null
     * @Groups("Competence")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The skill description {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 65535,
     *      minMessage = "The skill description is too short, at least {{ limit }} characters.",
     *      maxMessage = "The skill description is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="COMP_DESCRIPTION", type="text", length=65535, nullable=true)
     */
    private $compDescription;

    /**
     * Self-referencing
     *
     * @Groups({"Competence", "Badge"})
     *
     * @ORM\ManyToOne(targetEntity="Competence", inversedBy="enfants")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="COM_COMP_ID", referencedColumnName="COMP_ID")
     * })
     */
    private $competenceParent;

    /**
     * Self-referencing
     * @ORM\OneToMany(targetEntity="Competence", mappedBy="competenceParent", mappedBy="competenceParent")
     * @Groups("Competence")
     */
    private $enfants;


    /**
     * @var array
     * @Groups("Competence")
     *
     * @ORM\ManyToMany(targetEntity="Organisation", inversedBy="competences")
     * @ORM\JoinTable(name="PROPOSER_COMPETENCE",
     *   joinColumns={
     *     @ORM\JoinColumn(name="COMP_ID", referencedColumnName="COMP_ID")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ORGA_ID", referencedColumnName="ORGA_ID")
     *   }
     * )
     */
    private $organisations;

    /**
     * @var Badge
     *
     * @Groups("Competence")
     * @ORM\OneToOne(targetEntity="Badge", mappedBy="competence")
     */
    private $badge;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->organisations = new ArrayCollection();
        $this->enfants = new ArrayCollection();
    }
    /**
     * @return int
     */
    public function getCompId()
    {
        return $this->compId;
    }

    /**
     * @param int $compId
     */
    public function setCompId(int $compId): void
    {
        $this->compId = $compId;
    }

    /**
     * @return string
     */
    public function getCompNom()
    {
        return $this->compNom;
    }

    /**
     * @param string $compNom
     */
    public function setCompNom(string $compNom): void
    {
        $this->compNom = $compNom;
    }

    /**
     * @return null|string
     */
    public function getCompDescription()
    {
        return $this->compDescription;
    }

    /**
     * @param null|string $compDescription
     */
    public function setCompDescription(string $compDescription): void
    {
        $this->compDescription = $compDescription;
    }

    public function getCompetenceParent()
    {
        return $this->competenceParent;
    }

    public function setCompetenceParent(?Competence $competenceParent): void
    {
        $this->competenceParent = $competenceParent;
    }

    public function addCompetenceParent(Competence $competenceParent): void {
        if (!$this->competenceParent->contains($competenceParent))
            $this->competenceParent->add($competenceParent);
    }

    public function removeCompetenceParent(Competence $competenceParent): void
    {
        if($this->competenceParent->contains($competenceParent))
            $this->competenceParent->removeElement($competenceParent);
    }

    //public function getBadge(EntityManagerInterface $em): ?Badge
    //{
    //    $badge = $em->getRepository(Badge::class)->findOneBy(array('competence' => $this->compId));
    //    return $badge;
    //}

    /**
     * @return array
     */
    public function getOrganisations()
    {
        return $this->organisations;
    }

    /**
     * @param array $orga
     */
    public function setOrganisations(array $organisations): void
    {
        $this->organisations = $organisations;
    }

    public function addOrganisation(Organisation $organisation): void
    {
        if(!$this->organisations->contains($organisation))
            $this->organisations->add($organisation);
    }

    public function removeOrganisation(Organisation $organisation): void
    {
        if($this->organisations->contains($organisation))
            $this->organisations->removeElement($organisation);
    }

    public function getBadge()
    {
        return $this->badge;
    }

    public function setBadge($badge)
    {
        $this->badge = $badge;
    }

    public function getEnfants()
    {
        return $this->enfants;
    }

    public function setEnfants($enfant)
    {
        //not relevant
    }


    public static function getGroup(): string
    {
        return 'Competence';
    }
}
