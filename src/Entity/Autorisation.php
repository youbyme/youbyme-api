<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\INormalizable as EntityNormalizable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Autorisation
 *
 * @ORM\Table(name="AUTORISATION")
 * @ORM\Entity
 */
class Autorisation implements EntityNormalizable
{
    /**
     * @var int
     *
     * @Groups({"Autorisation", "Utilisateur", "Role"})
     *
     * @Assert\Type(
     *     type="int",
     *     message="ERR0100 : The authorization id : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="AUTO_ID", type="integer", options={"unsigned":true}, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $autoId;

    /**
     * @var string
     * @Groups({"Utilisateur", "Autorisation", "Role"})
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Type(
     *     type="string",
     *     message="ERR0101 : The authorization name {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 40,
     *      minMessage = "ERR0102 : The authorization name is too short, at least {{ limit }} characters.",
     *      maxMessage = "ERR0103 : The authorization name is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="AUTO_NOM", type="string", length=40, nullable=false)
     */
    private $autoNom;

    /**
     * @var string|null
     * @Groups({"Autorisation"})
     *
     * @Assert\Type(
     *     type="string",
     *     message="ERR0104 : The authorization descritpion {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 65535,
     *      minMessage = "ERR0105 : The authorization description is too short, at least {{ limit }} characters.",
     *      maxMessage = "ERR0106 : The authorization description is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="AUTO_DESCRIPTION", type="text", length=65535, nullable=true)
     */
    private $autoDescription;

    /**
     * @Groups("Autorisation")
     * @var array
     * @ORM\ManyToMany(targetEntity="Role", mappedBy="autorisations", cascade={"persist"})
     */
    private $roles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }
    /**
     * @return int
     */
    public function getAutoId(): int
    {
        return $this->autoId;
    }

    /**
     * @param int $autoId
     */
    public function setAutoId(int $autoId): void
    {
        $this->autoId = $autoId;
    }

    /**
     * @return string
     */
    public function getAutoNom(): string
    {
        return $this->autoNom;
    }

    /**
     * @param string $autoNom
     */
    public function setAutoNom(string $autoNom): void
    {
        $this->autoNom = $autoNom;
    }

    /**
     * @return null|string
     */
    public function getAutoDescription(): ?string
    {
        return $this->autoDescription;
    }

    /**
     * @param null|string $autoDescription
     */
    public function setAutoDescription(?string $autoDescription): void
    {
        $this->autoDescription = $autoDescription;
    }

    // Méthodes liées aux roles
    public function getRoles()
    {
        return $this->roles;
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    public function addRole($role): void {
        if (!$this->roles->contains($role))
            $this->roles->add($role);
    }

    public static function getGroup(): string
    {
        return "Autorisation";
    }
}
