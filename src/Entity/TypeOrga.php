<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use App\Entity\INormalizable as EntityNormalizable;

/**
 * Typeorga
 *
 * @ORM\Table(name="TYPEORGA")
 * @ORM\Entity
 */
class TypeOrga implements EntityNormalizable
{
    /**
     * @var int
     * @Groups({"TypeOrga", "Organisation"})
     *
     * @Assert\Type(
     *     type="int",
     *     message="The organization type id : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="TYPEORGA_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $typeOrgaId;

    /**
     * @var string
     * @Groups({"TypeOrga", "Organisation"})
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Type(
     *     type="string",
     *     message="The organization type name {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 40,
     *      minMessage = "The organization type name is too short, at least {{ limit }} characters.",
     *      maxMessage = "The organization type name is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="TYPEORGA_NOM", type="string", length=40, nullable=false)
     */
    private $typeOrgaNom;

    /**
     * @return int
     */
    public function getTypeOrgaId(): int
    {
        return $this->typeOrgaId;
    }

    /**
     * @param int $typeorgaId
     */
    public function setTypeOrgaId(int $typeOrgaId): void
    {
        $this->typeOrgaId = $typeOrgaId;
    }

    /**
     * @return string
     */
    public function getTypeOrgaNom(): string
    {
        return $this->typeOrgaNom;
    }

    /**
     * @param string $typeorgaNom
     */
    public function setTypeOrgaNom(string $typeOrgaNom): void
    {
        $this->typeOrgaNom = $typeOrgaNom;
    }

    public static function getGroup(): string
    {
        return 'TypeOrga';
    }

}
