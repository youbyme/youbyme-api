<?php

namespace App\Entity;

use App\Util\Constants;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\INormalizable as EntityNormalizable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;


/**
 * Vote
 *
 * @ORM\Table(name="VOTE", indexes={@ORM\Index(name="FK_EMETTRE", columns={"UTIL_ID"}), @ORM\Index(name="FK_RECEVOIR", columns={"UTI_UTIL_ID"}), @ORM\Index(name="FK_VOTER", columns={"COMP_ID"})})
 * @ORM\Entity(repositoryClass="App\Repository\VoteRepository")
 */
class Vote implements EntityNormalizable
{
    const CLASSIC = 0;
    const BONUS = 1;

    /**
     * @var int
     * @Groups({"Utilisateur", "Vote"})
     *
     * @Assert\Type(
     *     type="int",
     *     message="The vote id : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="VOTE_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $voteId;

    /**
     * @var \DateTime
     * @Groups("Vote")
     *
     * @Assert\DateTime
     *
     * @ORM\Column(name="VOTE_DATE", type="date", nullable=false)
     */
    private $date;

    /**
     * @var bool
     * @Groups("Vote")
     *
     * @Assert\NotNull
     * @Assert\Type(
     *     type="bool",
     *     message="The vote validity : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="VOTE_VALIDE", type="boolean", nullable=false)
     */
    private $valide = true;

    /**
     * @var int
     * @Groups("Vote")
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Type(
     *     type="int",
     *     message="The vote type : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="VOTE_TYPE", type="integer", nullable=false)
     */
    private $type;

    /**
     * @var Utilisateur
     * @Groups("Vote")
     *
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur", inversedBy="votesEmis")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UTIL_ID", referencedColumnName="UTIL_ID")
     * })
     */
    private $utilisateurEmetteur;

    /**
     * @var Utilisateur
     * @Groups("Vote")
     *
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur", inversedBy="votesRecus")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="UTI_UTIL_ID", referencedColumnName="UTIL_ID")
     * })
     */
    private $utilisateurReceveur;

    /**
     * @var Competence
     * @Groups("Vote")
     *
     *
     * @ORM\ManyToOne(targetEntity="Competence")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="COMP_ID", referencedColumnName="COMP_ID")
     * })
     */
    private $competence;

    /**
     * @return int
     */
    public function getVoteId(): int
    {
        return $this->voteId;
    }

    /**
     * @param int $voteId
     */
    public function setVoteId(int $voteId): void
    {
        $this->voteId = $voteId;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date->format(Constants::$DATE_FORMAT);
    }

    /**
     * @param \DateTime $voteDate
     */
    public function setDate($stringDate): void
    {
        $this->date = new \DateTime($stringDate);
    }

    /**
     * @return bool
     */
    public function isValide(): bool
    {
        return $this->valide;
    }

    /**
     * @param bool $voteValide
     */
    public function setValide(bool $valide): void
    {
        $this->valide = $valide;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $voteType
     */
    public function setType(int $voteType): void
    {
        $this->type = $voteType;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateurEmetteur()
    {
        return $this->utilisateurEmetteur;
    }

    /**
     * @param Utilisateur $utilisateurEmetteur
     */
    public function setUtilisateurEmetteur(Utilisateur $utilisateurEmetteur): void
    {
        $this->utilisateurEmetteur = $utilisateurEmetteur;
    }

    /**
     * @return Utilisateur
     */
    public function getUtilisateurReceveur()
    {
        return $this->utilisateurReceveur;
    }

    /**
     * @param Utilisateur $utilisateurReceveur
     */
    public function setUtilisateurReceveur(Utilisateur $utilisateurReceveur): void
    {
        $this->utilisateurReceveur = $utilisateurReceveur;
    }

    /**
     * @return Competence
     */
    public function getCompetence(): Competence
    {
        return $this->competence;
    }

    /**
     * @param Competence $comp
     */
    public function setCompetence(Competence $competence): void
    {
        $this->competence = $competence;
    }

    public static function getGroup(): string
    {
        return 'Vote';
    }


}
