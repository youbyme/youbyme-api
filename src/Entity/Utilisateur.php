<?php

namespace App\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\INormalizable as EntityNormalizable;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Utilisateur
 *
 * @ORM\Table(name="UTILISATEUR")
 * @ORM\Entity
 */

class Utilisateur implements UserInterface, EntityNormalizable
{
    /**
     * @var int
     * @Groups({"Utilisateur", "Role", "Vote", "Organisation", "BadgeAssignation"})
     *
     * @Assert\Type(
     *     type="int",
     *     message="The user id : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="UTIL_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $utilId;

    /**
     * @var string
     *
     * @Groups("Utilisateur")
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     *
     * @Assert\Type(
     *     type="string",
     *     message="The user login {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 40,
     *      minMessage = "The user login is too short, at least {{ limit }} characters.",
     *      maxMessage = "The user login is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="UTIL_PSEUDO", type="string", length=40, nullable=false)
     */
    private $utilPseudo;

    /*
      @Assert\Email(
           message = "The email '{{ value }}' is not a valid email.",
           checkMX = true
       )
     */
    /**
     * @var string
     *
     * @Groups("Utilisateur")
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     *
     * @Assert\Type(
     *     type="string",
     *     message="The user email {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 40,
     *      minMessage = "The user email is too short, at least {{ limit }} characters.",
     *      maxMessage = "The user email is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="UTIL_EMAIL", type="string", length=40, nullable=false)
     */
    private $utilEmail;

    /**
     * @var string
     *
     * @Groups({"Vote", "Regle", "Role", "Utilisateur", "Organisation", "BadgeAssignation"})
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     *
     * @Assert\Type(
     *     type="string",
     *     message="The user first name {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 40,
     *      minMessage = "The first name is too short, at least {{ limit }} characters.",
     *      maxMessage = "The first name is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="UTIL_PRENOM", type="string", length=40, nullable=false)
     */
    private $utilPrenom;

    /**
     * @var string
     *
     * @Groups({"Vote", "Regle", "Role", "Utilisateur", "Organisation", "BadgeAssignation"})
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     *
     * @Assert\Type(
     *     type="string",
     *     message="The user civility {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 10,
     *      minMessage = "The civility is too short, at least {{ limit }} characters.",
     *      maxMessage = "The civility is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="UTIL_CIVILITE", type="string", length=10, nullable=false)
     */
    private $utilCivilite;

    /**
     * @var string
     *
     * @Groups({"Vote", "Regle", "Role", "Utilisateur", "Organisation", "BadgeAssignation"})
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     *
     * @Assert\Type(
     *     type="string",
     *     message="The user last name {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 40,
     *      minMessage = "The last name is too short, at least {{ limit }} characters.",
     *      maxMessage = "The last name is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="UTIL_NOM", type="string", length=40, nullable=false)
     */
    private $utilNom;

    /**
     * @var ?string
     *
     * @Assert\Type(
     *     type="string",
     *     message="The user password {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 4,
     *      max = 256,
     *      minMessage = "The password is too short, at least {{ limit }} characters.",
     *      maxMessage = "The password is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="UTIL_MOT_DE_PASSE", type="string", length=256, nullable=false)
     */
    private $utilMotDePasse;

    /**
     * @var string
     *
     * @ORM\Column(name="UTIL_SALT", type="string", length=256, nullable=false)
     */
    private $utilSalt;

    /**
     * @var string|null
     *
     * @Groups("Utilisateur")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The user avatar URL {{ value }} is not a {{ type }}."
     * )
     * @Assert\Url(
     *    protocols = {"http", "https"}
     * )
     *
     * @ORM\Column(name="UTIL_URL_AVATAR", type="string", length=256, nullable=true)
     */
    private $utilUrlAvatar;

    /**
     * @var int
     *
     * @Groups("Utilisateur")
     *
     *
     * @ORM\Column(name="UTIL_POINT_DISPO", type="integer", nullable=false)
     */
    private $utilPointDispo;

    /**
     * @var int
     *
     *
     * @Groups("Utilisateur")
     *
     *
     * @ORM\Column(name="UTIL_POINT_BONUS_DISPO", type="integer", nullable=false)
     */
    private $utilPointBonusDispo;

    /**
     * @var array
     * @Groups("Utilisateur")
     *
     *
     * @ORM\ManyToMany(targetEntity="Role", mappedBy="utilisateurs", cascade={"persist"})
     */
    private $utilRoles;

    /**
     * @var array
     *
     * @Groups("Utilisateur")
     *
     * @ORM\OneToMany(targetEntity="BadgeAssignation", mappedBy="utilisateur")
     */
    private $badges;

    /**
     *
     * @Groups("Utilisateur")
     *
     * @ORM\ManyToOne(targetEntity="Organisation", inversedBy="utilisateurs")
     * @ORM\JoinColumn(name="ORGA_ID", referencedColumnName="ORGA_ID", nullable=true, onDelete="SET NULL")
     */
    private $organisation;

    /**
     * @var array
     * @Groups("Utilisateur")
     *
     * @ORM\ManyToMany(targetEntity="Organisation", inversedBy="administrateurs")
     * @ORM\JoinTable(name="GERER_ORGANISATION",
     *   joinColumns={
     *     @ORM\JoinColumn(name="UTIL_ID", referencedColumnName="UTIL_ID")
     *  },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="ORGA_ID", referencedColumnName="ORGA_ID")
     *    }
     * )
     */
    private $organisationsGeres;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Vote", mappedBy="utilisateurEmetteur", cascade={"remove"})
     */
    private $votesEmis;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Vote", mappedBy="utilisateurReceveur", cascade={"remove"})
     */
    private $votesRecus;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->utilRoles = new ArrayCollection();
        $this->badges = new ArrayCollection();
        $this->organisationsGeres = new ArrayCollection();
        $this->votesEmis = new ArrayCollection();
        $this->votesRecus = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getUtilId(): int
    {
        return $this->utilId;
    }

    /**
     * @param int $utilId
     */
    public function setUtilId(int $utilId): void
    {
        $this->utilId = $utilId;
    }

    /**
     * @return string
     */
    public function getUtilPseudo(): string
    {
        return $this->utilPseudo;
    }

    /**
     * @param string $utilPseudo
     */
    public function setUtilPseudo(string $utilPseudo): void
    {
        $this->utilPseudo = $utilPseudo;
    }

    /**
     * @return string
     */
    public function getUtilEmail(): string
    {
        return $this->utilEmail;
    }

    /**
     * @param string $utilEmail
     */
    public function setUtilEmail(string $utilEmail): void
    {
        $this->utilEmail = $utilEmail;
    }

    /**
     * @return string
     */
    public function getUtilCivilite(): string
    {
        return $this->utilCivilite;
    }

    /**
     * @param string $utilCivilite
     */
    public function setUtilCivilite(string $utilCivilite): void
    {
        $this->utilCivilite = $utilCivilite;
    }

    /**
     * @return string
     */
    public function getUtilPrenom(): string
    {
        return $this->utilPrenom;
    }

    /**
     * @param string $utilPrenom
     */
    public function setUtilPrenom(string $utilPrenom): void
    {
        $this->utilPrenom = $utilPrenom;
    }

    /**
     * @return string
     */
    public function getUtilNom(): string
    {
        return $this->utilNom;
    }

    /**
     * @param string $utilNom
     */
    public function setUtilNom(string $utilNom): void
    {
        $this->utilNom = $utilNom;
    }

    /**
     * @return null|string
     */
    public function getUtilMotDePasse(): ?string
    {
        return $this->utilMotDePasse;
    }

    /**
     * @param string $utilMotDePasse
     */
    public function setUtilMotDePasse(string $utilMotDePasse): void
    {
        $this->utilMotDePasse = $utilMotDePasse;
    }

    /**
     * @return string
     */
    public function getUtilSalt(): string
    {
        return $this->utilSalt;
    }

    /**
     * @param string $utilSalt
     */
    public function setUtilSalt(string $utilSalt): void
    {
        $this->utilSalt = $utilSalt;
    }

    /**
     * @return null|string
     */
    public function getUtilUrlAvatar(): ?string
    {
        return $this->utilUrlAvatar;
    }

    /**
     * @param null|string $utilUrlAvatar
     */
    public function setUtilUrlAvatar(?string $utilUrlAvatar): void
    {
        $this->utilUrlAvatar = $utilUrlAvatar;
    }

    /**
     * @return int
     */
    public function getUtilPointDispo(): int
    {
        return $this->utilPointDispo;
    }

    /**
     * @param int $utilPointDispo
     */
    public function setUtilPointDispo(int $utilPointDispo): void
    {
        $this->utilPointDispo = $utilPointDispo;
    }

    /**
     * @return int
     */
    public function getUtilPointBonusDispo(): int
    {
        return $this->utilPointBonusDispo;
    }

    /**
     * @param int $utilPointBonusDispo
     */
    public function setUtilPointBonusDispo(int $utilPointBonusDispo): void
    {
        $this->utilPointBonusDispo = $utilPointBonusDispo;
    }


    /**
     * @return ArrayCollection
     */
    public function getUtilRoles()
    {
        return $this->utilRoles;
    }

    /**
     * @param ArrayCollection $roles
     */
    public function setUtilRoles(array $roles): void
    {
        $this->utilRoles = $roles;
    }

    public function setOrganisation($organisation)
    {
        $this->organisation = $organisation;
    }

    public function getOrganisation()
    {
        return $this->organisation;
    }

    public function setOrganisationsGeres(array $orgas)
    {
        $this->organisationsGeres = $orgas;
    }

    public function getOrganisationsGeres()
    {
        return $this->organisationsGeres;
    }

    public function addOrganisationGere($orga)
    {
        if (!$this->organisationsGeres->contains($orga))
            $this->organisationsGeres->add($orga);

        //heritage
        foreach ($orga->getOrgaEnfants() as $child) {
            $this->addOrganisationGere($child);
        }
    }

    public function removeOrganisationGere($orga)
    {
        if ($this->organisationsGeres->contains($orga))
            $this->organisationsGeres->removeElement($orga);
    }

    /**
     * @return ArrayCollection
     */
    public function getBadges()
    {
        $badges = array();
        foreach ($this->badges->toArray() as $attribution)
        {
            if ($attribution->isValide())
                array_push($badges, $attribution->getBadge());
        }
        return $badges;
    }

    public function getAttributions()
    {
        return $this->badges;
    }

    public function removeBadge(Badge $badge, EntityManagerInterface $em)
    {
        $attributionBadge = $em->getRepository(BadgeAssignation::class)->findOneBy(array('utilisateur' => $this->getUtilId(), 'badge' => $badge->getBadgId()));
        if ($attributionBadge != null) {
            $em->remove($attributionBadge);
            $em->flush();
        }

    }

    public function assignBadge(Badge $badge, EntityManagerInterface $em)
    {
        $attributionbadge = new BadgeAssignation();
        $attributionbadge->setUtilisateur($this);
        $attributionbadge->setBadge($badge);
        $attributionbadge->setDate('now');

        $this->badges->add($attributionbadge);

        $em->persist($attributionbadge);
        $em->flush();

    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->getUtilMotDePasse();
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return $this->getUtilSalt();
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->getUtilEmail();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    public function getRoles()
    {
        $strRoles = array();
        $roles = $this->getUtilRoles();
        foreach ($roles as $key => $value){
            array_push($strRoles, $value->getRoleNom());
        }
        return $strRoles;
    }

    public function getVotesEmis()
    {
        return $this->votesEmis;
    }

    public function getVotesRecus()
    {
        return $this->votesRecus;
    }

    public function getEtudiants()
    {
        $orgas = $this->getOrganisationsGeres()->toArray();
        $utils = array();
        foreach ($orgas as $orga)
        {
            foreach ($orga->getUtilisateurs()->toArray() as $util)
            {
                array_push($utils, $util);
            }
        }
        //Suppime l'utilisateur courant
        array_splice($utils, array_search($this, $utils), 1);
        return $utils;
    }

    public static function getGroup(): string
    {
        return 'Utilisateur';
    }
}

class UtilisateurProgress implements EntityNormalizable
{
    /**
     * @Groups("UtilisateurProgress")
     */
    private $compId;

    /**
     * @Groups("UtilisateurProgress")
     */
    private $competence;

    /**
     * @Groups("UtilisateurProgress")
     */
    private $percentage;

    public function setCompId($id)
    {
        $this->compId = $id;
    }

    public function getCompId()
    {
        return $this->compId;
    }

    /**
     * @return mixed
     */
    public function getCompetence()
    {
        return $this->competence;
    }

    /**
     * @param mixed $competence
     */
    public function setCompetence($competence): void
    {
        $this->competence = $competence;
    }

    /**
     * @return mixed
     */
    public function getPercentage()
    {
        return $this->percentage;
    }

    /**
     * @param mixed $percentage
     */
    public function setPercentage($percentage): void
    {
        $this->percentage = $percentage;
    }

    static function getGroup(): string
    {
        return 'UtilisateurProgress';
    }
}