<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\INormalizable as EntityNormalizable;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Role
 *
 * @ORM\Table(name="ROLE")
 * @ORM\Entity
 */
class Role implements EntityNormalizable
{
    /**
     * @var int
     *
     * @Groups({"Role", "Utilisateur", "Autorisation"})
     *
     * @Assert\Type(
     *     type="int",
     *     message="The role id : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="ROLE_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $roleId;

    /**
     * @var string
     *
     * @Groups({"Role", "Utilisateur", "Autorisation"})
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Type(
     *     type="string",
     *     message="The role name {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 40,
     *      minMessage = "The role name is too short, at least {{ limit }} characters.",
     *      maxMessage = "The role name is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ROLE_NOM", type="string", length=40, nullable=false)
     */
    private $roleNom;

    /**
     * @var string|null
     * @Groups("Role")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The role description {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 1,
     *      max = 65535,
     *      minMessage = "The role description is too short, at least {{ limit }} characters.",
     *      maxMessage = "The role description is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="ROLE_DESCRIPTION", type="text", length=65535, nullable=true)
     */
    private $roleDescription;

    /**
     * @var ArrayCollection
     * @Groups({"Utilisateur", "Role"})
     *
     * @ORM\ManyToMany(targetEntity="Autorisation", inversedBy="roles", cascade={"persist"})
     * @ORM\JoinTable(name="CONTENIR_AUTORISATION",
     *   joinColumns={
     *     @ORM\JoinColumn(name="ROLE_ID", referencedColumnName="ROLE_ID")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="AUTO_ID", referencedColumnName="AUTO_ID")
     *   }
     * )
     */
    private $autorisations;

    /**
     * @var array
     * @Groups("Role")
     *
     * @ORM\ManyToMany(targetEntity="Utilisateur", inversedBy="utilRoles")
     * @ORM\JoinTable(name="OBTENIR_ROLE",
     *   joinColumns={
     *     @ORM\JoinColumn(name="ROLE_ID", referencedColumnName="ROLE_ID")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="UTIL_ID", referencedColumnName="UTIL_ID")
     *   }
     * )

     */
    private $utilisateurs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->autorisations = new ArrayCollection();
        $this->utilisateurs = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getRoleId(): int
    {
        return $this->roleId;
    }

    /**
     * @param int $roleId
     */
    public function setRoleId(int $roleId): Role
    {
        $this->roleId = $roleId;
        return $this;
    }

    /**
     * @return string
     */
    public function getRoleNom(): string
    {
        return $this->roleNom;
    }

    /**
     * @param string $roleNom
     */
    public function setRoleNom(string $roleNom): void
    {
        $this->roleNom = $roleNom;
    }

    /**
     * @return null|string
     */
    public function getRoleDescription(): ?string
    {
        return $this->roleDescription;
    }

    /**
     * @param null|string $roleDescription
     */
    public function setRoleDescription(?string $roleDescription): void
    {
        $this->roleDescription = $roleDescription;
    }

    /**
     * @return ArrayCollection
     */
    public function getAutorisations()
    {
        return $this->autorisations;
    }

    /**
     * @param ArrayCollection $auto
     */
    public function setAutorisations(array $autorisations): void
    {
        $this->autorisations = $autorisations;
    }

    public function addAutorisation($autorisation): void
    {
        if(!$this->autorisations->contains($autorisation))
            $this->autorisations->add($autorisation);
    }

    public function removeAutorisation($autorisation): void
    {
        if($this->autorisations->contains($autorisation))
            $this->autorisations->removeElement($autorisation);
    }

    public function addUtilisateur($utilisateur): void
    {

        if(!$this->utilisateurs->contains($utilisateur))
            $this->utilisateurs->add($utilisateur);
    }

    public function removeUtilisateur($utilisateur)
    {
        if($this->utilisateurs->contains($utilisateur))
            $this->utilisateurs->removeElement($utilisateur);
    }

    /**
     * @return ArrayCollection
     */
    public function getUtilisateurs()
    {
        return $this->utilisateurs;
    }

    /**
     * @param ArrayCollection $util
     */
    public function setUtilisateurs(array $utilisateurs): void
    {
        $this->utilisateurs = $utilisateurs;
    }

    public static function getGroup(): string
    {
        return 'Role';
    }

}
