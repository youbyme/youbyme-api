<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Table(name="HISTORIQUE_PERIODE", indexes={@ORM\Index(name="FK_F1D7728C9E6B1585", columns={"organisation_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\HistoriquePeriodesRepository")
 *
 * This entity is used to register voting history for an oganisation.
 * This is useful in particular to display the calendar.
 */
class HistoriquePeriode implements INormalizable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups("HistoriquePeriode")
     */
    private $id;

    /** owning side
     * @ORM\ManyToOne(targetEntity="App\Entity\Organisation", inversedBy="historique")
     * @ORM\JoinColumn(nullable=false, referencedColumnName="ORGA_ID")
     * @Groups("HistoriquePeriode")
     */
    private $organisation;

    /**
     * @ORM\Column(type="datetime")
     * @Groups("HistoriquePeriode")
     */
    private $date;

    public function __construct(Organisation $orga, string $date)
    {
        $this->organisation = $orga;
        $this->date = new \DateTime($date);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOrganisation()
    {
        return $this->organisation;
    }

    public function setOrganisation(?Organisation $organisation)
    {
        $this->organisation = $organisation;

        return $this;
    }

    public function getDate()
    {
        return $this->date->format('Y-m-d');
    }

    public function setDate($date)
    {
        $this->date = new \DateTime($date);
    }

    public static function getGroup(): string
    {
        return 'HistoriquePeriode';
    }
}
