<?php
/**
 * Created by PhpStorm.
 * User: I341379
 * Date: 16/01/2019
 * Time: 16:45
 */

namespace App\Entity;


interface INormalizable
{
    static function getGroup(): string;
}