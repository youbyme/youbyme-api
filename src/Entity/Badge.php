<?php

namespace App\Entity;

use App\Util\Constants;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use App\Entity\INormalizable as EntityNormalizable;
use Hoa\Exception\Exception;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Badge
 *
 * @ORM\Table(name="BADGE", indexes={@ORM\Index(name="FK_REPRESENTER", columns={"COMP_ID"})})
 * @ORM\Entity
 */
class Badge implements EntityNormalizable
{

    /**
     * @var int
     * @Groups({"Badge", "Utilisateur", "BadgeAssignation", "Competence"})
     *
     *
     * @Assert\Type(
     *     type="int",
     *     message="The badge id : {{ value }} is not an {{ type }}."
     * )
     *
     * @ORM\Column(name="BADG_ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $badgId;

    /**
     * @var string
     * @Groups({"Utilisateur", "Badge", "BadgeAssignation", "Competence"})
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     * @Assert\Type(
     *     type="string",
     *     message="The badge name {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 40,
     *      minMessage = "The badge name is too short, at least {{ limit }} characters.",
     *      maxMessage = "The badge name is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="BADG_NOM", type="string", length=40, nullable=false)
     */
    private $badgNom;

    /**
     * @var string|null
     * @Groups({"Badge", "Competence"})
     *
     * @Assert\Type(
     *     type="string",
     *     message="The badge image URL {{ value }} is not a {{ type }}."
     * )
     * @Assert\Url(
     *    protocols = {"http", "https"}
     * )
     *
     * @ORM\Column(name="BADG_IMAGE_URL", type="string", length=256, nullable=true)
     */
    private $badgImageUrl;

    /**
     * @var string|null
     * @Groups("Badge")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The badge descritpion {{ value }} is not a {{ type }}."
     * )
     * @Assert\Length(
     *      min = 0,
     *      max = 65535,
     *      minMessage = "The badge description is too short, at least {{ limit }} characters.",
     *      maxMessage = "The badge description is too long, at most {{ limit }} characters."
     * )
     *
     * @ORM\Column(name="BADG_DESCRIPTION", type="text", length=65535, nullable=true)
     */
    private $badgDescription;

    /**
     * @var \DateTime|string|null
     * @Groups("Badge")
     *
     * @Assert\DateTime
     *
     * @ORM\Column(name="BADG_EXPIRATION", type="date", nullable=true)
     */
    private $badgExpiration;

    /**
     * @var string|null
     * @Groups("Badge")
     *
     * @Assert\Type(
     *     type="string",
     *     message="The badge credential URL {{ value }} is not a {{ type }}."
     * )
     * @Assert\Url(
     *    protocols = {"http", "https"}
     * )
     *
     * @ORM\Column(name="BADG_PREUVE_URL", type="string", length=256, nullable=true)
     */
    private $badgPreuveUrl;

    /**
     * @var ?int
     *
     * @Assert\Type(
     *     type="int",
     *     message="The badge required points {{ value }} are not an {{ type }}."
     * )
     *
     * @Groups("Badge")
     * @ORM\Column(name="BADG_POINTS_REQUIS", type="integer", nullable=true)
     */
    private $nbPointsRequis;

    /**
     * @var Competence
     * @Groups("Badge")
     *
     * @ORM\OneToOne(targetEntity="Competence", inversedBy="badge", fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="COMP_ID", referencedColumnName="COMP_ID")
     * })
     */
    private $competence;

    /**
     * @var array
     *
     * @ORM\OneToMany(targetEntity="BadgeAssignation", mappedBy="badge", cascade={"remove"})
     */
    private $attributionsUtilisateur;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->attributionsUtilisateur = new ArrayCollection();
    }
    /**
     * @return int
     */
    public function getBadgId()
    {
        return $this->badgId;
    }

    /**
     * @param int $badgId
     */
    public function setBadgId(int $badgId): void
    {
        $this->badgId = $badgId;
    }

    /**
     * @return string
     */
    public function getBadgNom(): string
    {
        return $this->badgNom;
    }

    /**
     * @param string $badgNom
     */
    public function setBadgNom(string $badgNom): void
    {
        $this->badgNom = $badgNom;
    }

    /**
     * @return null|string
     */
    public function getBadgImageUrl(): ?string
    {
        return $this->badgImageUrl;
    }

    /**
     * @param null|string $badgImageUrl
     */
    public function setBadgImageUrl(?string $badgImageUrl): void
    {
        $this->badgImageUrl = $badgImageUrl;
    }

    /**
     * @return null|string
     */
    public function getBadgDescription(): ?string
    {
        return $this->badgDescription;
    }

    /**
     * @param null|string $badgDescription
     */
    public function setBadgDescription(?string $badgDescription): void
    {
        $this->badgDescription = $badgDescription;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getBadgExpiration()
    {
        if ($this->badgExpiration)
            return $this->badgExpiration->format(Constants::$DATE_FORMAT);
        return null;
    }

    /**
     * @param \DateTime|string|null $badgExpiration
     */
    public function setBadgExpiration($badgExpiration): void
    {
        if (is_string($badgExpiration)) {
            $badgExpiration = new \DateTime($badgExpiration);
        }
        $this->badgExpiration = $badgExpiration;
    }

    /**
     * @return null|string
     */
    public function getBadgPreuveUrl(): ?string
    {
        return $this->badgPreuveUrl;
    }

    /**
     * @param null|string $badgPreuveUrl
     */
    public function setBadgPreuveUrl(?string $badgPreuveUrl): void
    {
        $this->badgPreuveUrl = $badgPreuveUrl;
    }

    public function setNbPointsRequis(?int $nbPoints)
    {
        $this->nbPointsRequis = $nbPoints;
    }

    public function  getNbPointsRequis()
    {
        return $this->nbPointsRequis;
    }

    public function getAttributionsUtilisateur()
    {
        return $this->attributionsUtilisateur;
    }

    /**
     * @return Competence
     */
    public function getCompetence()
    {
        return $this->competence;
    }

    /**
     * @param Competence $comp
     */
    public function setCompetence($competence): void
    {
        $this->competence = $competence;
    }


    public static function getGroup(): string
    {
        return 'Badge';
    }

}
