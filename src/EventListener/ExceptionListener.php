<?php
    // src/EventListener/ExceptionListener.php
    namespace App\EventListener;

    use App\Exception\AlreadyVotedException;
    use App\Exception\NoMoreBonusPointsAvailableException;
    use App\Exception\NoMorePointsAvailableException;
    use Doctrine\ORM\EntityNotFoundException;
    use Doctrine\ORM\ORMInvalidArgumentException;
    use http\Exception\InvalidArgumentException;
    use JMS\Serializer\Exception\NotAcceptableException;
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
    use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
    use Symfony\Component\Intl\Exception\NotImplementedException;
    use Symfony\Component\Security\Core\Exception\AuthenticationException;
    use Symfony\Component\Validator\Exception\InvalidOptionsException;

    class ExceptionListener
    {
        /**
         * Catch errors on entity before controllers processing
         */
        public function onKernelException(GetResponseForExceptionEvent $event)
        {
            // You get the exception object from the received event
            $exception = $event->getException();
            $data['message'] = $exception->getMessage();
            //FIXME relevant ?
            $data['statusCode'] = $exception->getCode();

            // Customize your response object to display the exception details
            $response = new JsonResponse('',Response::HTTP_BAD_REQUEST,[], true);
            if($exception instanceof ORMInvalidArgumentException) {
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            }
            if ($exception instanceof NotFoundHttpException) {
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
                // Vérifier si c'est une erreur de ParamConverter
                if(strpos($data['message'], ' object not found by the @ParamConverter annotation.') && strpos($data['message'], '\Entity')) {
                    $entityPath = explode(' ', $exception->getMessage());
                    $entity = explode('\\',$entityPath[0]);
                    $entity ? $data['message'] = "The requested '".end($entity)."' entity does not exist, please change the id in your request and try again.": null;
                }
            }
            if($exception instanceof NotImplementedException) {
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            }
            if ($exception instanceof BadRequestHttpException) {
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            }
            if($exception instanceof AuthenticationException) {
                $response->setStatusCode(Response::HTTP_UNAUTHORIZED);
            }
            if ($exception instanceof InvalidOptionsException) {
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
                $data['errors'] = $exception->getOptions();
            }
            if ($exception instanceof InvalidArgumentException) {
                $response->setStatusCode(Response::HTTP_BAD_REQUEST);
            }
            if ($exception instanceof EntityNotFoundException) {
                $response->setStatusCode(Response::HTTP_NOT_FOUND);
            }
            if ($exception instanceof NotAcceptableException) {
                $response->setStatusCode(Response::HTTP_NOT_ACCEPTABLE);
            }
            if ($exception instanceof AlreadyVotedException || $exception instanceof NoMorePointsAvailableException || $exception instanceof NoMoreBonusPointsAvailableException) {
                $response->setStatusCode(Response::HTTP_NOT_MODIFIED);
            }

            $data['statusCode'] = $response->getStatusCode();
            $message = json_encode($data);
            $response->setContent($message);

            // sends the modified response object to the event
            $event->setResponse($response);
        }
    }