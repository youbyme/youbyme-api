<?php
/**
 * Created by PhpStorm.
 * User: I341379
 * Date: 04/01/2019
 * Time: 12:28
 */
namespace App\Util;

use App\Entity\INormalizable;
use App\Entity\Utilisateur;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Doctrine\Common\Annotations\AnnotationReader;

class YoubymeUtil
{
    public static function toJson($entity)
    {
        //obligatoire pour lire l'annotation @MaxDepth
        $classMetadataFactory = new ClassMetadataFactory(new AnnotationLoader(new AnnotationReader()));
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer($classMetadataFactory);


        if (!is_array($entity))
            $group = $entity->getGroup();
        else if (sizeof($entity) > 0)
            $group = $entity[0]->getGroup();
        else
            $group = '';


        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return "self";
        });

        $serializer = new Serializer(array($normalizer), array($encoder));
        $json = $serializer->serialize($entity, 'json', [ObjectNormalizer::GROUPS => [$group]]);
        return $json;
    }


    public static function isPilote(Utilisateur $util)
    {
        if ($util->getOrganisationsGeres() !== null && count($util->getOrganisationsGeres()))
        {
            return true;
        }
        return false;
    }

    public static function distinct($array) {
        $distinct = array();
        foreach ($array as $entity) {
            $exist = false;
            foreach ($distinct as $setEntity) {
                if ($setEntity == $entity) {
                    $exist = true;
                    break;
                }
            }
            if (!$exist) {
                array_push($distinct, $entity);
            }
        }
        return $distinct;
    }
}