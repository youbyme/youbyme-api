<?php

namespace App\Util;
use App\Entity\Utilisateur;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\Exception\NotAcceptableException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class HandleAPIExceptionUtil extends AbstractController
{
    public static function isRequestValid(Request $request, $entity, ValidatorInterface $validator, $classname):void {
        self::testContentType($request);
        self::isEntityValid($request, $entity, $validator, $classname);
    }

    private static function isEntityValid(Request $request, $entity, ValidatorInterface $validator, $classname):void {
        $entity = $entity->get('serializer')->deserialize($request->getContent(), $classname, 'json');
        // Permet de valider l'entité
        $errors = $validator->validate($entity);
        if (\count($errors) > 0) {
            $arrError = [];
            foreach ($errors as $violation) {
                $arrError[] = "'".$violation->getPropertyPath()."' : ".$violation->getMessage();
            }
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrError);
        }
    }

    private static function testContentType(Request $request):void {
        if ($request->getContentType() !== 'json'){
            throw new NotAcceptableException('ContentType of your request is not required json type, '.$request->getContentType().' content sent.');
        }
    }

    public static function entityExist($idEntity, $classname, EntityManagerInterface $em) {
        $entity = $em->find($classname, $idEntity);
        if(!$entity) {
            return 'The '.substr(strrchr($classname, "\\"), 1).' with id '.$idEntity.' doesn\'t exist.';
        }
        return $entity;
    }

    public static function isUserAlreadyExist($util, EntityManagerInterface $em):void {
        $utilisateurPseudo = $em->getRepository(Utilisateur::class)->findOneBy([
            'utilPseudo' => $util->getUtilPseudo()
        ]);
        if($utilisateurPseudo) {
            $arrExecption[] = "Ce pseudo existe déjà.";
        }

        $utilisateurEmail = $em->getRepository(Utilisateur::class)->findOneBy([
            'utilEmail' => $util->getUtilEmail()
        ]);
        if($utilisateurEmail) {
            $arrExecption[] = "Cette adresse mail existe déjà.";
        }

        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);
    }
}