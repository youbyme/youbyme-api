<?php
/**
 * Created by PhpStorm.
 * User: I341379
 * Date: 22/01/2019
 * Time: 15:39
 */
namespace App\Repository;
use App\Entity\Vote;
use App\Util\Constants;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Symfony\Component\Validator\Constraints\DateTime;

class VoteRepository extends EntityRepository
{
    public function countValidVotes($userId, $compId)
    {
        $qb = $this->_em->createQueryBuilder();
        return $qb->select($qb->expr()->count('v'))->from(Vote::class, 'v')
            ->where('v.valide = true')->andWhere('v.utilisateurReceveur = ?1')->andWhere('v.competence = ?2')
            ->setParameters(array(1 => $userId, 2 => $compId))->getQuery()->getSingleScalarResult();
    }

    public function hasAlreadyVotedFor($userEmetId, $userRecevId, $dernierReset): bool
    {
        //dd($dernierReset);
        $qb = $this->_em->createQueryBuilder();
        $result = $qb->select('v.voteId')->from(Vote::class, 'v')->where('v.utilisateurEmetteur = ?1')->andWhere('v.utilisateurReceveur = ?2')
            ->andWhere('v.date BETWEEN :startDate AND :endDate')->setParameters(array(1 => $userEmetId, 2 => $userRecevId))
            ->setParameter('startDate', $dernierReset)
            ->setParameter('endDate', date(Constants::$DATE_FORMAT, strtotime('next saturday')))
            ->getQuery()->getResult();

        if (count($result) != 0)
            return true;
        return false;
    }

    public function getVotesDateRanged(\DateTime $startDate, \DateTime $endDate)
    {
        $qb = $this->_em->createQueryBuilder('v');
        $qb->select('v.voteId')->from(Vote::class, 'v')
            ->where('v.date BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate->format(Constants::$DATE_FORMAT))
            ->setParameter('endDate', $endDate->format(Constants::$DATE_FORMAT));
        $results = $qb->getQuery()->getArrayResult();
        $objectResults = array();
        foreach ($results as $result)
        {
            array_push($objectResults, $this->findOneBy($result));
        }
        return $objectResults;
    }
}