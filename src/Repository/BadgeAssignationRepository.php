<?php
/**
 * Created by PhpStorm.
 * User: Snowy
 * Date: 05/04/2019
 * Time: 11:46
 */

namespace App\Repository;

use App\Entity\BadgeAssignation;
use App\Util\Constants;
use Doctrine\ORM\EntityRepository;

class BadgeAssignationRepository extends EntityRepository
{

    public function getVotesDateRanged(\DateTime $startDate, \DateTime $endDate)
    {
        $qb = $this->_em->createQueryBuilder();
        $qb->select('(a.utilisateur)', '(a.badge)')->from(BadgeAssignation::class, 'a')
            ->where('a.date BETWEEN :startDate AND :endDate')
            ->setParameter('startDate', $startDate->format(Constants::$DATE_FORMAT))
            ->setParameter('endDate', $endDate->format(Constants::$DATE_FORMAT));


        return $qb->getQuery()->getArrayResult();
    }

}