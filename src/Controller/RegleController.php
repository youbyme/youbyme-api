<?php

namespace App\Controller;

use App\Annotation\Autorisation;
use App\Entity\Organisation;
use App\Entity\Regle;
use App\Util\HandleAPIExceptionUtil;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Util\YoubymeUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * @Route("/regles")
 */
class RegleController extends AbstractController
{
    /**
     * Get the list of all rules
     *
     * This route is used to display all the rules.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the rules.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Regle::class, groups={"Regle"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there are no rules."
     * )
     * @SWG\Tag(name="Regles")
     * @Security(name="Bearer")
     *
     * @Route(name="regle_index", methods="GET")
     * @Autorisation("ReadRegles")
     */
    public function index(): Response
    {
        $regle = $this->getDoctrine()
            ->getRepository(Regle::class)
            ->findAll();

        $jsonArray = YoubymeUtil::toJson($regle);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    /**
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the types of the values",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(type="string")
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Tag(name="Regles")
     * @Security(name="Bearer")
     *
     * @Route("/types", name="types", methods={"GET"})
     * @Autorisation("ReadRegles")
     */
    public function getTypeBinding()
    {
        $typeBinding = array('b', 'i', 's');
        $result = $this->get('serializer')->serialize($typeBinding, 'json');
        return new JsonResponse($result, Response::HTTP_OK, [], true);
    }

    /**
     * Create a rule
     *
     * This route is used to create a rule.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when created.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Regle::class, groups={"Regle"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="reglNom", type="string", example="Name"),
     *              @SWG\Property(property="reglValeur", type="string", example="Value"),
     *              @SWG\Property(property="reglType", type="string", example="X"),
     *              @SWG\Property( property="organisations",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Organisation::class",
     *                      @SWG\Property(property="orgaId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Regles")
     * @Security(name="Bearer")
     *
     * @Route(name="regle_new", methods="POST")
     * @Autorisation("CreateRegles")
     */
    public function new(Request $request, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Regle::class);
        $regle = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Regle', 'json');

        $organisations = new ArrayCollection();
        foreach ($regle->getOrganisations() as $emptyOrga)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyOrga->getOrgaId(), Organisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $organisation = $result;
            isset($organisation) ? $organisations->add($organisation) : null;
        }
        isset($organisations) ? $regle->setOrganisations($organisations->toArray()) : null;

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->persist($regle);
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($regle), Response::HTTP_CREATED, [], true);
    }

    /**
     * Get one rule
     *
     * This route is used to display one given rule.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the requested rule.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Regle::class, groups={"Regle"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no rule."
     * )
     * @SWG\Tag(name="Regles")
     * @Security(name="Bearer")
     *
     * @Route("/{reglId}", name="regle_show", methods="GET", requirements={"reglId"="\d+"})
     * @Autorisation("ReadRegles")
     */
    public function show(Regle $regle): Response
    {
        $json = YoubymeUtil::toJson($regle);

        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    /**
     * Update a rule
     *
     * This route is used to update an existing rule.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when updated.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Regle::class, groups={"Regle"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no rule."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="reglNom", type="string", example="Name"),
     *              @SWG\Property(property="reglValeur", type="string", example="Value"),
     *              @SWG\Property(property="reglType", type="string", example="X"),
     *              @SWG\Property( property="organisations",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Organisation::class",
     *                      @SWG\Property(property="orgaId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Regles")
     * @Security(name="Bearer")
     *
     * @Route("/{reglId}", name="regle_edit", methods="PUT", requirements={"reglId"="\d+"})
     * @Autorisation("UpdateRegles")
     */
    public function edit(Request $request, Regle $regle, EntityManagerInterface$em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Regle::class);
        $regleUpdated = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Regle', 'json');
        $regle->setReglNom($regleUpdated->getReglNom());
        $regle->setReglValeur($regleUpdated->getReglValeur());
        $regle->setReglType($regleUpdated->getReglType());

        $organisations = new ArrayCollection();
        foreach ($regleUpdated->getOrganisations() as $emptyOrga)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyOrga->getOrgaId(), Organisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $organisation = $result;
            isset($organisation) ? $organisations->add($organisation) : null;
        }
        isset($organisations) ? $regle->setOrganisations($organisations->toArray()) : null;

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($regle),  Response::HTTP_ACCEPTED, [], true);
    }

    /**
     * Delete a rule
     *
     * This route is rule for delete an existing skill.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when deleted."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no rule."
     * )

     * @SWG\Tag(name="Regles")
     * @Security(name="Bearer")
     *
     * @Route("/{reglId}", name="regle_delete", methods="DELETE", requirements={"reglId"="\d+"})
     * @Autorisation("DeleteRegles")
     */
    public function delete(Request $request, Regle $regle): Response
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($regle);
        $em->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
