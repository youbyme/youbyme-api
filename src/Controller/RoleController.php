<?php

namespace App\Controller;

use App\Entity\Autorisation;
use App\Entity\Role;
use App\Entity\Utilisateur;
use App\Util\HandleAPIExceptionUtil;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Util\YoubymeUtil;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use App\Annotation\Autorisation as AutorisationAnnotation;

/**
 * @Route("/roles")
 */
class RoleController extends AbstractController
{
    /**
     * Get the list of all roles
     *
     * This route is used to display all the roles.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the roles.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Role::class, groups={"Role"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there are no roles."
     * )
     * @SWG\Tag(name="Roles")
     * @Security(name="Bearer")
     *
     * @Route(name="role_index", methods="GET")
     * @AutorisationAnnotation("ReadRoles")
     */
    public function index(): Response
    {
        $role = $this->getDoctrine()
            ->getRepository(Role::class)
            ->findAll();

        $jsonArray = YoubymeUtil::toJson($role);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    /**
     * Create a role
     *
     * This route is used to create a role.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when created.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Role::class, groups={"Role"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="roleNom", type="string", example="Name"),
     *              @SWG\Property(property="roleDescription", type="string", example="Value"),
     *              @SWG\Property( property="autorisations",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Autorisation::class",
     *                      @SWG\Property(property="autoId", type="integer", example=0 ),
     *                  )
     *              ),
     *         @SWG\Property( property="utilisateurs",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Utilisateur::class",
     *                      @SWG\Property(property="utilId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Roles")
     * @Security(name="Bearer")
     *
     * @Route(name="role_new", methods="POST")
     * @AutorisationAnnotation("CreateRoles")
     */
    public function new(Request $request, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Role::class);
        $role = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Role', 'json');

        // Relation autorisation
        $autorisations = new ArrayCollection();
        foreach ($role->getAutorisations() as $emptyAuto)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyAuto->getAutoId(), Autorisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $autorisation = $result;
            isset($autorisation) ? $autorisations->add($autorisation) : null;
        }
        isset($autorisations) ? $role->setAutorisations($autorisations->toArray()) : null;

        // Relation utilisateur
        $utilisateurs = new ArrayCollection();
        foreach ($role->getUtilisateurs() as $emptyUtil)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyUtil->getUtilId(), Utilisateur::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $utilisateur = $result;
            isset($utilisateur) ? $utilisateurs->add($utilisateur) : null;
        }
        isset($utilisateurs) ? $role->setUtilisateurs($utilisateurs->toArray()) : null;

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->persist($role);
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($role), Response::HTTP_CREATED, [], true);
    }

    /**
     * Get one role
     *
     * This route is used to display one given role.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the requested role.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Role::class, groups={"Role"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no role."
     * )
     * @SWG\Tag(name="Roles")
     * @Security(name="Bearer")
     *
     * @Route("/{roleId}", name="role_show", methods="GET", requirements={"roleId"="\d+"})
     * @AutorisationAnnotation("ReadRoles")
     */
    public function show(Role $role): Response
    {
        $json = YoubymeUtil::toJson($role);

        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    /**
     * Update a role
     *
     * This route is used to update an existing role.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when updated.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Role::class, groups={"Role"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no role."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="roleNom", type="string", example="Name"),
     *              @SWG\Property(property="roleDescription", type="string", example="Value"),
     *              @SWG\Property( property="autorisations",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Autorisation::class",
     *                      @SWG\Property(property="autoId", type="integer", example=0 ),
     *                  )
     *              ),
     *         @SWG\Property( property="utilisateurs",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Utilisateur::class",
     *                      @SWG\Property(property="utilId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Roles")
     * @Security(name="Bearer")
     *
     * @Route("/{roleId}", name="role_edit", methods="PUT", requirements={"roleId"="\d+"})
     * @AutorisationAnnotation("UpdateRoles")
     */
    public function edit(Request $request, Role $role, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Role::class);
        $roleUpdated = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Role', 'json');
        $role->setRoleNom($roleUpdated->getRoleNom());
        $role->setRoleDescription($roleUpdated->getRoleDescription());

        // Relation autorisation
        $autorisations = new ArrayCollection();
        foreach ($roleUpdated->getAutorisations() as $emptyAuto)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyAuto->getAutoId(), Autorisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $autorisation = $result;
            isset($autorisation) ? $autorisations->add($autorisation) : null;
        }
        isset($autorisations) ? $role->setAutorisations($autorisations->toArray()) : null;

        // Relation utilisateur
        $utilisateurs = new ArrayCollection();
        foreach ($roleUpdated->getUtilisateurs() as $emptyUtil)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyUtil->getUtilId(), Utilisateur::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $utilisateur = $result;
            isset($utilisateur) ? $utilisateurs->add($utilisateur) : null;
        }
        isset($utilisateurs) ? $role->setUtilisateurs($utilisateurs->toArray()) : null;

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($role),  Response::HTTP_ACCEPTED, [], true);
    }

    /**
     * Delete a role
     *
     * This route is role for delete an existing skill.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when deleted."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no role."
     * )

     * @SWG\Tag(name="Roles")
     * @Security(name="Bearer")
     *
     * @Route("/{roleId}", name="role_delete", methods="DELETE", requirements={"roleId"="\d+"})
     * @AutorisationAnnotation("DeleteRoles")
     */
    public function delete(Request $request, Role $role, EntityManagerInterface $em): Response
    {
        //if ($this->isCsrfTokenValid('delete'.$role->getRoleId(), $request->request->get('_token'))) {
            $em->remove($role);
            $em->flush();
        //}

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
