<?php

namespace App\Controller;

use App\Entity\Autorisation;
use App\Entity\Role;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Util\YoubymeUtil;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Util\HandleAPIExceptionUtil;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use Swagger\Annotations as SWG;

/**
 * @Route("/autorisations")
 */
class AutorisationController extends AbstractController
{
    /**
     * Get the list of all authorizations
     *
     * This route is used to display all the authorizations.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the authorizations.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Autorisation::class, groups={"Autorisation"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there are no authorizations."
     * )
     * @SWG\Tag(name="Autorisations")
     * @Security(name="Bearer")
     *
     * @Route(name="autorisation_index", methods="GET")
     *
     * @QueryParam(name="autoNom")
     *
     * @\App\Annotation\Autorisation("ReadAutorisations")
     */
    //@QueryParam(name="roles", map=true)
    public function index(Request $request, ParamFetcher $paramFetcher): Response
    {
        $params = array_intersect_assoc($paramFetcher->all(), $request->query->all());
        $autorisations = $this->getDoctrine()
            ->getRepository(Autorisation::class)
            ->findBy($params);
        $jsonArray = YoubymeUtil::toJson($autorisations);

        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    /**
     * Create an authorization
     *
     * This route is used to create an authorization.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when created.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Autorisation::class, groups={"Autorisation"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="autoNom", type="string", example="Name"),
     *              @SWG\Property(property="autoDescription", type="string", example="Description"),
     *              @SWG\Property( property="roles",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Role::class",
     *                      @SWG\Property(property="roleId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Autorisations")
     * @Security(name="Bearer")
     *
     * @Route(name="autorisation_new", methods="POST")
     * @\App\Annotation\Autorisation("CreateAutorisations")
     */
    public function new(Request $request, EntityManagerInterface $em, ValidatorInterface $validator): JsonResponse
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Autorisation::class);

        $autorisation = $this->get('serializer')->deserialize($request->getContent(), Autorisation::class, 'json');

        /*$autorisation->getRoles() ne retourne que des roles vides, seul leur Id est indiqué
         * On va donc chercher les entités role par leur Id pour effectuer la relation
         */
        //Recuperation des roles par rapport a leur Id indiqué dans le json
        $roles = new ArrayCollection();
        foreach ($autorisation->getRoles() as $emptyRole)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyRole->getRoleId(), Role::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $role = $result;
            isset($role) ? $role->addAutorisation($autorisation) : null;
            isset($role) ? $roles->add($role) : null;
        }
        isset($roles) ? $autorisation->setRoles($roles->toArray()) : null;

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->persist($autorisation);
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($autorisation), Response::HTTP_CREATED, [], true);
    }

    /**
     * Get one authorization
     *
     * This route is used to display one given authorization.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the requested authorization.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Autorisation::class, groups={"Autorisation"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no authorization."
     * )
     * @SWG\Tag(name="Autorisations")
     * @Security(name="Bearer")
     *
     * @Route("/{autoId}", name="autorisation_show", methods="GET", requirements={"autoId"="\d+"})
     * @\App\Annotation\Autorisation("ReadAutorisations")
     */
    public function show(Autorisation $autorisation): Response
    {
        $json = YoubymeUtil::toJson($autorisation);

        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    /**
     * Update an authorization
     *
     * This route is used to update an existing authorization.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when updated.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Autorisation::class, groups={"Autorisation"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no authorization."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="autoNom", type="string", example="Name"),
     *              @SWG\Property(property="autoDescription", type="string", example="Description"),
     *              @SWG\Property( property="roles",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Role::class",
     *                      @SWG\Property(property="roleId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Autorisations")
     * @Security(name="Bearer")
     *
     * @Route("/{autoId}", name="autorisation_edit", methods="PUT", requirements={"autoId"="\d+"})
     * @\App\Annotation\Autorisation("UpdateAutorisations")
     */
    public function edit(Request $request, Autorisation $autorisation, EntityManagerInterface $em, ValidatorInterface $validator): JsonResponse
    {
        // Util pour tester la requête avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Autorisation::class);

        $autorisationUpdated = $this->get('serializer')->deserialize($request->getContent(), Autorisation::class, 'json');

        $autorisation->setAutoNom($autorisationUpdated->getAutoNom());
        $autorisation->setAutoDescription($autorisationUpdated->getAutoDescription());

        $roles = new ArrayCollection();
        //On supprime les anciennes liaisons des roles
        foreach($autorisation->getRoles() as $emptyRole)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyRole->getRoleId(), Role::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $role = $result;
            isset($role) ? $role->removeAutorisation($autorisation) : null;
        }
        foreach ($autorisationUpdated->getRoles() as $emptyRole)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyRole->getRoleId(), Role::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $role = $result;
            isset($role) ? $role->addAutorisation($autorisation) : null;
            isset($role) ? $roles->add($role) : null;
        }
        isset($roles) ? $autorisation->setRoles($roles->toArray()) : null;

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($autorisation),  Response::HTTP_ACCEPTED, [], true);
    }

    /**
     * Delete an authorization
     *
     * This route is used to delete an existing authorization.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when deleted."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no authorization."
     * )

     * @SWG\Tag(name="Autorisations")
     * @Security(name="Bearer")
     *
     * @Route("/{autoId}", name="autorisation_delete", methods="DELETE", requirements={"autoId"="\d+"})
     * @\App\Annotation\Autorisation("DeleteAutorisations")
     */
    public function delete(Request $request, Autorisation $autorisation, EntityManagerInterface $em): Response
    {
        //if ($this->isCsrfTokenValid('delete'.$autorisation->getAutoId(), $request->request->get('_token'))) {
            $em->remove($autorisation);
            $em->flush();
        //}

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
