<?php
namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Utilisateur;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Util\YoubymeUtil;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class AuthController extends AbstractController
{
    public function logout()
    {
        //TODO
    }

    /**
     * Get current user
     *
     * This route is used to display current user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the current user.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Utilisateur::class, groups={"Utilisateur"}))
     *     )
     * )
     * @SWG\Tag(name="Utilisateurs")
     * @Security(name="Bearer")
     *
     * @Route("/auth/currentuser", name="organisation_show", methods="GET", requirements={"orgaId"="\d+"})
     */
    public function current(): Response
    {
        return new JsonResponse(YoubymeUtil::toJson($this->getUser()),  Response::HTTP_OK, [], true);
    }
}