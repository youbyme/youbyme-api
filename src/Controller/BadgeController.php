<?php

namespace App\Controller;

use App\Annotation\Autorisation;
use App\Entity\Badge;
use App\Entity\Competence;
use App\Entity\Utilisateur;
use App\Util\HandleAPIExceptionUtil;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Util\YoubymeUtil;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * @Route("/badges")
 */
class BadgeController extends AbstractController
{
    /**
     * Get the list of all badges
     *
     * This route is used to display all the badges.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the badges.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Badge::class, groups={"Badge"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there are no badges."
     * )
     * @SWG\Tag(name="Badges")
     * @Security(name="Bearer")
     *
     * @Route(name="badge_index", methods="GET")
     * @Autorisation("ReadBadges")
     */
    public function index(): Response
    {
        $badges = array();
        if (YoubymeUtil::isPilote($this->getUser()))
        {
            $badges = $this->getBadgePilote($this->getUser());
        } else {
            $badges = $this->getBadgeEtudiant($this->getUser());
        }

        $jsonArray = YoubymeUtil::toJson($badges);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    private function getBadgePilote(Utilisateur $pilote)
    {
        $badges = array();
        $competences = array();
        foreach ($pilote->getOrganisationsGeres() as $orga)
        {
            $competences = array_merge($competences, $orga->getCompetences()->toArray());
        }
        foreach($competences as $comp)
        {
            array_push($badges, $comp->getBadge());
        }
        return YoubymeUtil::distinct($badges);
    }

    private function getBadgeEtudiant(Utilisateur $etudiant)
    {
        $badges = array();
        foreach ($etudiant->getOrganisation()->getCompetences()->toArray() as $comp)
        {
            array_push($badges, $comp->getBadge());
        }
        return $badges;
    }


    /**
     * Create a badge
     *
     * This route is used to create a badge.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when created.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Badge::class, groups={"Badge"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="badgNom", type="string", example="Name"),
     *              @SWG\Property(property="badgImageUrl", type="string", example="http://via.placeholder.com/500"),
     *              @SWG\Property(property="badgDescription", type="string", example="Description"),
     *              @SWG\Property(property="badgExpiration", type="string", example="2019-01-01 00:00:00"),
     *              @SWG\Property(property="badgPreuveUrl", type="string", example="http://via.placeholder.com/500"),
     *              @SWG\Property(property="nbPointsRequis", type="integer", example=0),
     *              @SWG\Property( property="competence", type="object",
     *                  @SWG\Property(property="compId", type="integer", example=0 ),
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Badges")
     * @Security(name="Bearer")
     *
     * @Route(name="badge_new", methods="POST")
     * @Autorisation("CreateBadges")
     */
    public function new(Request $request, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Badge::class);

        $badge = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Badge', 'json');
        // Association du Utilisateur émeteur
        if ($badge->getCompetence() !== null && $badge->getCompetence()->getCompId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($badge->getCompetence()->getCompId(), Competence::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $competence = $result;
        }
        isset($competence) ? $badge->setCompetence($competence) : null;

        // Vérifier qu'il n'y ait pas déjà une compétence associée au badge
        if($em->getRepository(Badge::class)->findOneBy(array('competence' => $badge->getCompetence()))) {
            $arrExecption[] = "This competence is already associated to a badge.";
        }

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->persist($badge);
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($badge), Response::HTTP_CREATED, [], true);
    }

    /**
     * Get one badge
     *
     * This route is used to display one given badge.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the requested badge.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Badge::class, groups={"Badge"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no badge."
     * )
     * @SWG\Tag(name="Badges")
     * @Security(name="Bearer")
     *
     * @Route("/{badgId}", name="badge_show", methods="GET", requirements={"badgId"="\d+"})
     * @Autorisation("ReadBadges")
     */
    public function show(Badge $badge): Response
    {
        $json = YoubymeUtil::toJson($badge);

        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    /**
     * Update a badge
     *
     * This route is used to update an existing badge.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when updated.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Badge::class, groups={"Badge"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no badge."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="badgNom", type="string", example="Name"),
     *              @SWG\Property(property="badgImageUrl", type="string", example="http://via.placeholder.com/500"),
     *              @SWG\Property(property="badgDescription", type="string", example="Description"),
     *              @SWG\Property(property="badgExpiration", type="string", example="2019-01-01 00:00:00"),
     *              @SWG\Property(property="badgPreuveUrl", type="string", example="http://via.placeholder.com/500"),
     *              @SWG\Property(property="nbPointsRequis", type="integer", example=0),
     *              @SWG\Property( property="competence", type="object",
     *                  @SWG\Property(property="compId", type="integer", example=0 ),
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Badges")
     * @Security(name="Bearer")
     *
     * @Route("/{badgId}", name="badge_edit", methods="PUT", requirements={"badgId"="\d+"})
     * @Autorisation("UpdateBadges")
     */
    public function edit(Request $request, Badge $badge, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Badge::class);

        $badgeUpdated =  $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Badge', 'json');
        $badge->setBadgNom($badgeUpdated->getBadgNom());
        $badge->setBadgImageUrl($badgeUpdated->getBadgImageUrl());
        $badge->setBadgDescription($badgeUpdated->getBadgDescription());

        $badge->setBadgExpiration($badgeUpdated->getBadgExpiration());
        $badge->setBadgPreuveUrl($badgeUpdated->getBadgPreuveUrl());
        $badge->setNbPointsRequis($badgeUpdated->getNbPointsRequis());

        // Association du Utilisateur émeteur
        if ($badgeUpdated->getCompetence() && $badgeUpdated->getCompetence()->getCompId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($badgeUpdated->getCompetence()->getCompId(), Competence::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $competence = $result;
        }
        isset($competence) ? $badge->setCompetence($competence) : null;


        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($badge),  Response::HTTP_ACCEPTED, [], true);
    }

    /**
     * Delete a badge
     *
     * This route is used to delete an existing badge.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when deleted."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no badge."
     * )

     * @SWG\Tag(name="Badges")
     * @Security(name="Bearer")
     *
     * @Route("/{badgId}", name="badge_delete", methods="DELETE", requirements={"badgId"="\d+"})
     * @Autorisation("DeleteBadges")
     */
    public function delete(Request $request, Badge $badge, EntityManagerInterface $em): Response
    {
        //if ($this->isCsrfTokenValid('delete'.$badge->getBadgId(), $request->request->get('_token'))) {}
        $em->remove($badge);
        $em->flush();

        return new Response('', Response::HTTP_NO_CONTENT);
    }

}
