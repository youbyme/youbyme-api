<?php

namespace App\Controller;

use App\Entity\Badge;
use App\Entity\Competence;
use App\Entity\Organisation;
use App\Entity\Role;
use App\Entity\Vote;
use App\Entity\Utilisateur;
use App\Entity\UtilisateurProgress;
use App\Util\HandleAPIExceptionUtil;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Util\YoubymeUtil;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use App\Annotation\Autorisation;
use Nelmio\ApiDocBundle\Annotation\Model;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * @Route("/utilisateurs")
 */
class UtilisateurController extends AbstractController
{
    /**
     * Get the list of all users
     *
     * This route is used to display all the users.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the users.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Utilisateur::class, groups={"Utilisateur"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Tag(name="Utilisateurs")
     * @Security(name="Bearer")
     *
     * @QueryParam(name="utilPrenom")
     * @QueryParam(name="utilNom")
     * @QueryParam(name="utilPseudo")
     * @QueryParam(name="utilEmail")
     * @QueryParam(name="utilCivilite")
     * @QueryParam(name="utilPointDispo", requirements="\d+")
     * @QueryParam(name="organisation", requirements="\d+")
     *
     *
     * @Route(name="utilisateur_index", methods="GET")
     * @Autorisation("ReadUtilisateurs")
     */
    //@QueryParam(name="utilRoles")
    //@QueryParam(name="organisationsGeres")
    public function index(Request $request, ParamFetcher $paramFetcher): Response
    {
        $params = array_intersect_assoc($paramFetcher->all(), $request->query->all());
        $utilFiltre = $this->getDoctrine()
            ->getRepository(Utilisateur::class)
            ->findBy($params);
        $etudiants = $this->getUser()->getEtudiants();
        $etudFiltres = array_values(array_uintersect(array_values($utilFiltre), array_values($etudiants), function($a, $b)
        {
            return strcmp(spl_object_hash($a), spl_object_hash($b));
        }));
        $jsonArray = YoubymeUtil::toJson($etudFiltres);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    /**
     * Get all votable users
     *
     * This route is used to get all votable users available for one user
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the votable users.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Utilisateur::class, groups={"Utilisateur"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Tag(name="Utilisateurs")
     * @Security(name="Bearer")
     *
     * @Route("/votables", name="utilisateurs_votables", methods="GET")
     * @Autorisation("ShowUtilisateursVotables")
     */
    public function listeUtilisateurVotables(EntityManagerInterface $em): Response
    {
        $utilisateur = $this->getUser();
        $camarades = $utilisateur->getOrganisation()->getUtilisateurs()->toArray();
        array_splice($camarades, array_search($utilisateur, $camarades),1);

        $utilisateurVotables = array();
        foreach ($camarades as $camarade)
        {       //Si l'utilisateur n'a pas déja voté pour ce camarade
            if (!$em->getRepository(Vote::class)->hasAlreadyVotedFor($utilisateur, $camarade,
                $utilisateur->getOrganisation()->getDernierReset())) {
                array_push($utilisateurVotables, $camarade);
                //Alors il est consideré comme un utilisateur votable
            }
        }

        $jsonArray = YoubymeUtil::toJson($utilisateurVotables);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    /**
     * Get all skills available
     *
     * This route is used to get all skills available available for one user
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the skills votables.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Competence::class, groups={"Competence"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Tag(name="Utilisateurs")
     * @Security(name="Bearer")
     *
     * @Route("/{utilId}/compVotables", name="competences_votables", methods={"GET"}, requirements={"utilId"="\d+"})
     * @Autorisation("ShowUtilisateursVotables")
     * @Security(name="Bearer")
     */
    public function listeCompetenceVotables(Utilisateur $utilisateur): Response
    {
        $competencesNonVotables = array();
        foreach ($utilisateur->getBadges() as $badgeRecu)
        {
            array_push( $competencesNonVotables, $badgeRecu->getCompetence());
        }

        $competencesVotables = array_values(array_udiff($utilisateur->getOrganisation()->getCompetences()->toArray(),$competencesNonVotables, function ($a, $b) {
            return strcmp(spl_object_hash($a), spl_object_hash($b));
        }));

        $jsonArray = YoubymeUtil::toJson($competencesVotables);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    /**
     * Create a list of user
     *
     * This route is used to several users
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when created.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Utilisateur::class, groups={"Utilisateur"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(
     *                  type="object",
     *                  @SWG\Property(property="utilPseudo", type="string", example="Pseudo"),
     *                  @SWG\Property(property="utilEmail", type="string", example="mail@mail.com"),
     *                  @SWG\Property(property="utilPrenom", type="string", example="First name"),
     *                  @SWG\Property(property="utilNom", type="string", example="Last name"),
     *                  @SWG\Property(property="utilMotdePasse", type="string", example="password"),
     *                  @SWG\Property(property="utilPointDispo", type="string", example=0),
     *                  @SWG\Property(property="utilPointBonusDispo", type="string", example=0),
     *                  @SWG\Property( property="roles",
     *                      type="array",
     *                      @SWG\Items(
     *                          type="Role::class",
     *                          @SWG\Property(property="roleId", type="integer", example=0 ),
     *                      )
     *                  ),
     *                  @SWG\Property( property="organisation", type="Organisation::class",
     *                      @SWG\Property(property="orgaId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Utilisateurs")
     * @Security(name="Bearer")
     * @Route("/import/{codeAnalytics}", name="importUserArray", methods={"POST"})
     * @Autorisation("CreateUtilisateurs")
     */
    public function newUsers(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder, $codeAnalytics)
    {
        $utilisateurs = new ArrayCollection();
        $data = json_decode($request->getContent(), true);
        foreach ($data as $result)
        {
            $utilisateur = $this->get('serializer')->denormalize($result, 'App\Entity\Utilisateur', 'json');
            $utilisateur->setOrganisation($this->getOrganisationByCodeAnalytics($em, $codeAnalytics));
            $this->createUser($utilisateur, $em, $encoder);
            $utilisateurs->add($utilisateur);
        }
        return new JsonResponse(YoubymeUtil::toJson($utilisateurs->toArray()), Response::HTTP_CREATED, [], true);
    }

    /**
     * Create a user
     *
     * This route is used to create a user.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when created.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Utilisateur::class, groups={"Utilisateur"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="utilPseudo", type="string", example="Pseudo"),
     *              @SWG\Property(property="utilEmail", type="string", example="mail@mail.com"),
     *              @SWG\Property(property="utilPrenom", type="string", example="First name"),
     *              @SWG\Property(property="utilNom", type="string", example="Last name"),
     *              @SWG\Property(property="utilMotdePasse", type="string", example="password"),
     *              @SWG\Property(property="utilPointDispo", type="string", example=0),
     *              @SWG\Property(property="utilPointBonusDispo", type="string", example=0),
     *              @SWG\Property( property="roles",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Role::class",
     *                      @SWG\Property(property="roleId", type="integer", example=0 ),
     *                  )
     *              ),
     *              @SWG\Property( property="organisation", type="Organisation::class",
     *                  @SWG\Property(property="orgaId", type="integer", example=0 ),
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Utilisateurs")
     * @Security(name="Bearer")
     * @Autorisation("CreateUtilisateurs")
     * @Route(name="utilisateur_new", methods="POST")
     */

    public function new(Request $request, EntityManagerInterface $em, ValidatorInterface $validator, UserPasswordEncoderInterface $encoder): Response
    {
        //Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Utilisateur::class);
        $utilisateur = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Utilisateur', 'json');
        $this->createUser($utilisateur, $em, $encoder);

        return new JsonResponse(YoubymeUtil::toJson($utilisateur), Response::HTTP_CREATED, [], true);
    }

    public function createUser(Utilisateur $utilisateur, EntityManagerInterface $em, UserPasswordEncoderInterface $encoder)
    {
        // Vérifier si l'utilisateur n'existe pas déjà
        HandleAPIExceptionUtil::isUserAlreadyExist($utilisateur, $em);

        $time = time();
        $utilisateur->setUtilSalt($utilisateur->getUtilEmail() . $time);
        if($utilisateur->getUtilMotDePasse()) {
            $utilisateur->setUtilMotDePasse($encoder->encodePassword($utilisateur, $utilisateur->getUtilMotDePasse()));
        } else {
            throw new BadRequestHttpException("You haven't define a password.");
        }

        // Association de l'organisation
        if ($utilisateur->getOrganisation()->getOrgaId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($utilisateur->getOrganisation()->getOrgaId(), Organisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $organisation = $result;
        }
        isset($organisation) ? $utilisateur->setOrganisation($organisation) : null;

        //association des organisationGeres
        $utilisateur->setOrganisationsGeres(array());
        foreach ($utilisateur->getOrganisationsGeres() as $emptyOrga)
        {
            $result = HandleAPIExceptionUtil::entityExist($emptyOrga->getOrgaId(), Organisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $orga = $result;
            isset($orga) ? $orga->addAdministrateur($utilisateur) : null;
            isset($orga) ? $utilisateur->addOrganisationGere($orga) : null;
        }


        // Associations des Roles
        $roles = new ArrayCollection();
        foreach ($utilisateur->getUtilRoles() as $emptyRole)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyRole->getRoleId(), Role::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $role = $result;
            isset($role) ? $role->addUtilisateur($utilisateur) : null;
            isset($role) ? $roles->add($role) : null;
        }
        isset($roles) ? $utilisateur->setUtilRoles($roles->toArray()) : null;
        $utilisateur->setUtilPointDispo(0);
        $utilisateur->setUtilPointBonusDispo(0);
        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->persist($utilisateur);
        $em->flush();
    }

    /**
     * Get one user
     *
     * This route is used to display one given user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the requested user.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Utilisateur::class, groups={"Utilisateur"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no user."
     * )
     * @SWG\Tag(name="Utilisateurs")
     * @Security(name="Bearer")
     *
     * @Route("/{utilId}", name="utilisateur_show", methods="GET", requirements={"utilId"="\d+"})
     * @Autorisation("ReadUtilisateurs")
     */
    public function show(Utilisateur $utilisateur): Response
    {
        $json = YoubymeUtil::toJson($utilisateur);

        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    /**
     * Update a user
     *
     * This route is used to update an existing user.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when updated.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Utilisateur::class, groups={"Utilisateur"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no user."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="utilPseudo", type="string", example="Pseudo"),
     *              @SWG\Property(property="utilEmail", type="string", example="mail@mail.com"),
     *              @SWG\Property(property="utilPrenom", type="string", example="First name"),
     *              @SWG\Property(property="utilNom", type="string", example="Last name"),
     *              @SWG\Property(property="utilMotdePasse", type="string", example="password"),
     *              @SWG\Property(property="utilPointDispo", type="string", example=0),
     *              @SWG\Property(property="utilPointBonusDispo", type="string", example=0),
     *              @SWG\Property( property="roles",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Role::class",
     *                      @SWG\Property(property="roleId", type="integer", example=0 ),
     *                  )
     *              ),
     *              @SWG\Property( property="organisation", type="Organisation::class",
     *                  @SWG\Property(property="orgaId", type="integer", example=0 ),
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Utilisateurs")
     * @Security(name="Bearer")
     *
     * @Route("/{utilId}", name="utilisateur_edit", methods="PUT", requirements={"utilId"="\d+"})
     * @Autorisation("UpdateUtilisateurs")
     */
    public function edit(Request $request, Utilisateur $utilisateur, EntityManagerInterface $em, ValidatorInterface $validator, UserPasswordEncoderInterface $encoder): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Utilisateur::class);
        $utilUpdated = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Utilisateur', 'json');
        // Vérifier si changement de pseudo ou email
        if($utilisateur->getUtilPseudo() != $utilUpdated->getUtilPseudo()) {
            $arrExecption[] = "You cannot change the pseudo.";
        } else {
            $utilisateur->setUtilPseudo($utilUpdated->getUtilPseudo());
        }
        if($utilisateur->getUtilEmail() != $utilUpdated->getUtilEmail()) {
            $arrExecption[] = "You cannot change the email.";
        } else {
            $utilisateur->setUtilEmail($utilUpdated->getUtilEmail());
        }
        $utilisateur->setUtilPrenom($utilUpdated->getUtilPrenom());
        $utilisateur->setUtilNom($utilUpdated->getUtilNom());
        $utilisateur->setUtilMotdePasse($encoder->encodePassword($utilisateur, $utilUpdated->getUtilMotDePasse()));
        $utilisateur->setUtilUrlAvatar($utilUpdated->getUtilUrlAvatar());
        $utilisateur->setUtilPointDispo($utilUpdated->getUtilPointDispo());
        $utilisateur->setUtilPointBonusDispo($utilUpdated->getUtilPointBonusDispo());

        // Association de l'organisation
        if ($utilUpdated->getOrganisation()->getOrgaId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($utilUpdated->getOrganisation()->getOrgaId(), Organisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $organisation = $result;
        }
        isset($organisation) ? $utilisateur->setOrganisation($organisation) : null;

        //association des organisationGeres
        $utilisateur->setOrganisationsGeres(array());
        foreach ($utilUpdated->getOrganisationsGeres() as $emptyOrga)
        {
            $result = HandleAPIExceptionUtil::entityExist($emptyOrga->getOrgaId(), Organisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $orga = $result;
            isset($orga) ? $orga->addAdministrateur($utilisateur) : null;
            isset($orga) ? $utilisateur->addOrganisationGere($orga) : null;
        }

        // Associations des roles
        $utilisateurs = new ArrayCollection();
        //On supprime les anciennes liaisons des regles
        foreach($utilisateur->getUtilRoles() as $emptyRole)
        {
            // Util pour tester les associations
            //FIXME not relevant wtf
            $result = HandleAPIExceptionUtil::entityExist($emptyRole->getRoleId(), Role::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $role = $result;
            isset($role) ? $role->removeUtilisateur($utilisateur) : null;
        }
        foreach ($utilUpdated->getUtilRoles() as $emptyRole)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyRole->getRoleId(), Role::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $role = $result;
            isset($role) ? $role->addUtilisateur($utilisateur) : null;
            isset($role) ? $utilisateurs->add($role) : null;
        }
        isset($utilisateurs) ? $utilisateur->setUtilRoles($utilisateurs->toArray()) : null;

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($utilisateur),  Response::HTTP_ACCEPTED, [], true);
    }

    /**
     * Delete a user
     *
     * This route is used to delete an existing user.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when deleted."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no skill."
     * )

     * @SWG\Tag(name="Utilisateurs")
     * @Security(name="Bearer")
     *
     * @Route("/{utilId}", name="utilisateur_delete", methods="DELETE", requirements={"utilId"="\d+"})
     * @Autorisation("DeleteUtilisateurs")
     */
    public function delete(Request $request, Utilisateur $utilisateur, EntityManagerInterface $em): Response
    {
        //if ($this->isCsrfTokenValid('delete'.$utilisateur->getUtilId(), $request->request->get('_token'))) {
            $em->remove($utilisateur);
            $em->flush();
        //}

        return new Response('', Response::HTTP_NO_CONTENT);
    }

    public function getOrganisationByCodeAnalytics(EntityManagerInterface $em, $codeAnalytics)
    {
        return $em->getRepository(Organisation::class)->findOneBy(array('orgaCodeAnalytics' => $codeAnalytics));
    }


    /**
     * Get progress of a user
     *
     * This route is used to get percentage of progress in each competences for a user.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when user found."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when user not found."
     * )

     * @SWG\Tag(name="Utilisateurs")
     * @Security(name="Bearer")
     *
     * @Route("/{utilId}/progress", name="utilisateur_progress", methods="GET", requirements={"utilId"="\d+"})
     * @Autorisation("ReadUtilisateurs")
     */
    public function getProgress(EntityManagerInterface $em, Utilisateur $utilisateur)
    {
        $userCompetences = $utilisateur->getOrganisation()->getCompetences()->toArray();
        $childCompetences = array();
        foreach ($userCompetences as $comp) {
            if ($comp->getCompetenceParent() != null)
            {
                array_push($childCompetences, $comp);
            }
        }
        $progresses = array();
        foreach ($childCompetences as $comp)
        {
            $progress = new UtilisateurProgress();
            $progress->setCompetence($comp->getCompNom());
            $progress->setCompId($comp->getCompId());
            $userVotedCompetences = $em->getRepository(Vote::class)->findBy(array(
                'utilisateurReceveur' => $utilisateur->getUtilId(),
                'competence' => $comp,
                'valide' => true
            ));

            $count = count($userVotedCompetences);
            $progress->setPercentage($count / $comp->getBadge()->getNbPointsRequis() * 100);
            array_push($progresses, $progress);
        }
        $jsonArray = YoubymeUtil::toJson($progresses);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);

    }
}
