<?php

namespace App\Controller;

use App\Annotation\Autorisation;
use App\Entity\Competence;
use App\Entity\Organisation;
use App\Entity\Utilisateur;
use App\Util\HandleAPIExceptionUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Util\YoubymeUtil;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * @Route("/competences")
 */
class CompetenceController extends AbstractController
{
    /**
     * Get the list of all skills
     *
     * This route is used to display all the skills.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the skills.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Competence::class, groups={"Competence"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there are no skills."
     * )
     * @SWG\Tag(name="Competences")
     * @Security(name="Bearer")
     *
     * @Route(name="competence_index", methods="GET")
     * @Autorisation("ReadCompetences")
     */
    public function index(): Response
    {
        $user = $this->getUser();

        if (YoubymeUtil::isPilote($user))
        {
            $competences = $this->getCompetencesPilote($user);
        } else {
            $competences = $this->getCompetencesEtudiant($user);
        }

        $jsonArray = YoubymeUtil::toJson($competences);

        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    private function getCompetencesPilote(Utilisateur $pilote)
    {
        $competences = array();
        foreach ($pilote->getOrganisationsGeres() as $orga)
        {
            $competences = array_merge($competences, $orga->getCompetences()->toArray());
        }
        return YoubymeUtil::distinct($competences);
    }

    private function getCompetencesEtudiant(Utilisateur $etudiant)
    {
        return $etudiant->getOrganisation()->getCompetences()->toArray();
    }

    /**
     * Create a skill
     *
     * This route is used to create a skill.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when created.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Competence::class, groups={"Competence"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="compNom", type="string", example="Name"),
     *              @SWG\Property(property="compDescription", type="string", example="Description"),
     *              @SWG\Property( property="competenceParent", type="object",
     *                  @SWG\Property(property="compId", type="integer", example=0 ),
     *              ),
     *              @SWG\Property( property="organisations",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="orgaId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Competences")
     * @Security(name="Bearer")
     *
     * @Route(name="competence_new", methods="POST")
     * @Autorisation("CreateCompetences")
     */
    public function new(Request $request, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Competence::class);
        $competence = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Competence', 'json');

        //Recuperation de la competence parent si existant
        if ($competence->getCompetenceParent()->getCompId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($competence->getCompetenceParent()->getCompId(), Competence::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $competenceParent = $result;
        }
        $competence->setCompetenceParent($competenceParent ?? null);

        // Relation organisation
        $organisations = new ArrayCollection();
        foreach ($competence->getOrganisations() as $emptyOrganisation)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyOrganisation->getOrgaId(), Organisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $organisation = $result;
            isset($organisation) ? $organisations->add($organisation) : null;
        }
        isset($organisations) ? $competence->setOrganisations($organisations->toArray()) : null;

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->persist($competence);
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($competence), Response::HTTP_CREATED, [], true);
    }

    /**
     * Get one skill
     *
     * This route is used to display one given skill.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the requested skill.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Competence::class, groups={"Competence"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no skill."
     * )
     * @SWG\Tag(name="Competences")
     * @Security(name="Bearer")
     *
     * @Route("/{compId}", name="competence_show", methods="GET", requirements={"compId"="\d+"})
     * @Autorisation("ReadCompetences")
     */
    public function show(Competence $competence): Response
    {
        $json = YoubymeUtil::toJson($competence);

        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    /**
     * Update a skill
     *
     * This route is used to update an existing skill.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when updated.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Competence::class, groups={"Competence"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no skill."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="compNom", type="string", example="Name"),
     *              @SWG\Property(property="compDescription", type="string", example="Description"),
     *              @SWG\Property( property="competenceParent", type="object",
     *                  @SWG\Property(property="compId", type="integer", example=0 ),
     *              ),
     *              @SWG\Property( property="organisations",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="orgaId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Competences")
     * @Security(name="Bearer")
     *
     * @Route("/{compId}", name="competence_edit", methods="PUT", requirements={"compId"="\d+"})
     * @Autorisation("UpdateCompetences")
     */
    public function edit(Request $request, Competence $competence, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {   //FIXME
        $requestContent = preg_replace('/.*\"self\".*/', "",$request->getContent());
        $request->initialize($request->query->all(), $request->request->all(), $request->attributes->all(),
            $request->cookies->all(), $request->files->all(), $request->server->all(), $requestContent);

        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Competence::class);
        $competenceUpdated = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Competence', 'json');
        $competence->setCompNom($competenceUpdated->getCompNom());
        $competence->setCompDescription($competenceUpdated->getCompDescription());

        // Relation compétence parent
        if ($competenceUpdated->getCompetenceParent()->getCompId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($competenceUpdated->getCompetenceParent()->getCompId(), Competence::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $competenceParent = $result;
        }
        $competence->setCompetenceParent($competenceParent ?? null);

        //$badge = null;
        //if ($competenceUpdated->getBadge()->getBadgId() !== null)
        //{
        //    $badge = $em->find(Badge::class, $competenceUpdated->getBadge()->getBadgId());
        //}
        //$competence->setBadge($badge);

        // Relation organisation
        $organisations = new ArrayCollection();
        foreach ($competence->getOrganisations() as $emptyOrganisation)
        {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyOrganisation->getOrgaId(), Organisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $organisation = $result;
            isset($organisation) ? $organisations->add($organisation) : null;
        }
        isset($organisations) ? $competence->setOrganisations($organisations->toArray()) : null;

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->flush();
        return new JsonResponse(YoubymeUtil::toJson($competence),Response::HTTP_ACCEPTED, [], true);
    }

    /**
     * Delete a skill
     *
     * This route is used to delete an existing skill.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when deleted."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no skill."
     * )

     * @SWG\Tag(name="Competences")
     * @Security(name="Bearer")
     *
     * @Route("/{compId}", name="competence_delete", methods="DELETE", requirements={"compId"="\d+"})
     * @Autorisation("DeleteCompetences")
     */
    public function delete(Request $request, Competence $competence, EntityManagerInterface $em): Response
    {
        //if ($this->isCsrfTokenValid('delete'.$organisation->getOrgaId(), $request->request->get('_token'))) {
        $em->remove($competence);
        $em->flush();
        //}

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
