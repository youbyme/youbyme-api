<?php

namespace App\Controller;

use App\Annotation\Autorisation;
use App\Entity\Competence;
use App\Entity\HistoriquePeriode;
use App\Entity\Utilisateur;
use App\Entity\Organisation;
use App\Entity\Regle;
use App\Entity\TypeOrga;
use App\Util\HandleAPIExceptionUtil;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use function MongoDB\BSON\toJSON;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Util\YoubymeUtil;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * @Route("/organisations")
 */
class OrganisationController extends AbstractController
{
    /**
     * Get the list of all organizations
     *
     * This route is used to display all the organizations.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the organizations.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Organisation::class, groups={"Organisation"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there are no organizations."
     * )
     * @SWG\Tag(name="Organisations")
     * @Security(name="Bearer")
     *
     * @Route(name="organisation_index", methods="GET")
     * @Autorisation("ReadOrganisations")
     */
    public function index(Request $request): Response
    {
        $organisations = $this->getUser()->getOrganisationsGeres()->toArray();
        $jsonArray = YoubymeUtil::toJson($organisations);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    /**
     * Create an organization
     *
     * This route is used to create an organization.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when created.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Organisation::class, groups={"Organisation"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="orgaNom", type="string", example="Name"),
     *              @SWG\Property(property="orgaAdreRue", type="string", example="Address"),
     *              @SWG\Property(property="orgaAdreNumero", type="string", example="Number"),
     *              @SWG\Property(property="orgaAdreComplement", type="string", example="Additional address"),
     *              @SWG\Property(property="orgaAdreVille", type="string", example="City"),
     *              @SWG\Property(property="orgaAdreCp", type="string", example="00000"),
     *              @SWG\Property(property="orgaAdrePays", type="string", example="Country"),
     *              @SWG\Property(property="orgaTel", type="string", example="0102030405"),
     *              @SWG\Property(property="orgaMail", type="string", example="mail@mail.com"),
     *              @SWG\Property(property="orgaSiret", type="string", example="SIRET"),
     *              @SWG\Property( property="orgaParent", type="Organisation::class",
     *                  @SWG\Property(property="orgaId", type="integer", example=0 ),
     *              ),
     *              @SWG\Property( property="typeOrga", type="TypeOrga::class",
     *                  @SWG\Property(property="typeOrgaId", type="integer", example=0 ),
     *              ),
     *              @SWG\Property( property="regles",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Regle::class",
     *                      @SWG\Property(property="reglId", type="integer", example=0 ),
     *                  )
     *              ),
     *                   @SWG\Property( property="competences",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Competence::class",
     *                      @SWG\Property(property="compId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Organisations")
     * @Security(name="Bearer")
     *
     * @Route(name="organisation_new", methods="POST")
     * @Autorisation("CreateOrganisations")
     */
    public function new(Request $request, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Organisation::class);
        $organisation = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Organisation', 'json');

        // Association des Règles
        $regles = new ArrayCollection();
        foreach ($organisation->getRegles() as $emptyRegle) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyRegle->getReglId(), Regle::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $regle = $result;
            isset($regle) ? $regle->addOrganisation($organisation) : null;
            isset($regle) ? $regles->add($regle) : null;
        }
        isset($regles) ? $organisation->setRegles($regles->toArray()) : null;

        $utilisateurs = new ArrayCollection();
        foreach ($organisation->getUtilisateurs() as $emptyUtilisateur) {
            $result = HandleAPIExceptionUtil::entityExist($emptyUtilisateur->getUtilId(), Utilisateur::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $utilisateur = $result;
            isset($utilisateur) ? $utilisateur->setOrganisation($organisation) : null;
            isset($utilisateur) ? $utilisateurs->add($utilisateur) : null;
        }
        //FIXME not relevant
        isset($utilisateurs) ? $organisation->setUtilisateurs($utilisateurs->toArray()) : null;

        // Associations des Compétencess
        $competences = new ArrayCollection();
        foreach ($organisation->getCompetences() as $emptyCompetence) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyCompetence->getCompId(), Competence::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $competence = $result;
            isset($competence) ? $competence->addOrganisation($organisation) : null;
            isset($competence) ? $competences->add($competence) : null;
        }
        isset($competences) ? $organisation->setCompetences($competences->toArray()) : null;

        // Association de l'Organisation parent
        if ($organisation->getOrgaParent() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($organisation->getOrgaParent()->getOrgaId(), Organisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $organisationParent = $result;
        }
        $organisation->setOrgaParent($organisationParent ?? null);

        $administrateurs = new ArrayCollection();
        foreach ($organisation->getAdministrateurs() as $emptyAdmin) {
            $result = HandleAPIExceptionUtil::entityExist($emptyAdmin->getUtilId(), Utilisateur::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $admin = $result;
            isset($admin) ? $admin->addOrganisationGere($organisation) : null;
            isset($admin) ? $administrateurs->add($admin) : null;
        }
        $organisation->setAdministrateurs($administrateurs->toArray());

        // Association du Type organisation
        if ($organisation->getTypeOrga() !== null && $organisation->getTypeOrga()->getTypeOrgaId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($organisation->getTypeOrga()->getTypeOrgaId(), TypeOrga::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $typeOrga = $result;
        }
        $organisation->setTypeOrga($typeOrga ?? null);

        //heritage de l'organisation parent
        if ($organisation->getOrgaParent() != null) {
            foreach ($organisation->getOrgaParent()->getCompetences() as $competenceParent) {
                ($organisation->addCompetence($competenceParent));
            }
            foreach ($organisation->getOrgaParent()->getRegles() as $regleParent) {
                ($organisation->addRegle($regleParent));
            }
        }

        // administrateur par defaut
        if ($organisation->getAdministrateurs() == null || count($organisation->getAdministrateurs()) <= 0) {
            // set calling user as administrateur
            $this->getUser()->addOrganisationGere($organisation);
            $organisation->setAdministrateurs(array($this->getUser()));
        }

        $organisation->setDernierReset('now');

        // Gestion des erreurs avant insertion en BDD
        if (isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->persist($organisation);
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($organisation), Response::HTTP_CREATED, [], true);
    }

    /**
     * Get one organization
     *
     * This route is used to display one given organization.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the requested organization.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Organisation::class, groups={"Organisation"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no organization."
     * )
     * @SWG\Tag(name="Organisations")
     * @Security(name="Bearer")
     *
     * @Route("/{orgaId}", name="organisation_show", methods="GET", requirements={"orgaId"="\d+"})
     * @Autorisation("ReadOrganisations")
     */
    public function show(Organisation $organisation): Response
    {
        $json = YoubymeUtil::toJson($organisation);

        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    /**
     * Update an organization
     *
     * This route is used to update an existing organization.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when updated.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Organisation::class, groups={"Organisation"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no organization."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="orgaNom", type="string", example="Name"),
     *              @SWG\Property(property="orgaAdreRue", type="string", example="Address"),
     *              @SWG\Property(property="orgaAdreNumero", type="string", example="Number"),
     *              @SWG\Property(property="orgaAdreComplement", type="string", example="Additional address"),
     *              @SWG\Property(property="orgaAdreVille", type="string", example="City"),
     *              @SWG\Property(property="orgaAdreCp", type="string", example="00000"),
     *              @SWG\Property(property="orgaAdrePays", type="string", example="Country"),
     *              @SWG\Property(property="orgaTel", type="string", example="0102030405"),
     *              @SWG\Property(property="orgaMail", type="string", example="mail@mail.com"),
     *              @SWG\Property(property="orgaSiret", type="string", example="SIRET"),
     *              @SWG\Property( property="orgaParent", type="Organisation::class",
     *                  @SWG\Property(property="orgaId", type="integer", example=0 ),
     *              ),
     *              @SWG\Property( property="typeOrga", type="TypeOrga::class",
     *                  @SWG\Property(property="typeOrgaId", type="integer", example=0 ),
     *              ),
     *              @SWG\Property( property="regles",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Regle::class",
     *                      @SWG\Property(property="reglId", type="integer", example=0 ),
     *                  )
     *              ),
     *                   @SWG\Property( property="competences",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="Competence::class",
     *                      @SWG\Property(property="compId", type="integer", example=0 ),
     *                  )
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Organisations")
     * @Security(name="Bearer")
     *
     * @Route("/{orgaId}", name="organisation_edit", methods="PUT", requirements={"orgaId"="\d+"})
     * @Autorisation("UpdateOrganisations")
     */
    public function edit(Request $request, Organisation $organisation, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {   //FIXME
        $requestContent = preg_replace('/.*\"self\".*/', "",$request->getContent());
        $request->initialize($request->query->all(), $request->request->all(), $request->attributes->all(),
            $request->cookies->all(), $request->files->all(), $request->server->all(), $requestContent);

        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Organisation::class);

        $organisationUpdated = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Organisation', 'json');
        $organisation->setOrgaNom($organisationUpdated->getOrgaNom());
        $organisation->setOrgaAdreRue($organisationUpdated->getOrgaAdreRue());
        $organisation->setOrgaAdreNumero($organisationUpdated->getOrgaAdreNumero());
        $organisation->setOrgaAdreComplement($organisationUpdated->getOrgaAdreComplement());
        $organisation->setOrgaAdreVille($organisationUpdated->getOrgaAdreVille());
        $organisation->setOrgaAdreCp($organisationUpdated->getOrgaAdreCp());
        $organisation->setOrgaAdrePays($organisationUpdated->getOrgaAdrePays());
        $organisation->setOrgaTel($organisationUpdated->getOrgaTel());
        $organisation->setOrgaMail($organisationUpdated->getOrgaMail());
        $organisation->setOrgaSiret($organisationUpdated->getOrgaSiret());


        // Associations des règles
        $regles = new ArrayCollection();
        //suppression des ancielles liaisons des regles
        foreach ($organisation->getRegles() as $emptyRegle) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyRegle->getReglId(), Regle::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $regle = $result;
            isset($regle) ? $regle->removeOrganisation($organisation) : null;
        }
        foreach ($organisationUpdated->getRegles() as $emptyRegle) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyRegle->getReglId(), Regle::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $regle = $result;
            isset($regle) ? $regle->addOrganisation($organisation) : null;
            isset($regle) ? $regles->add($regle) : null;
        }
        isset($regles) ? $organisation->setRegles($regles->toArray()) : null;

        $utilisateurs = new ArrayCollection();
        foreach ($organisation->getUtilisateurs() as $emptyUtilisateur) {
            $utilisateur = $em->find(Utilisateur::class, $emptyUtilisateur->getUtilId());
            $utilisateur->setOrganisation(null);
        }
        foreach ($organisationUpdated->getUtilisateurs() as $emptyUtilisateur) {
            $result = HandleAPIExceptionUtil::entityExist($emptyUtilisateur->getUtilId(), Utilisateur::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $utilisateur = $result;
            isset($utilisateur) ? $utilisateur->setOrganisation($organisation) : null;
            isset($utilisateur) ? $utilisateurs->add($utilisateur) : null;
        }
        isset($utilisateurs) ? $organisation->setUtilisateurs($utilisateurs->toArray()) : null;

        $administrateurs = new ArrayCollection();
        foreach ($organisation->getAdministrateurs() as $oldAdmin) {
            $oldAdmin->removeOrganisationGere($organisation);
        }
        foreach ($organisationUpdated->getAdministrateurs() as $emptyAdmin) {
            $result = HandleAPIExceptionUtil::entityExist($emptyAdmin->getUtilId(), Utilisateur::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $admin = $result;
            isset($admin) ? $admin->addOrganisationGere($organisation) : null;
            isset($admin) ? $administrateurs->add($admin) : null;
        }
        $organisation->setAdministrateurs($administrateurs->toArray());

        // Associations des compétencess
        $competences = new ArrayCollection();
        //suppression des ancielles liaisons des competences
        foreach ($organisation->getCompetences() as $emptyCompetence) {
            //FIXME wtf
            $result = HandleAPIExceptionUtil::entityExist($emptyCompetence->getCompId(), Competence::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $competence = $result;
            isset($competence) ? $competence->removeOrganisation($organisation) : null;
        }
        foreach ($organisationUpdated->getCompetences() as $emptyCompetence) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($emptyCompetence->getCompId(), Competence::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $competence = $result;
            isset($competence) ? $competence->addOrganisation($organisation) : null;
            isset($competence) ? $competences->add($competence) : null;
        }
        isset($competences) ? $organisation->setCompetences($competences->toArray()) : null;

        // Association de l'Organisation parent
        if ($organisationUpdated->getOrgaParent() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($organisationUpdated->getOrgaParent()->getOrgaId(), Organisation::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $organisationParent = $result;
        }
        $organisation->setOrgaParent($organisationParent ?? null);

        // Association du Type orga
        if ($organisationUpdated->getTypeOrga()->getTypeOrgaId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($organisationUpdated->getTypeOrga()->getTypeOrgaId(), TypeOrga::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $typeOrga = $result;
        }
        $organisation->setTypeOrga($typeOrga ?? null);

        // Gestion des erreurs avant insertion en BDD
        if (isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->flush();
        return new JsonResponse(YoubymeUtil::toJson($organisation), Response::HTTP_ACCEPTED, [], true);
    }

    /**
     * Reset points of users belonging to an organization
     *
     * This route is used to reset points of users belonging to an organization.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when reseted.",
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there are no organisations."
     * )
     *
     * @SWG\Tag(name="Organisations")
     * @Security(name="Bearer")
     *
     * @Route("/{orgaId}/resetPoints", name="reset_points", methods="PUT", requirements={"orgaId"="\d+"})
     * @Autorisation("UpdateUtilisateurs")
     */
    public function reset(Organisation $organisation, EntityManagerInterface $em)
    {
        $utilisateurs = $organisation->getUtilisateurs();
        foreach ($utilisateurs as $utilisateur) {
            $utilisateur->setUtilPointDispo($utilisateurs->count() - 1);
        }

        $organisation->setDernierReset('now');
        $em->flush();
        return new JsonResponse(YoubymeUtil::toJson($organisation), Response::HTTP_ACCEPTED, [], true);
    }

    /**
     * Delete an organization
     *
     * This route is used to delete an existing organization.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when deleted."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no skill."
     * )
     * @SWG\Tag(name="Organisations")
     * @Security(name="Bearer")
     *
     * @Route("/{orgaId}", name="organisation_delete", methods="DELETE", requirements={"orgaId"="\d+"})
     * @Autorisation("DeleteOrganisations")
     */
    public function delete(Request $request, Organisation $organisation, EntityManagerInterface $em): Response
    {
        //if ($this->isCsrfTokenValid('delete'.$organisation->getOrgaId(), $request->request->get('_token'))) {
        $em->remove($organisation);
        $em->flush();
        //}

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
