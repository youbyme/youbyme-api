<?php

namespace App\Controller;

use App\Annotation\Autorisation;
use App\Entity\Competence;
use App\Entity\Regle;
use App\Entity\Utilisateur;
use App\Entity\Vote;
use App\Exception\AlreadyVotedException;
use App\Exception\NoMoreBonusPointsAvailableException;
use App\Exception\NoMorePointsAvailableException;
use App\Util\HandleAPIExceptionUtil;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Util\YoubymeUtil;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Exception\InvalidOptionsException;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Request\ParamFetcher;
use Swagger\Annotations as SWG;

/**
 * @Route("/votes")
 */
class VoteController extends AbstractController
{
    /**
     * Get the list of all votes
     *
     * This route is used to display all the votes.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the votes.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Vote::class, groups={"Vote"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there are no votes."
     * )
     * @SWG\Tag(name="Votes")
     * @Security(name="Bearer")
     *
     * @QueryParam(name="utilisateurEmetteur")
     * @QueryParam(name="utilisateurReceveur")
     * @QueryParam(name="startDate", requirements="\d{4}-\d{2}-\d{2}")
     * @QueryParam(name="endDate", requirements="\d{4}-\d{2}-\d{2}")
     *
     * @Route(name="vote_index", methods="GET")
     * @Autorisation("ReadVotes")
     */


    public function index(Request $request, ParamFetcher $paramFetcher, EntityManagerInterface $em): Response
    {
        $params = array_intersect_assoc($paramFetcher->all(), $request->query->all());
        $startDate = new \DateTime('1970-01-01');
        $endDate = new \DateTime('now');

        if (array_key_exists('startDate', $params)) {
            $startDate = new \DateTime($params['startDate']);
            unset($params['startDate']);
        }
        if (array_key_exists('endDate', $params)) {
            $endDate = new \DateTime($params['endDate']);
            unset($params['endDate']);
        }

        $votesFiltresDate = $this->getDoctrine()->getRepository(Vote::class)
            ->getVotesDateRanged($startDate, $endDate);

        $votesFiltres = $this->getDoctrine()
            ->getRepository(Vote::class)
            ->findBy($params);

        $votesEtudiants = array();
        foreach ($this->getUser()->getEtudiants() as $etud)
        {
            foreach($etud->getVotesEmis()->toArray() as $votesEmis)
            {
                array_push($votesEtudiants, $votesEmis);
            }
        }

        $votes = array_values(array_uintersect($votesFiltres, $votesEtudiants, $votesFiltresDate, function ($a, $b)
        {
            return strcmp(spl_object_hash($a), spl_object_hash($b));
        }));

        $jsonArray = YoubymeUtil::toJson($votes);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }


    /**
     * Create a vote
     *
     * This route is used to create a vote.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when created.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Vote::class, groups={"Vote"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="type", type="integer", example=0),
     *              @SWG\Property( property="utilisateurEmetteur", type="object",
     *                  @SWG\Property(property="utilId", type="integer", example=0 ),
     *              ),
     *              @SWG\Property( property="utilisateurReceveur", type="object",
     *                  @SWG\Property(property="utilId", type="integer", example=0 ),
     *              ),
     *              @SWG\Property( property="competence", type="object",
     *                  @SWG\Property(property="compId", type="integer", example=0 ),
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Votes")
     * @Security(name="Bearer")
     * @Autorisation("CreateVotes")
     * @Route(name="vote_new", methods="POST")
     * @throws AlreadyVotedException
     * @throws NoMoreBonusPointsAvailableException
     * @throws NoMorePointsAvailableException
     */
    public function new(Request $request, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {

        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Vote::class);
        $vote = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Vote', 'json');
        // forbidden if in WEEK-END
        if ($this->isWeekend())
            throw new \Exception("You cannot vote during the week-end");
        // Association du Utilisateur emetteur
        if ($vote->getUtilisateurEmetteur()->getUtilId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($vote->getUtilisateurEmetteur()->getUtilId(), Utilisateur::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $utilEmet = $result;
        }
        isset($utilEmet) ? $vote->setUtilisateurEmetteur($utilEmet) : null;

        // Association du Utilisateur récepteur
        if ($vote->getUtilisateurReceveur()->getUtilId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($vote->getUtilisateurReceveur()->getUtilId(), Utilisateur::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $utilRece = $result;
        }
        isset($utilRece) ? $vote->setUtilisateurReceveur($utilRece) : null;

        // Association de la compétence
        if ($vote->getCompetence()->getCompId() !== null) {
            // Util pour tester les associations
            $result = HandleAPIExceptionUtil::entityExist($vote->getCompetence()->getCompId(), Competence::class, $em);
            \is_string($result) ? $arrExecption[] = $result : $competence = $result;
        }
        isset($competence) ? $vote->setCompetence($competence) : null;

        if (!$competence->getCompetenceParent())
            throw new \Exception("This skill is parent.");

        $vote->setDate('now');

        if(isset($utilEmet)) {
        if ($vote->getType() == Vote::CLASSIC) {
            if ($utilEmet->getUtilPointDispo() == 0)
                throw new NoMorePointsAvailableException("Vous n'avez plus de points disponibles.");
            if ($em->getRepository(Vote::class)->hasAlreadyVotedFor($vote->getUtilisateurEmetteur()->getUtilId(),
                $vote->getUtilisateurReceveur()->getUtilId(),
                $vote->getUtilisateurEmetteur()->getOrganisation()->getDernierReset()))
                throw new AlreadyVotedException("Vous avez déja voté pour cet utilisateur ". $utilRece->getUtilNom() . ' ' . $utilRece->getUtilPrenom() . " pendant cette période.");

            $utilEmet->setUtilPointDispo($utilEmet->getUtilPointDispo() - 1);
        }
        else if ($vote->getType() == Vote::BONUS) {
            if ($utilEmet->getUtilPointBonusDispo() == 0)
                throw new NoMoreBonusPointsAvailableException("Vous n'avez plus de point bonus disponibles");

            $utilEmet->setUtilPointBonusDispo($utilEmet->getUtilPointBonusDispo() - 1);
        }
    }

        // Gestion des erreurs avant insertion en BDD
        if(isset($arrExecption))
            throw new InvalidOptionsException("Il y a des erreurs dans votre requête.", $arrExecption);

        // Ajout en base
        $em->persist($vote);
        $em->flush();
        $this->checkBadgeEligibility($utilRece, $vote->getCompetence(), $em);

        return new JsonResponse(YoubymeUtil::toJson($vote), Response::HTTP_CREATED, [], true);
    }

    /**
     * Get one vote
     *
     * This route is used to display one given vote.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the requested vote.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Vote::class, groups={"Vote"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no vote."
     * )
     * @SWG\Tag(name="Votes")
     * @Security(name="Bearer")
     *
     * @Route("/{voteId}", name="vote_show", methods="GET", requirements={"voteId"="\d+"})
     * @Autorisation("ReadVotes")
     */
    public function show(Vote $vote): Response
    {
        $json = YoubymeUtil::toJson($vote);

        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    // public function edit(Request $request, Vote $vote, EntityManagerInterface $em, ValidatorInterface $validator): Response
    /**
     * Validate/Invalidate a vote
     *
     * This route is used to validate or invalidate a vote.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when the validity of a vote is changed.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Vote::class, groups={"Vote"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="valide", type="boolean", example=true),
     *              @SWG\Property(property="type", type="integer", example=1),
     *              )
     *          )
     *      )
     * @SWG\Tag(name="Votes")
     * @Security(name="Bearer")
     *
     * @Route("/{voteId}", name="vote_valid", methods="PUT", requirements={"voteId"="\d+"})
     * @Autorisation("UpdateVotes")
     */
    public function setValidation(Request $request, Vote $vote, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, Vote::class);
        $voteUpdated = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\Vote', 'json');
        /*$utilEmet = $em->find(Utilisateur::class, $voteUpdated->getUtilisateurEmetteur()->getUtilId());
        $utilRece = $em->find(Utilisateur::class, $voteUpdated->getUtilisateurReceveur()->getUtilId());
        $competence = $em->find(Competence::class, $voteUpdated->getCompetence()->getCompId());*/

        $vote->setValide($voteUpdated->isValide());
        //changement de validité du vote
        if ($vote->isValide() !== $voteUpdated->isValide())
        {
            //pas prioritaire selon la doc
            //TODO update points & badges ?
        }

        // Ajout en base
        $em->flush();
        return new JsonResponse(YoubymeUtil::toJson($vote),  Response::HTTP_ACCEPTED, [], true);
    }


    /**
     * Check if a student eligible for a badge
     */
    private function checkBadgeEligibility(Utilisateur $utilisateur, Competence $competence, EntityManagerInterface $em)
    {
        $badge = $competence->getBadge($em);
        if ($badge != null)
        {
            $pointsMin = $badge->getNbPointsRequis();
            if ($pointsMin == null)
            {       //On va chercher la règle correspondante pour obtenir la valeur par défaut
                $pointsMin = $utilisateur->getOrganisation()->getRegle(Regle::NB_POINTS_MIN_BADGE);
            }
            $nbVotesValides = $em->getRepository(Vote::class)->countValidVotes($utilisateur->getUtilId(), $competence->getCompId());

            if (!in_array($badge, $utilisateur->getBadges()) && $nbVotesValides >= $pointsMin)
            {
                $utilisateur->assignBadge($badge, $em);
                $this->checkBadgeParentEligibility($utilisateur, $competence->getCompetenceParent(), $em);
            } else if (in_array($badge, $utilisateur->getBadges()) && $nbVotesValides < $pointsMin)
            {       //Il se peut qu'un vote ayant été invalidé, l'utilisateur n'est plus eligible au badge correspondant à la competence
                $utilisateur->removeBadge($badge, $em);
            }
        }
    }

    /**
     * Check if a student is eligible for a badge category, after received all the child badges
     */
    private function checkBadgeParentEligibility(Utilisateur $utilisateur, Competence $competence, EntityManagerInterface $em)
    {
        foreach ($competence->getEnfants()->toArray() as $compEnfant)
        {
            $badge = $compEnfant->getBadge();
            //Si l'utilisateur possède les badges des competences enfants
            if (in_array($badge, $utilisateur->getBadges()))
            {
                //Alors on lui attribue le badge parent
                $utilisateur->assignBadge($competence->getBadge(), $em);
            }
        }
    }

    /**
     * Return if current time is in week-end or not.
     */
    private function isWeekend()
    {
        return (date('N', strtotime('now')) >= 6);

    }

}
