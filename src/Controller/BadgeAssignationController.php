<?php
/**
 * Created by PhpStorm.
 * User: I341379
 * Date: 24/01/2019
 * Time: 18:08
 */

namespace App\Controller;


use App\Annotation\Autorisation;
use App\Entity\BadgeAssignation;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Util\YoubymeUtil;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\Security;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("/badgeassignations")
 */
class BadgeAssignationController extends AbstractController
{

    /**
     * Get the list of all assignations of badges
     *
     * This route is used to display all assignations of badges.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all badge's assignations."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there are no badge's assignations."
     * )
     * @SWG\Tag(name="Badge's assignations")
     * @Security(name="Bearer")
     *
     * @Route(name="index", methods="GET")
     * @QueryParam(name="utilisateur")
     * @QueryParam(name="badge")
     * @QueryParam(name="valide", requirements="0|1")
     * @Autorisation("ReadBadgeAssignations")
     */

    public function index(Request $request, ParamFetcher $paramFetcher): Response
    {   //TODO refactor this
        $params = array_intersect_assoc($paramFetcher->all(), $request->query->all());
        $assignationsFiltres = $this->getDoctrine()
        ->getRepository(BadgeAssignation::class)
        ->findBy($params);

        $user = $this->getUser();
        if (YoubymeUtil::isPilote($user))
        {
            $assignationsLimites = $this->getAssignationsPilote($user);
        } else {
            $assignationsLimites = $this->getAssignationsEtudiant($user);
        }

        $badgesAssign = array_values(array_uintersect(array_values($assignationsLimites), array_values($assignationsFiltres), function ($a, $b)
        {
            return strcmp(spl_object_hash($a), spl_object_hash($b));
        }));

        $jsonArray = YoubymeUtil::toJson($badgesAssign);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    private function getAssignationsPilote($pilote)
    {
        $badgesAssignEtud = array();
        foreach ($this->getUser()->getEtudiants() as $etud)
        {
            foreach ($etud->getAttributions() as $badge)
            {
                array_push($badgesAssignEtud, $badge);
            }
        }
        return $badgesAssignEtud;
    }

    private function getAssignationsEtudiant($etudiant)
    {
        return $this->getDoctrine()->getRepository(BadgeAssignation::class)->findAll();
    }



    /**
     * Validate a badge assignation
     *
     * This route is used to validate  an existing badge assignation.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when validated.",
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no badge assignation."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Tag(name="Badge's assignations")
     * @Security(name="Bearer")
     *
     * @Route("/{utilisateur}/{badge}", name="validate", methods="PUT", requirements={"utilisateur"="\d+", "badge"="\d+"})
     * @Autorisation("UpdateBadgeAssignations")
     */
    public function validate(BadgeAssignation $assignation, EntityManagerInterface $em): Response
    {
        $assignation->setValide(true);

        // Ajout en base
        $em->flush();
        return new JsonResponse(YoubymeUtil::toJson($assignation), Response::HTTP_ACCEPTED, [], true);
    }

    /**
     * Invalidate a badge assignation
     *
     * This route is used to invalidate an existing badge assignation.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when validated.",
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no badge assignation."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Tag(name="Badge's assignations")
     * @Security(name="Bearer")
     *
     * @Route("/{utilisateur}/{badge}/cancel", name="invalidate", methods="PUT", requirements={"utilisateur"="\d+", "badge"="\d+"})
     * @Autorisation("UpdateBadgeAssignations")
     */
    public function invalidate(BadgeAssignation $assignation, EntityManagerInterface $em): Response
    {
        $assignation->setValide(false);

        // Ajout en base
        $em->flush();
        return new JsonResponse(YoubymeUtil::toJson($assignation), Response::HTTP_ACCEPTED, [], true);
    }
}