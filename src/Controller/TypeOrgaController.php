<?php

namespace App\Controller;

use App\Annotation\Autorisation;
use App\Entity\TypeOrga;
use App\Util\HandleAPIExceptionUtil;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Util\YoubymeUtil;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

/**
 * @Route("/typeorgas")
 */
class TypeOrgaController extends AbstractController
{
    /**
     * Get the list of all organization types
     *
     * This route is used to display all the organization types.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting all the organization types.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=TypeOrga::class, groups={"TypeOrga"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there are no organization types."
     * )
     * @SWG\Tag(name="Types organisations")
     * @Security(name="Bearer")
     *
     * @Route(name="typeOrga_index", methods="GET")
     * @Autorisation("ReadTypeOrgas")
     */
    public function index(): Response
    {
        $typeOrganisations = $this->getDoctrine()
            ->getRepository(TypeOrga::class)
            ->findAll();

        $jsonArray = YoubymeUtil::toJson($typeOrganisations);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }

    /**
     * Create an organization type
     *
     * This route is used to create an organization type.
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returned when created.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=TypeOrga::class, groups={"TypeOrga"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="typeOrgaNom", type="string", example="Name")
     *          )
     * )
     * @SWG\Tag(name="Types organisations")
     * @Security(name="Bearer")
     *
     * @Route(name="typeOrga_new", methods="POST")
     * @Autorisation("CreateTypeOrgas")
     */
    public function new(Request $request, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, TypeOrga::class);
        $typeOrga = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\TypeOrga', 'json');
        $em->persist($typeOrga);
        $em->flush();

        return new JsonResponse(YoubymeUtil::toJson($typeOrga), Response::HTTP_CREATED, [], true);
    }

    /**
     * Get one organization type
     *
     * This route is used to display one given organization type.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the requested organization type.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=TypeOrga::class, groups={"TypeOrga"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no organization type."
     * )
     * @SWG\Tag(name="Types organisations")
     * @Security(name="Bearer")
     *
     * @Route("/{typeOrgaId}", name="typeorga_show", methods="GET", requirements={"typeOrgaId"="\d+"})
     * @Autorisation("ReadTypeOrgas")
     */
    public function show(TypeOrga $typeOrga): Response
    {
        $json = YoubymeUtil::toJson($typeOrga);

        return new JsonResponse($json, Response::HTTP_OK, [], true);
    }

    /**
     * Update an organization type
     *
     * This route is used to update an existing organization type.
     *
     * @SWG\Response(
     *     response=202,
     *     description="Returned when updated.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=TypeOrga::class, groups={"TypeOrga"}))
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Returned when Il y a des erreurs dans votre requête."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no organization type."
     * )
     * @SWG\Response(
     *     response=406,
     *     description="Returned when the contentType of your request is not required json type."
     * )
     * @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="typeOrgaNom", type="string", example="Name")
     *          )
     * )
     * @SWG\Tag(name="Types organisations")
     * @Security(name="Bearer")
     *
     * @Route("/{typeOrgaId}", name="typeorga_edit", methods="PUT", requirements={"typeOrgaId"="\d+"})
     * @Autorisation("UpdateTypeOrgas")
     */
    public function edit(Request $request, TypeOrga $typeOrga, EntityManagerInterface $em, ValidatorInterface $validator): Response
    {
        // Util pour tester la requête et l'entité avec des méthodes spécifiques
        HandleAPIExceptionUtil::isRequestValid($request, $this, $validator, TypeOrga::class);
       $typeOrgaUpdated = $this->get('serializer')->deserialize($request->getContent(), 'App\Entity\TypeOrga', 'json');
       $typeOrga->setTypeOrgaNom($typeOrgaUpdated->getTypeOrgaNom());

        $em->flush();
        return new JsonResponse(YoubymeUtil::toJson($typeOrga),  Response::HTTP_ACCEPTED, [], true);
    }

    /**
     * Delete an organization type
     *
     * This route is role for delete an existing organization type.
     *
     * @SWG\Response(
     *     response=204,
     *     description="Returned when deleted."
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Response(
     *     response=404,
     *     description="Returned when there is no organization type."
     * )

     * @SWG\Tag(name="Types organisations")
     * @Security(name="Bearer")
     *
     * @Route("/{typeOrgaId}", name="typeOrga_delete", methods="DELETE", requirements={"typeOrgaId"="\d+"})
     * @Autorisation("DeleteTypeOrgas")
     */
    public function delete(Request $request, TypeOrga $typeOrga, EntityManagerInterface $em): Response
    {
        //if ($this->isCsrfTokenValid('delete'.$autorisation->getAutoId(), $request->request->get('_token'))) {
        $em->remove($typeOrga);
        $em->flush();
        //}

        return new Response('', Response::HTTP_NO_CONTENT);
    }
}
