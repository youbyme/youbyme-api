<?php

namespace App\Controller;

use App\Annotation\Autorisation;
use App\Entity\HistoriquePeriode;
use App\Entity\Organisation;
use App\Util\YoubymeUtil;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;

class HistoriquePeriodesController extends AbstractController
{
    /**
     * Get the historic of an organisation
     *
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returned when getting the historic",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=HistoriquePeriode::class, groups={"HistoriquePeriode"}))
     *     )
     * )
     * @SWG\Response(
     *     response=401,
     *     description="Returned when you don't use a valid JWT Token"
     * )
     * @SWG\Response(
     *     response=403,
     *     description="You haven't sufficient rights for this request."
     * )
     * @SWG\Tag(name="HistoriquePeriode")
     * @Security(name="Bearer")
     *
     * @Route("/historique", name="historique_periodes", methods="GET", requirements={"typeOrgaId"="\d+"})
     * @Autorisation("ReadOrganisations")
     */
    public function index()
    {
        $pilote = $this->getUser();
        $historiques = array();
        foreach ($pilote->getOrganisationsGeres() as $orga)
        {
            $historiques = array_merge($historiques, $orga->getHistorique()->toArray());
        }
        $jsonArray = YoubymeUtil::toJson($historiques);
        return new JsonResponse($jsonArray, Response::HTTP_OK, [], true);
    }
}
