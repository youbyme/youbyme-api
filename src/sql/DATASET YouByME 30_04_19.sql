-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Client :  db5000038320.hosting-data.io
-- Généré le :  Mar 30 Avril 2019 à 11:30
-- Version du serveur :  5.7.25-log
-- Version de PHP :  7.0.33-0+deb9u3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `dbs33283`
--

-- --------------------------------------------------------

--
-- Structure de la table `AUTORISATION`
--

CREATE TABLE `AUTORISATION` (
  `AUTO_ID` int(10) UNSIGNED NOT NULL,
  `AUTO_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AUTO_DESCRIPTION` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `AUTORISATION`
--

INSERT INTO `AUTORISATION` (`AUTO_ID`, `AUTO_NOM`, `AUTO_DESCRIPTION`) VALUES
(1, 'ReadAutorisations', 'ReadAutorisations'),
(2, 'CreateAutorisations', 'CreateAutorisations'),
(3, 'UpdateAutorisations', 'UpdateAutorisations'),
(4, 'DeleteAutorisations', 'DeleteAutorisations'),
(5, 'ReadBadges', 'ReadBadges'),
(6, 'CreateBadges', 'CreateBadges'),
(7, 'UpdateBadges', 'UpdateBadges'),
(8, 'DeleteBadges', 'DeleteBadges'),
(9, 'ReadCompetences', 'ReadCompetences'),
(10, 'CreateCompetences', 'CreateCompetences'),
(11, 'UpdateCompetences', 'UpdateCompetences'),
(12, 'DeleteCompetences', 'DeleteCompetences'),
(13, 'ReadOrganisations', 'ReadOrganisations'),
(14, 'CreateOrganisations', 'CreateOrganisations'),
(15, 'UpdateOrganisations', 'UpdateOrganisations'),
(16, 'DeleteOrganisations', 'DeleteOrganisations'),
(17, 'ReadRegles', 'ReadRegles'),
(18, 'CreateRegles', 'CreateRegles'),
(19, 'UpdateRegles', 'UpdateRegles'),
(20, 'DeleteRegles', 'DeleteRegles'),
(21, 'ReadRoles', 'ReadRoles'),
(22, 'CreateRoles', 'CreateRoles'),
(23, 'UpdateRoles', 'UpdateRoles'),
(24, 'DeleteRoles', 'DeleteRoles'),
(25, 'ReadTypeOrgas', 'ReadTypeOrgas'),
(26, 'CreateTypeOrgas', 'CreateTypeOrgas'),
(27, 'UpdateTypeOrgas', 'UpdateTypeOrgas'),
(28, 'DeleteTypeOrgas', 'DeleteTypeOrgas'),
(29, 'ReadUtilisateurs', 'ReadUtilisateurs'),
(30, 'CreateUtilisateurs', 'CreateUtilisateurs'),
(31, 'UpdateUtilisateurs', 'UpdateUtilisateurs'),
(32, 'DeleteUtilisateurs', 'DeleteUtilisateurs'),
(33, 'ReadVotes', 'ReadVotes'),
(34, 'CreateVotes', 'CreateVotes'),
(35, 'UpdateVotes', 'UpdateVotes'),
(36, 'DeleteVotes', 'DeleteVotes'),
(37, 'ReadBadgeAssignations', 'ReadBadgeAssignations'),
(38, 'CreateBadgeAssignations', 'CreateBadgeAssignations'),
(39, 'UpdateBadgeAssignations', 'UpdateBadgeAssignations'),
(40, 'DeleteBadgeAssignations', 'DeleteBadgeAssignations'),
(41, 'ShowUtilisateursVotables', 'ShowUtilisateursVotables'),
(42,'ResetVotes','ResetVotes'),
(43,'UseBO','UseBO');

-- --------------------------------------------------------

--
-- Structure de la table `BADGE`
--

CREATE TABLE `BADGE` (
  `BADG_ID` int(11) NOT NULL,
  `BADG_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `BADG_IMAGE_URL` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BADG_DESCRIPTION` text COLLATE utf8mb4_unicode_ci,
  `BADG_EXPIRATION` date DEFAULT NULL,
  `BADG_PREUVE_URL` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BADG_POINTS_REQUIS` int(11) DEFAULT NULL,
  `COMP_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `BADGE`
--

INSERT INTO `BADGE` (`BADG_ID`, `BADG_NOM`, `BADG_IMAGE_URL`, `BADG_DESCRIPTION`, `BADG_EXPIRATION`, `BADG_PREUVE_URL`, `BADG_POINTS_REQUIS`, `COMP_ID`) VALUES
(1, 'Rigueur', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-01.png', 'Badge pour la compétence Rigueur.', NULL, NULL, 10, 1),
(2, 'Autonomie', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-02.png', 'Badge pour la compétence Autonomie.', NULL, NULL, 10, 2),
(3, 'Adaptabilité', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-03.png', 'Badge pour la compétence Adaptabilité.', NULL, NULL, 10, 3),
(4, 'Capacités relationnelles', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-04.png', 'Badge pour la compétence Capacités relationnelles.', NULL, NULL, 10, 4),
(5, 'Esprit d\'équipe', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-05.png', 'Badge pour la compétence Esprit d\'équipe.', NULL, NULL, 10, 5),
(6, 'Curiosité', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-06.png', 'Badge pour la compétence Curiosité.', NULL, NULL, 10, 6),
(7, 'Ouverture d\'esprit', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-07.png', 'Badge pour la compétence Ouverture d\'esprit.', NULL, NULL, 10, 7),
(8, 'Passion', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-08.png', 'Badge pour la compétence Passion.', NULL, NULL, 10, 8),
(9, 'Esprit d\'entrepreneur', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-09.png', 'Badge pour la compétence Esprit d\'entrepreneur.', NULL, NULL, 10, 9),
(10, 'Sens du service client', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-10.png', 'Badge pour la compétence Sens du service client.', NULL, NULL, 10, 10),
(11, 'Leadership', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-11.png', 'Badge pour la compétence Leadership.', NULL, NULL, 10, 11),
(12, 'Efficace', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-12.png', 'Badge pour la compétence Efficace.', NULL, NULL, 0, 12),
(13, 'Bon relationnel', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-13.png', 'Badge pour la compétence Bon relationnel.', NULL, NULL, 0, 13),
(14, 'Créatif', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-14.png', 'Badge pour la compétence Créatif.', NULL, NULL, 0, 14),
(15, 'Force de proposition', 'http://youbyme.nicolas-broquet.fr/assets/badge/badge-15.png', 'Badge pour la compétence Force de proposition.', NULL, NULL, 0, 15);

-- --------------------------------------------------------

--
-- Structure de la table `COMPETENCE`
--

CREATE TABLE `COMPETENCE` (
  `COMP_ID` int(11) NOT NULL,
  `COMP_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `COMP_DESCRIPTION` text COLLATE utf8mb4_unicode_ci,
  `COM_COMP_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `COMPETENCE`
--

INSERT INTO `COMPETENCE` (`COMP_ID`, `COMP_NOM`, `COMP_DESCRIPTION`, `COM_COMP_ID`) VALUES
(1, 'Rigueur', 'Compétence Rigueur', 12),
(2, 'Autonomie', 'Compétence Autonomie', 12),
(3, 'Adaptabilité', 'Compétence Adaptabilité', 12),
(4, 'Capacités relationnelles', 'Compétence Capacités relationnelles', 13),
(5, 'Esprit d\'équipe', 'Compétence Esprit d\'équipe', 13),
(6, 'Curiosité', 'Compétence Curiosité', 14),
(7, 'Ouverture d\'esprit', 'Compétence Ouverture d\'esprit', 14),
(8, 'Passion', 'Compétence Passion', 14),
(9, 'Esprit d\'entrepreneur', 'Compétence Esprit d\'entrepreneur', 15),
(10, 'Sens du service client', 'Compétence Sens du service client', 15),
(11, 'Leadership', 'Compétence Leadership', 15),
(12, 'Efficace', 'Compétence Efficace', NULL),
(13, 'Bon relationnel', 'Compétence Bon relationnel', NULL),
(14, 'Créatif', 'Compétence Créatif', NULL),
(15, 'Force de proposition', 'Compétence Force de proposition', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `CONTENIR_AUTORISATION`
--

CREATE TABLE `CONTENIR_AUTORISATION` (
  `ROLE_ID` int(11) NOT NULL,
  `AUTO_ID` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `CONTENIR_AUTORISATION`
--

INSERT INTO `CONTENIR_AUTORISATION` (`ROLE_ID`, `AUTO_ID`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(1, 26),
(1, 27),
(1, 28),
(1, 29),
(1, 30),
(1, 31),
(1, 32),
(1, 33),
(1, 34),
(1, 35),
(1, 36),
(1, 37),
(1, 38),
(1, 39),
(1, 40),
(1, 41),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 6),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25),
(2, 26),
(2, 27),
(2, 28),
(2, 29),
(2, 30),
(2, 31),
(2, 32),
(2, 33),
(2, 34),
(2, 35),
(2, 36),
(2, 37),
(2, 38),
(2, 39),
(2, 40),
(2, 41),
(3, 5),
(3, 9),
(3, 29),
(3, 41),
(3, 34),
(1,42),
(2,42),
(1,43),
(2,43);

-- --------------------------------------------------------

--
-- Structure de la table `DEFINIR_REGLE`
--

CREATE TABLE `DEFINIR_REGLE` (
  `REGL_ID` int(11) NOT NULL,
  `ORGA_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `DEFINIR_REGLE`
--

INSERT INTO `DEFINIR_REGLE` (`REGL_ID`, `ORGA_ID`) VALUES
(1, 7),
(1, 8),
(1, 9),
(1, 10),
(1, 11),
(1, 12),
(1, 13),
(1, 14),
(1, 15),
(1, 16),
(1, 17),
(1, 18),
(1, 19),
(1, 20),
(1, 21),
(1, 22),
(1, 23),
(1, 24),
(1, 25),
(2, 7),
(2, 8),
(2, 9),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 17),
(2, 18),
(2, 19),
(2, 20),
(2, 21),
(2, 22),
(2, 23),
(2, 24),
(2, 25);

-- --------------------------------------------------------

--
-- Structure de la table `GERER_ORGANISATION`
--

CREATE TABLE `GERER_ORGANISATION` (
  `UTIL_ID` int(11) NOT NULL,
  `ORGA_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `GERER_ORGANISATION`
--

INSERT INTO `GERER_ORGANISATION` (`UTIL_ID`, `ORGA_ID`) VALUES
(1, 1),
(2, 3),
(3, 4),
(3, 16),
(4, 7),
(4, 8),
(4, 9),
(4, 10),
(4, 11),
(4, 12),
(4, 13),
(4, 14),
(4, 15),
(3, 16),
(3, 17),
(3, 18),
(3, 19),
(3, 20),
(3, 21),
(3, 22),
(3, 23),
(3, 24),
(3, 25);


-- --------------------------------------------------------

--
-- Structure de la table `OBTENIR_ROLE`
--

CREATE TABLE `OBTENIR_ROLE` (
  `ROLE_ID` int(11) NOT NULL,
  `UTIL_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `OBTENIR_ROLE`
--

INSERT INTO `OBTENIR_ROLE` (`ROLE_ID`, `UTIL_ID`) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 5),
(3, 6),
(3, 7),
(3, 8),
(3, 9),
(3, 10),
(3, 11),
(3, 12),
(3, 13),
(3, 14),
(3, 15),
(3, 16),
(3, 17),
(3, 18),
(3, 19);

-- --------------------------------------------------------

--
-- Structure de la table `ORGANISATION`
--

CREATE TABLE `ORGANISATION` (
  `ORGA_ID` int(11) NOT NULL,
  `ORGA_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ORGA_ADRE_RUE` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_ADRE_NUMERO` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_ADRE_COMPLEMENT` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_ADRE_VILLE` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_ADRE_CP` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_ADRE_PAYS` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_TEL` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_MAIL` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_SIRET` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORG_ORGA_ID` int(11) DEFAULT NULL,
  `TYPEORGA_ID` int(11) NOT NULL,
  `ORGA_CODE_ANALYTICS` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_DATE_RESET` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `ORGANISATION`
--

INSERT INTO `ORGANISATION` (`ORGA_ID`, `ORGA_NOM`, `ORGA_ADRE_RUE`, `ORGA_ADRE_NUMERO`, `ORGA_ADRE_COMPLEMENT`, `ORGA_ADRE_VILLE`, `ORGA_ADRE_CP`, `ORGA_ADRE_PAYS`, `ORGA_TEL`, `ORGA_MAIL`, `ORGA_SIRET`, `ORG_ORGA_ID`, `TYPEORGA_ID`, `ORGA_CODE_ANALYTICS`) VALUES
(1, 'ROOT', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@youbyme.fr', NULL, NULL, 1, NULL),
(2, 'CESI', 'Avenue du Général de Gaulle', '1', 'Tour pb5', 'Puteaux', '92800', 'France', NULL, 'contact@cesi.fr', '34270750200684', 1, 2, NULL),
(3, 'CESI Nord-Ouest', 'Avenue du Général de Gaulle', '2', 'Tour pb6', 'Puteaux', '92801', 'France', NULL, 'contact@cesi.fr', '34270750200684', 2, 3, NULL),
(4, 'CESI Mont-Saint-Aignan', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 3, 4, NULL),
(5, 'Informatique & Numérique', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 4, 5, NULL),
(6, 'Ressources Humaines', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 4, 5, NULL),
(7, 'RIL', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 5, 6, 'RO754208'),
(8, 'RIL17', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 7, 7, 'RO754209'),
(9, 'RIL17-1', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 8, 8, 'RO754210'),
(10, 'RIL17-2', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 8, 8, 'RO754211'),
(11, 'RIL18', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 7, 7, 'RO754212'),
(12, 'RIL18-1', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 11, 8, 'RO754213'),
(13, 'RIL18-2', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 11, 8, 'RO754214'),
(14, 'RIL19', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 7, 7, 'RO754215'),
(15, 'RIL19-1', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 14, 8, 'RO754216'),
(16, 'MDRH', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 6, 6, 'RO754217'),
(17, 'MDRH17', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 16, 7, 'RO754218'),
(18, 'MDRH17-1', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 17, 8, 'RO754219'),
(19, 'MDRH17-2', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 17, 8, 'RO754220'),
(20, 'MDRH17-3', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 17, 8, 'RO754221'),
(21, 'MDRH18', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 16, 7, 'RO754222'),
(22, 'MDRH18-1', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 21, 8, 'RO754223'),
(23, 'MDRH18-2', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 21, 8, 'RO754224'),
(24, 'MRDH19', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 16, 7, 'RO754225'),
(25, 'MDRH19-1', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 24, 8, 'RO754226'),
(26, 'MDRH19-2', 'Rue Guglielmo Marconi', '1', 'Parc de La Vatine', 'Mont-Saint-Aignan', '76130', 'France', '02 32 81 8', 'contact@cesi.fr', NULL, 24, 8, 'RO754227');

-- --------------------------------------------------------

--
-- Structure de la table `POSSEDER_BADGE`
--

CREATE TABLE `POSSEDER_BADGE` (
  `utilisateur_id` int(11) NOT NULL,
  `badge_id` int(11) NOT NULL,
  `DATE` date NOT NULL,
  `VALIDE` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `PROPOSER_COMPETENCE`
--

CREATE TABLE `PROPOSER_COMPETENCE` (
  `COMP_ID` int(11) NOT NULL,
  `ORGA_ID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `PROPOSER_COMPETENCE`
--

INSERT INTO `PROPOSER_COMPETENCE` (`COMP_ID`, `ORGA_ID`) VALUES
(1, 9),
(1, 10),
(1, 12),
(1, 13),
(1, 15),
(1, 18),
(1, 19),
(1, 20),
(1, 22),
(1, 23),
(1, 25),
(1, 26),
(2, 9),
(2, 10),
(2, 12),
(2, 13),
(2, 15),
(2, 18),
(2, 19),
(2, 20),
(2, 22),
(2, 23),
(2, 25),
(2, 26),
(3, 9),
(3, 10),
(3, 12),
(3, 13),
(3, 15),
(3, 18),
(3, 19),
(3, 20),
(3, 22),
(3, 23),
(3, 25),
(3, 26),
(4, 9),
(4, 10),
(4, 12),
(4, 13),
(4, 15),
(4, 18),
(4, 19),
(4, 20),
(4, 22),
(4, 23),
(4, 25),
(4, 26),
(5, 9),
(5, 10),
(5, 12),
(5, 13),
(5, 15),
(5, 18),
(5, 19),
(5, 20),
(5, 22),
(5, 23),
(5, 25),
(5, 26),
(6, 9),
(6, 10),
(6, 12),
(6, 13),
(6, 15),
(6, 18),
(6, 19),
(6, 20),
(6, 22),
(6, 23),
(6, 25),
(6, 26),
(7, 9),
(7, 10),
(7, 12),
(7, 13),
(7, 15),
(7, 18),
(7, 19),
(7, 20),
(7, 22),
(7, 23),
(7, 25),
(7, 26),
(8, 9),
(8, 10),
(8, 12),
(8, 13),
(8, 15),
(8, 18),
(8, 19),
(8, 20),
(8, 22),
(8, 23),
(8, 25),
(8, 26),
(9, 9),
(9, 10),
(9, 12),
(9, 13),
(9, 15),
(9, 18),
(9, 19),
(9, 20),
(9, 22),
(9, 23),
(9, 25),
(9, 26),
(10, 9),
(10, 10),
(10, 12),
(10, 13),
(10, 15),
(10, 18),
(10, 19),
(10, 20),
(10, 22),
(10, 23),
(10, 25),
(10, 26),
(11, 9),
(11, 10),
(11, 12),
(11, 13),
(11, 15),
(11, 18),
(11, 19),
(11, 20),
(11, 22),
(11, 23),
(11, 25),
(11, 26),
(12, 9),
(12, 10),
(12, 12),
(12, 13),
(12, 15),
(12, 18),
(12, 19),
(12, 20),
(12, 22),
(12, 23),
(12, 25),
(12, 26),
(13, 9),
(13, 10),
(13, 12),
(13, 13),
(13, 15),
(13, 18),
(13, 19),
(13, 20),
(13, 22),
(13, 23),
(13, 25),
(13, 26),
(14, 9),
(14, 10),
(14, 12),
(14, 13),
(14, 15),
(14, 18),
(14, 19),
(14, 20),
(14, 22),
(14, 23),
(14, 25),
(14, 26),
(15, 9),
(15, 10),
(15, 12),
(15, 13),
(15, 15),
(15, 18),
(15, 19),
(15, 20),
(15, 22),
(15, 23),
(15, 25),
(15, 26);

-- --------------------------------------------------------

--
-- Structure de la table `refresh_tokens`
--

CREATE TABLE `refresh_tokens` (
  `id` int(11) NOT NULL,
  `refresh_token` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valid` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `REGLE`
--

CREATE TABLE `REGLE` (
  `REGL_ID` int(11) NOT NULL,
  `REGL_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `REGL_VALEUR` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `REGL_TYPE` char(1) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `REGLE`
--

INSERT INTO `REGLE` (`REGL_ID`, `REGL_NOM`, `REGL_VALEUR`, `REGL_TYPE`) VALUES
(1, 'bonus', '4', 'i'),
(2, 'pointsMinimumBadge', '1', 'i');

-- --------------------------------------------------------

--
-- Structure de la table `ROLE`
--

CREATE TABLE `ROLE` (
  `ROLE_ID` int(11) NOT NULL,
  `ROLE_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ROLE_DESCRIPTION` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `ROLE`
--

INSERT INTO `ROLE` (`ROLE_ID`, `ROLE_NOM`, `ROLE_DESCRIPTION`) VALUES
(1, 'Administrateur', 'Rôle Administrateur'),
(2, 'Pilote', 'Rôle Pilote'),
(3, 'Etudiant', 'Rôle Etudiant');

-- --------------------------------------------------------

--
-- Structure de la table `TYPEORGA`
--

CREATE TABLE `TYPEORGA` (
  `TYPEORGA_ID` int(11) NOT NULL,
  `TYPEORGA_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `TYPEORGA`
--

INSERT INTO `TYPEORGA` (`TYPEORGA_ID`, `TYPEORGA_NOM`) VALUES
(1, 'ROOT'),
(2, 'Centre de formation'),
(3, 'Secteur'),
(4, 'Ecole'),
(5, 'Pôle'),
(6, 'Formation'),
(7, 'Promotion'),
(8, 'Groupe');

-- --------------------------------------------------------

--
-- Structure de la table `UTILISATEUR`
--

CREATE TABLE `UTILISATEUR` (
  `UTIL_ID` int(11) NOT NULL,
  `UTIL_PSEUDO` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_EMAIL` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_PRENOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_MOT_DE_PASSE` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_SALT` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_URL_AVATAR` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UTIL_POINT_DISPO` int(11) NOT NULL,
  `UTIL_POINT_BONUS_DISPO` int(11) NOT NULL,
  `ORGA_ID` int(11) DEFAULT NULL,
  `UTIL_CIVILITE` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `UTILISATEUR`
--

INSERT INTO `UTILISATEUR` (`UTIL_ID`, `UTIL_PSEUDO`, `UTIL_EMAIL`, `UTIL_PRENOM`, `UTIL_NOM`, `UTIL_MOT_DE_PASSE`, `UTIL_SALT`, `UTIL_URL_AVATAR`, `UTIL_POINT_DISPO`, `UTIL_POINT_BONUS_DISPO`, `ORGA_ID`, `UTIL_CIVILITE`) VALUES
(1, 'Administrateur ROOT', 'administrateur.root@cesi.fr', 'Administrateur', 'ROOT', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', null, 0, 0, 1, 'Monsieur'),
(2, 'Xavier REVERAND', 'xavier.reverand@cesi.fr', 'Xavier', 'REVERAND', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', null, 0, 0, 2, 'Monsieur'),
(3, 'Virginie MERAT', 'virginie.merat@cesi.fr', 'Virginie', 'MERAT', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', null, 0, 0, 4, 'Madame'),
(4, 'Benoit BERENGER', 'benoit.berenger@cesi.fr', 'Benoit', 'BERENGER', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', null, 0, 0, 4, 'Monsieur'),
(5, 'Guillaume AGDAG', 'guillaume.agdag@cesi.fr', 'Guillaume', 'AGDAG', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/agdag_guillaume.jpg', 15, 3, 10, 'Monsieur'),
(6, 'Gregoire AMANN', 'gregoire.amann@cesi.fr', 'Gregoire', 'AMANN', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/amann_gregoire.jpg', 15, 3, 10, 'Monsieur'),
(7, 'Brice BIAVARDI', 'brice.biavardi@cesi.fr', 'Brice', 'BIAVARDI', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/biavardi_brice.jpg', 15, 3, 10, 'Monsieur'),
(8, 'Clement BRESSY', 'clement.bressy@cesi.fr', 'Clement', 'BRESSY', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/bressy_clement.jpg', 15, 3, 10, 'Monsieur'),
(9, 'Nicolas BROQUET', 'nicolas.broquet@cesi.fr', 'Nicolas', 'BROQUET', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/broquet_nicolas.jpg', 15, 3, 10, 'Monsieur'),
(10, 'Chris CERAIRE', 'chris.ceraire@cesi.fr', 'Chris', 'CERAIRE', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', null, 15, 3, 10, 'Monsieur'),
(11, 'Sebastien ESCAFFRE', 'sebastien.escaffre@cesi.fr', 'Sebastien', 'ESCAFFRE', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', null, 15, 3, 10, 'Monsieur'),
(12, 'Sébastien GAULIER', 'sebastien.gaulier@cesi.fr', 'Sebastien', 'GAULIER', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/gaulier_sébastien.jpg', 15, 3, 10, 'Monsieur'),
(13, 'Emmanuel HAUTEMER', 'emmanuel.hautemer@cesi.fr', 'Emmanuel', 'HAUTEMER', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/hautemer_emmanuel.jpg', 15, 3, 10, 'Monsieur'),
(14, 'Elphege HEBERT', 'elphege.hebert@cesi.fr', 'Elphege', 'HEBERT', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', null , 15, 3, 10, 'Monsieur'),
(15, 'Marvin LAISNEY', 'marvin.laisney@cesi.fr', 'Marvin', 'LAISNEY', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', null , 15, 3, 10, 'Monsieur'),
(16, 'Mickael LARCHEVEQUE', 'mickael.larcheveque@cesi.fr', 'Mickael', 'LARCHEVEQUE', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/larcheveque_mickael.jpg', 15, 3, 10, 'Monsieur'),
(17, 'Baptiste LEBORGNE', 'baptiste.leborgne@cesi.fr', 'Baptiste', 'LEBORGNE', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/leborgne_baptiste.jpg', 15, 3, 10, 'Monsieur'),
(18, 'Maxence POISSON', 'maxence.poisson@cesi.fr', 'Maxence', 'POISSON', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/poisson_maxence.jpg', 15, 3, 10, 'Monsieur'),
(19, 'Antoine SINAEVE', 'antoine.sinaeve@cesi.fr', 'Antoine', 'SINAEVE', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', 'benoit.berenger@cesi.fr1549025318', 'http://youbyme.nicolas-broquet.fr/api/v1/assets/student/sinaeve_antoine.jpg', 15, 3, 10, 'Monsieur');

-- --------------------------------------------------------

--
-- Structure de la table `VOTE`
--

CREATE TABLE `VOTE` (
  `VOTE_ID` int(11) NOT NULL,
  `VOTE_DATE` date NOT NULL,
  `VOTE_VALIDE` tinyint(1) NOT NULL,
  `VOTE_TYPE` int(11) NOT NULL,
  `UTIL_ID` int(11) DEFAULT NULL,
  `UTI_UTIL_ID` int(11) DEFAULT NULL,
  `COMP_ID` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Structure de la table `HISTORIQUE_PERIODE`
--

CREATE TABLE `HISTORIQUE_PERIODE` (
	`id` INT AUTO_INCREMENT NOT NULL, 
	`organisation_id` INT NOT NULL, 
	`date` DATETIME NOT NULL, 
	INDEX IDX_F1D7728C9E6B1585 (organisation_id), 
	PRIMARY KEY(id)
	)DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `AUTORISATION`
--
ALTER TABLE `AUTORISATION`
  ADD PRIMARY KEY (`AUTO_ID`);

--
-- Index pour la table `BADGE`
--
ALTER TABLE `BADGE`
  ADD PRIMARY KEY (`BADG_ID`),
  ADD UNIQUE KEY `UNIQ_9A48AAD4A058331` (`COMP_ID`),
  ADD KEY `FK_REPRESENTER` (`COMP_ID`);

--
-- Index pour la table `COMPETENCE`
--
ALTER TABLE `COMPETENCE`
  ADD PRIMARY KEY (`COMP_ID`),
  ADD KEY `FK_COMPOSER_COMPETENCE` (`COM_COMP_ID`);

--
-- Index pour la table `CONTENIR_AUTORISATION`
--
ALTER TABLE `CONTENIR_AUTORISATION`
  ADD PRIMARY KEY (`ROLE_ID`,`AUTO_ID`),
  ADD KEY `IDX_D52E6D6AD10B9A56` (`ROLE_ID`),
  ADD KEY `IDX_D52E6D6A1A5D01DF` (`AUTO_ID`);

--
-- Index pour la table `DEFINIR_REGLE`
--
ALTER TABLE `DEFINIR_REGLE`
  ADD PRIMARY KEY (`REGL_ID`,`ORGA_ID`),
  ADD KEY `IDX_2E74DB817A48C7AB` (`REGL_ID`),
  ADD KEY `IDX_2E74DB8190F8D05B` (`ORGA_ID`);

--
-- Index pour la table `GERER_ORGANISATION`
--
ALTER TABLE `GERER_ORGANISATION`
  ADD PRIMARY KEY (`UTIL_ID`,`ORGA_ID`),
  ADD KEY `IDX_C34C447FC855967A` (`UTIL_ID`),
  ADD KEY `IDX_C34C447F90F8D05B` (`ORGA_ID`);

--
-- Index pour la table `OBTENIR_ROLE`
--
ALTER TABLE `OBTENIR_ROLE`
  ADD PRIMARY KEY (`ROLE_ID`,`UTIL_ID`),
  ADD KEY `IDX_C5C587EBD10B9A56` (`ROLE_ID`),
  ADD KEY `IDX_C5C587EBC855967A` (`UTIL_ID`);

--
-- Index pour la table `ORGANISATION`
--
ALTER TABLE `ORGANISATION`
  ADD PRIMARY KEY (`ORGA_ID`),
  ADD KEY `FK_APPARTENIR_ORGANISATION` (`ORG_ORGA_ID`),
  ADD KEY `FK_DEFINIR_TYPE` (`TYPEORGA_ID`);

--
-- Index pour la table `POSSEDER_BADGE`
--
ALTER TABLE `POSSEDER_BADGE`
  ADD PRIMARY KEY (`utilisateur_id`,`badge_id`),
  ADD KEY `IDX_DA70A877FB88E14F` (`utilisateur_id`),
  ADD KEY `IDX_DA70A877F7A2C2FC` (`badge_id`);

--
-- Index pour la table `PROPOSER_COMPETENCE`
--
ALTER TABLE `PROPOSER_COMPETENCE`
  ADD PRIMARY KEY (`COMP_ID`,`ORGA_ID`),
  ADD KEY `IDX_645943AA4A058331` (`COMP_ID`),
  ADD KEY `IDX_645943AA90F8D05B` (`ORGA_ID`);

--
-- Index pour la table `refresh_tokens`
--
ALTER TABLE `refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_9BACE7E1C74F2195` (`refresh_token`);

--
-- Index pour la table `REGLE`
--
ALTER TABLE `REGLE`
  ADD PRIMARY KEY (`REGL_ID`);

--
-- Index pour la table `ROLE`
--
ALTER TABLE `ROLE`
  ADD PRIMARY KEY (`ROLE_ID`);

--
-- Index pour la table `TYPEORGA`
--
ALTER TABLE `TYPEORGA`
  ADD PRIMARY KEY (`TYPEORGA_ID`);

--
-- Index pour la table `UTILISATEUR`
--
ALTER TABLE `UTILISATEUR`
  ADD PRIMARY KEY (`UTIL_ID`),
  ADD KEY `IDX_901FF15B90F8D05B` (`ORGA_ID`);

--
-- Index pour la table `VOTE`
--
ALTER TABLE `VOTE`
  ADD PRIMARY KEY (`VOTE_ID`),
  ADD KEY `FK_EMETTRE` (`UTIL_ID`),
  ADD KEY `FK_RECEVOIR` (`UTI_UTIL_ID`),
  ADD KEY `FK_VOTER` (`COMP_ID`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `AUTORISATION`
--
ALTER TABLE `AUTORISATION`
  MODIFY `AUTO_ID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT pour la table `BADGE`
--
ALTER TABLE `BADGE`
  MODIFY `BADG_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `COMPETENCE`
--
ALTER TABLE `COMPETENCE`
  MODIFY `COMP_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT pour la table `ORGANISATION`
--
ALTER TABLE `ORGANISATION`
  MODIFY `ORGA_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT pour la table `refresh_tokens`
--
ALTER TABLE `refresh_tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `REGLE`
--
ALTER TABLE `REGLE`
  MODIFY `REGL_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `ROLE`
--
ALTER TABLE `ROLE`
  MODIFY `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `TYPEORGA`
--
ALTER TABLE `TYPEORGA`
  MODIFY `TYPEORGA_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `UTILISATEUR`
--
ALTER TABLE `UTILISATEUR`
  MODIFY `UTIL_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT pour la table `VOTE`
--
ALTER TABLE `VOTE`
  MODIFY `VOTE_ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `BADGE`
--
ALTER TABLE `BADGE`
  ADD CONSTRAINT `FK_9A48AAD4A058331` FOREIGN KEY (`COMP_ID`) REFERENCES `COMPETENCE` (`COMP_ID`);

--
-- Contraintes pour la table `COMPETENCE`
--
ALTER TABLE `COMPETENCE`
  ADD CONSTRAINT `FK_9F4B75403F2ECB5A` FOREIGN KEY (`COM_COMP_ID`) REFERENCES `COMPETENCE` (`COMP_ID`);

--
-- Contraintes pour la table `CONTENIR_AUTORISATION`
--
ALTER TABLE `CONTENIR_AUTORISATION`
  ADD CONSTRAINT `FK_D52E6D6A1A5D01DF` FOREIGN KEY (`AUTO_ID`) REFERENCES `AUTORISATION` (`AUTO_ID`),
  ADD CONSTRAINT `FK_D52E6D6AD10B9A56` FOREIGN KEY (`ROLE_ID`) REFERENCES `ROLE` (`ROLE_ID`);

--
-- Contraintes pour la table `DEFINIR_REGLE`
--
ALTER TABLE `DEFINIR_REGLE`
  ADD CONSTRAINT `FK_2E74DB817A48C7AB` FOREIGN KEY (`REGL_ID`) REFERENCES `REGLE` (`REGL_ID`),
  ADD CONSTRAINT `FK_2E74DB8190F8D05B` FOREIGN KEY (`ORGA_ID`) REFERENCES `ORGANISATION` (`ORGA_ID`);

--
-- Contraintes pour la table `GERER_ORGANISATION`
--
ALTER TABLE `GERER_ORGANISATION`
  ADD CONSTRAINT `FK_C34C447F90F8D05B` FOREIGN KEY (`ORGA_ID`) REFERENCES `ORGANISATION` (`ORGA_ID`),
  ADD CONSTRAINT `FK_C34C447FC855967A` FOREIGN KEY (`UTIL_ID`) REFERENCES `UTILISATEUR` (`UTIL_ID`);

--
-- Contraintes pour la table `OBTENIR_ROLE`
--
ALTER TABLE `OBTENIR_ROLE`
  ADD CONSTRAINT `FK_C5C587EBC855967A` FOREIGN KEY (`UTIL_ID`) REFERENCES `UTILISATEUR` (`UTIL_ID`),
  ADD CONSTRAINT `FK_C5C587EBD10B9A56` FOREIGN KEY (`ROLE_ID`) REFERENCES `ROLE` (`ROLE_ID`);

--
-- Contraintes pour la table `ORGANISATION`
--
ALTER TABLE `ORGANISATION`
  ADD CONSTRAINT `FK_73D37BA4A8B78A31` FOREIGN KEY (`ORG_ORGA_ID`) REFERENCES `ORGANISATION` (`ORGA_ID`),
  ADD CONSTRAINT `FK_73D37BA4F85A3012` FOREIGN KEY (`TYPEORGA_ID`) REFERENCES `TYPEORGA` (`TYPEORGA_ID`);

--
-- Contraintes pour la table `POSSEDER_BADGE`
--
ALTER TABLE `POSSEDER_BADGE`
  ADD CONSTRAINT `FK_DA70A877F7A2C2FC` FOREIGN KEY (`badge_id`) REFERENCES `BADGE` (`BADG_ID`),
  ADD CONSTRAINT `FK_DA70A877FB88E14F` FOREIGN KEY (`utilisateur_id`) REFERENCES `UTILISATEUR` (`UTIL_ID`);

--
-- Contraintes pour la table `PROPOSER_COMPETENCE`
--
ALTER TABLE `PROPOSER_COMPETENCE`
  ADD CONSTRAINT `FK_645943AA4A058331` FOREIGN KEY (`COMP_ID`) REFERENCES `COMPETENCE` (`COMP_ID`),
  ADD CONSTRAINT `FK_645943AA90F8D05B` FOREIGN KEY (`ORGA_ID`) REFERENCES `ORGANISATION` (`ORGA_ID`);

--
-- Contraintes pour la table `UTILISATEUR`
--
ALTER TABLE `UTILISATEUR`
  ADD CONSTRAINT `FK_901FF15B90F8D05B` FOREIGN KEY (`ORGA_ID`) REFERENCES `ORGANISATION` (`ORGA_ID`) ON DELETE SET NULL;

--
-- Contraintes pour la table `VOTE`
--
ALTER TABLE `VOTE`
  ADD CONSTRAINT `FK_6C8568D04A058331` FOREIGN KEY (`COMP_ID`) REFERENCES `COMPETENCE` (`COMP_ID`),
  ADD CONSTRAINT `FK_6C8568D0799FFB8A` FOREIGN KEY (`UTI_UTIL_ID`) REFERENCES `UTILISATEUR` (`UTIL_ID`),
  ADD CONSTRAINT `FK_6C8568D0C855967A` FOREIGN KEY (`UTIL_ID`) REFERENCES `UTILISATEUR` (`UTIL_ID`);
  
--
-- Contraintes pour la table `HISTORIQUE_PERIODE`
--
ALTER TABLE historique_periode 
	ADD CONSTRAINT FK_F1D7728C9E6B1585 FOREIGN KEY (organisation_id) REFERENCES ORGANISATION (ORGA_ID);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
