/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*!40000 ALTER TABLE `AUTORISATION` DISABLE KEYS */;
INSERT INTO `AUTORISATION` (`AUTO_ID`, `AUTO_NOM`, `AUTO_DESCRIPTION`) VALUES
(3, 'ShowUtilisateursVotables', NULL),
(4, 'ReadUtilisateurs', 'ReadUtilisateurs'),
(5, 'CreateUtilisateurs', 'CreateUtilisateurs'),
(6, 'UpdateUtilisateurs', 'UpdateUtilisateurs'),
(7, 'DeleteUtilisateurs', 'DeleteUtilisateurs'),
(8, 'ReadRoles', 'ReadRoles'),
(9, 'CreateRoles', 'CreateRoles'),
(10, 'UpdateRoles', 'UpdateRoles'),
(11, 'DeleteRoles', 'DeleteRoles'),
(12, 'ReadAutorisations', 'ReadAutorisations'),
(13, 'CreateAutorisations', 'CreateAutorisations'),
(14, 'UpdateAutorisations', 'UpdateAutorisations'),
(15, 'DeleteAutorisations', 'DeleteAutorisations'),
(16, 'ReadBadges', 'ReadBadges'),
(17, 'CreateBadges', 'CreateBadges'),
(18, 'UpdateBadges', 'UpdateBadges'),
(19, 'DeleteBadges', 'DeleteBadges'),
(20, 'ReadCompetences', 'ReadCompetences'),
(21, 'CreateCompetences', 'CreateCompetences'),
(22, 'UpdateCompetences', 'UpdateCompetences'),
(23, 'DeleteCompetences', 'DeleteCompetences'),
(24, 'ReadOrganisations', 'ReadOrganisations'),
(25, 'CreateOrganisations', 'CreateOrganisations'),
(26, 'UpdateOrganisations', 'UpdateOrganisations'),
(27, 'DeleteOrganisations', 'DeleteOrganisations'),
(28, 'ReadRegles', 'ReadRegles'),
(29, 'CreateRegles', 'CreateRegles'),
(30, 'UpdateRegles', 'UpdateRegles'),
(31, 'DeleteRegles', 'DeleteRegles'),
(32, 'ReadTypeOrgas', 'ReadTypeOrgas'),
(33, 'CreateTypeOrgas', 'CreateTypeOrgas'),
(34, 'UpdateTypeOrgas', 'UpdateTypeOrgas'),
(35, 'DeleteTypeOrgas', 'DeleteTypeOrgas'),
(36, 'ReadVotes', 'ReadVotes'),
(37, 'CreateVotes', 'CreateVotes'),
(38, 'UpdateVotes', 'UpdateVotes'),
(39, 'DeleteVotes', 'DeleteVotes');
/*!40000 ALTER TABLE `AUTORISATION` ENABLE KEYS */;

/*!40000 ALTER TABLE `BADGE` DISABLE KEYS */;
INSERT INTO `BADGE` (`BADG_ID`, `BADG_NOM`, `BADG_IMAGE_URL`, `BADG_DESCRIPTION`, `BADG_EXPIRATION`, `BADG_PREUVE_URL`, `BADG_POINTS_REQUIS`, `COMP_ID`) VALUES
(1, 'HTML', NULL, 'Badge web junior', '2019-03-03', NULL, 1, 1);
/*!40000 ALTER TABLE `BADGE` ENABLE KEYS */;

/*!40000 ALTER TABLE `COMPETENCE` DISABLE KEYS */;
INSERT INTO `COMPETENCE` (`COMP_ID`, `COMP_NOM`, `COMP_DESCRIPTION`, `COM_COMP_ID`) VALUES
(1, 'HTML', 'Compétence web basique HTML/CSS', NULL);
/*!40000 ALTER TABLE `COMPETENCE` ENABLE KEYS */;

/*!40000 ALTER TABLE `CONTENIR_AUTORISATION` DISABLE KEYS */;
INSERT INTO `CONTENIR_AUTORISATION` (`ROLE_ID`, `AUTO_ID`) VALUES
(6, 4),
(6, 5),
(6, 6),
(6, 7),
(6, 8),
(6, 9),
(6, 10),
(6, 11),
(6, 12),
(6, 13),
(6, 14),
(6, 15),
(6, 16),
(6, 17),
(6, 18),
(6, 19),
(6, 20),
(6, 21),
(6, 22),
(6, 23),
(6, 24),
(6, 25),
(6, 26),
(6, 27),
(6, 28),
(6, 29),
(6, 30),
(6, 31),
(6, 32),
(6, 33),
(6, 34),
(6, 35),
(6, 36),
(6, 37),
(6, 38),
(6, 39);
/*!40000 ALTER TABLE `CONTENIR_AUTORISATION` ENABLE KEYS */;

/*!40000 ALTER TABLE `DEFINIR_REGLE` DISABLE KEYS */;
INSERT INTO `DEFINIR_REGLE` (`REGL_ID`, `ORGA_ID`) VALUES
(1, 1),
(2, 1);
/*!40000 ALTER TABLE `DEFINIR_REGLE` ENABLE KEYS */;

/*!40000 ALTER TABLE `OBTENIR_ROLE` DISABLE KEYS */;
INSERT INTO `OBTENIR_ROLE` (`ROLE_ID`, `UTIL_ID`) VALUES
(6, 7),
(7, 7),
(6, 1);
/*!40000 ALTER TABLE `OBTENIR_ROLE` ENABLE KEYS */;

/*!40000 ALTER TABLE `ORGANISATION` DISABLE KEYS */;
INSERT INTO `ORGANISATION` (`ORGA_ID`, `ORGA_NOM`, `ORGA_ADRE_RUE`, `ORGA_ADRE_NUMERO`, `ORGA_ADRE_COMPLEMENT`, `ORGA_ADRE_VILLE`, `ORGA_ADRE_CP`, `ORGA_ADRE_PAYS`, `ORGA_TEL`, `ORGA_MAIL`, `ORGA_SIRET`, `ORG_ORGA_ID`, `TYPEORGA_ID`) VALUES
(1, 'RIL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1);
/*!40000 ALTER TABLE `ORGANISATION` ENABLE KEYS */;

/*!40000 ALTER TABLE `POSSEDER_BADGE` DISABLE KEYS */;
/*!40000 ALTER TABLE `POSSEDER_BADGE` ENABLE KEYS */;

/*!40000 ALTER TABLE `PROPOSER_COMPETENCE` DISABLE KEYS */;
INSERT INTO `PROPOSER_COMPETENCE` (`COMP_ID`, `ORGA_ID`) VALUES
(1, 1);
/*!40000 ALTER TABLE `PROPOSER_COMPETENCE` ENABLE KEYS */;

/*!40000 ALTER TABLE `refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `refresh_tokens` ENABLE KEYS */;

/*!40000 ALTER TABLE `REGLE` DISABLE KEYS */;
INSERT INTO `REGLE` (`REGL_ID`, `REGL_NOM`, `REGL_VALEUR`, `REGL_TYPE`) VALUES
(1, 'bonus', '4', 'i'),
(2, 'pointsMinimumBadge', '1', 'i');
/*!40000 ALTER TABLE `REGLE` ENABLE KEYS */;

/*!40000 ALTER TABLE `ROLE` DISABLE KEYS */;
INSERT INTO `ROLE` (`ROLE_ID`, `ROLE_NOM`, `ROLE_DESCRIPTION`) VALUES
(6, 'Admin', 'Role administrateur par défaut'),
(7, 'Etudiant', 'Role etudiant');
/*!40000 ALTER TABLE `ROLE` ENABLE KEYS */;

/*!40000 ALTER TABLE `TYPEORGA` DISABLE KEYS */;
INSERT INTO `TYPEORGA` (`TYPEORGA_ID`, `TYPEORGA_NOM`) VALUES
(1, 'Promotion');
/*!40000 ALTER TABLE `TYPEORGA` ENABLE KEYS */;

/*!40000 ALTER TABLE `UTILISATEUR` DISABLE KEYS */;
INSERT INTO `UTILISATEUR` (`UTIL_ID`, `UTIL_PSEUDO`, `UTIL_EMAIL`, `UTIL_PRENOM`, `UTIL_NOM`, `UTIL_MOT_DE_PASSE`, `UTIL_URL_AVATAR`, `UTIL_POINT_DISPO`, `UTIL_POINT_BONUS_DISPO`, `ORGA_ID`, `UTIL_SALT`) VALUES
(1, 'Admin-default', 'benoit.berenger@cesi.fr', 'Benoît', 'Bérenger', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', NULL, 0, 3, 1, 'benoit.berenger@cesi.fr1549025318'),
(7, 'Snowy', 'antoine.sinaeve@viacesi.fr', 'Antoine', 'Sinaeve', '$2y$13$aV5.qdjZGa3bofLku0Hh9eoI4Ridd9bXJ0itMnePIT9BVqxXdkdS2', NULL, 4, 3, 1, 'antoine.sinaeve@viacesi.fr1549271255');
/*!40000 alter table `UTILISATEUR` enable keys */;

/*!40000 ALTER TABLE `VOTE` DISABLE KEYS */;
/*!40000 ALTER TABLE `VOTE` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
