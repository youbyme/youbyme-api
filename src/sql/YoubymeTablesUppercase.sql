-- --------------------------------------------------------
-- Hôte :                        127.0.0.1
-- Version du serveur:           10.3.11-MariaDB - mariadb.org binary distribution
-- SE du serveur:                Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Export de la structure de la table youbyme. autorisation
CREATE TABLE IF NOT EXISTS `AUTORISATION` (
  `AUTO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `AUTO_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `AUTO_DESCRIPTION` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`AUTO_ID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. competence
CREATE TABLE IF NOT EXISTS `COMPETENCE` (
  `COMP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMP_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `COMP_DESCRIPTION` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `COM_COMP_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`COMP_ID`),
  KEY `FK_COMPOSER_COMPETENCE` (`COM_COMP_ID`),
  CONSTRAINT `FK_9F4B75403F2ECB5A` FOREIGN KEY (`COM_COMP_ID`) REFERENCES `COMPETENCE` (`COMP_ID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. badge
CREATE TABLE IF NOT EXISTS `BADGE` (
  `BADG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `BADG_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `BADG_IMAGE_URL` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BADG_DESCRIPTION` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BADG_EXPIRATION` date DEFAULT NULL,
  `BADG_PREUVE_URL` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `BADG_POINTS_REQUIS` int(11) DEFAULT NULL,
  `COMP_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`BADG_ID`),
  UNIQUE KEY `UNIQ_9A48AAD4A058331` (`COMP_ID`),
  KEY `FK_REPRESENTER` (`COMP_ID`),
  CONSTRAINT `FK_9A48AAD4A058331` FOREIGN KEY (`COMP_ID`) REFERENCES `COMPETENCE` (`COMP_ID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. role
CREATE TABLE IF NOT EXISTS `ROLE` (
  `ROLE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ROLE_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ROLE_DESCRIPTION` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ROLE_ID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. regle
CREATE TABLE IF NOT EXISTS `REGLE` (
  `REGL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `REGL_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `REGL_VALEUR` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `REGL_TYPE` char(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`REGL_ID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. typeorga
CREATE TABLE IF NOT EXISTS `TYPEORGA` (
  `TYPEORGA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TYPEORGA_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`TYPEORGA_ID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. organisation
CREATE TABLE IF NOT EXISTS `ORGANISATION` (
  `ORGA_ID` int(11) NOT NULL AUTO_INCREMENT,
  `ORGA_CODE_ANALYTICS` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ORGA_ADRE_RUE` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_ADRE_NUMERO` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_ADRE_COMPLEMENT` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_ADRE_VILLE` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_ADRE_CP` varchar(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_ADRE_PAYS` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_TEL` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_MAIL` varchar(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORGA_SIRET` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ORG_ORGA_ID` int(11) DEFAULT NULL,
  `TYPEORGA_ID` int(11) NOT NULL,
  `ORGA_DATE_RESET` date NOT NULL,
  PRIMARY KEY (`ORGA_ID`),
  KEY `FK_APPARTENIR_ORGANISATION` (`ORG_ORGA_ID`),
  KEY `FK_DEFINIR_TYPE` (`TYPEORGA_ID`),
  CONSTRAINT `FK_73D37BA4A8B78A31` FOREIGN KEY (`ORG_ORGA_ID`) REFERENCES `ORGANISATION` (`ORGA_ID`),
  CONSTRAINT `FK_73D37BA4F85A3012` FOREIGN KEY (`TYPEORGA_ID`) REFERENCES `TYPEORGA` (`TYPEORGA_ID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. utilisateur
CREATE TABLE IF NOT EXISTS `UTILISATEUR` (
  `UTIL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `UTIL_PSEUDO` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_EMAIL` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_PRENOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_NOM` varchar(40) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_CIVILITE` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_MOT_DE_PASSE` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  `UTIL_URL_AVATAR` varchar(256) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `UTIL_POINT_DISPO` int(11) NOT NULL,
  `UTIL_POINT_BONUS_DISPO` int(11) NOT NULL,
  `ORGA_ID` int(11) DEFAULT NULL,
  `UTIL_SALT` varchar(256) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`UTIL_ID`),
  KEY `IDX_901FF15B90F8D05B` (`ORGA_ID`),
  CONSTRAINT `FK_901FF15B90F8D05B` FOREIGN KEY (`ORGA_ID`) REFERENCES `ORGANISATION` (`ORGA_ID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. vote
CREATE TABLE IF NOT EXISTS `VOTE` (
  `VOTE_ID` int(11) NOT NULL AUTO_INCREMENT,
  `VOTE_DATE` date NOT NULL,
  `VOTE_VALIDE` tinyint(1) NOT NULL,
  `VOTE_TYPE` int(11) NOT NULL,
  `UTIL_ID` int(11) DEFAULT NULL,
  `UTI_UTIL_ID` int(11) DEFAULT NULL,
  `COMP_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`VOTE_ID`),
  KEY `FK_EMETTRE` (`UTIL_ID`),
  KEY `FK_RECEVOIR` (`UTI_UTIL_ID`),
  KEY `FK_VOTER` (`COMP_ID`),
  CONSTRAINT `FK_6C8568D04A058331` FOREIGN KEY (`COMP_ID`) REFERENCES `COMPETENCE` (`COMP_ID`),
  CONSTRAINT `FK_6C8568D0799FFB8A` FOREIGN KEY (`UTI_UTIL_ID`) REFERENCES `UTILISATEUR` (`UTIL_ID`),
  CONSTRAINT `FK_6C8568D0C855967A` FOREIGN KEY (`UTIL_ID`) REFERENCES `UTILISATEUR` (`UTIL_ID`)
  ) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. contenir_autorisation
CREATE TABLE IF NOT EXISTS `CONTENIR_AUTORISATION` (
  `ROLE_ID` int(11) NOT NULL,
  `AUTO_ID` int(11) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`AUTO_ID`),
  KEY `IDX_D52E6D6AD10B9A56` (`ROLE_ID`),
  KEY `IDX_D52E6D6A1A5D01DF` (`AUTO_ID`),
  CONSTRAINT `FK_D52E6D6A1A5D01DF` FOREIGN KEY (`AUTO_ID`) REFERENCES `AUTORISATION` (`AUTO_ID`),
  CONSTRAINT `FK_D52E6D6AD10B9A56` FOREIGN KEY (`ROLE_ID`) REFERENCES `ROLE` (`ROLE_ID`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. definir_regle
CREATE TABLE IF NOT EXISTS `DEFINIR_REGLE` (
  `REGL_ID` int(11) NOT NULL,
  `ORGA_ID` int(11) NOT NULL,
  PRIMARY KEY (`REGL_ID`,`ORGA_ID`),
  KEY `IDX_2E74DB817A48C7AB` (`REGL_ID`),
  KEY `IDX_2E74DB8190F8D05B` (`ORGA_ID`),
  CONSTRAINT `FK_2E74DB817A48C7AB` FOREIGN KEY (`REGL_ID`) REFERENCES `REGLE` (`REGL_ID`),
  CONSTRAINT `FK_2E74DB8190F8D05B` FOREIGN KEY (`ORGA_ID`) REFERENCES `ORGANISATION` (`ORGA_ID`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. obtenir_role
CREATE TABLE IF NOT EXISTS `OBTENIR_ROLE` (
  `ROLE_ID` int(11) NOT NULL,
  `UTIL_ID` int(11) NOT NULL,
  PRIMARY KEY (`ROLE_ID`,`UTIL_ID`),
  KEY `IDX_C5C587EBD10B9A56` (`ROLE_ID`),
  KEY `IDX_C5C587EBC855967A` (`UTIL_ID`),
  CONSTRAINT `FK_C5C587EBC855967A` FOREIGN KEY (`UTIL_ID`) REFERENCES `UTILISATEUR` (`UTIL_ID`),
  CONSTRAINT `FK_C5C587EBD10B9A56` FOREIGN KEY (`ROLE_ID`) REFERENCES `ROLE` (`ROLE_ID`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. posseder_badge
CREATE TABLE IF NOT EXISTS `POSSEDER_BADGE` (
  `UTILISATEUR_ID` int(11) NOT NULL,
  `BADGE_ID` int(11) NOT NULL,
  `DATE` date NOT NULL,
  `VALIDE` tinyint(1) NOT NULL,
  PRIMARY KEY (`UTILISATEUR_ID`,`BADGE_ID`),
  KEY `IDX_DA70A877FB88E14F` (`UTILISATEUR_ID`),
  KEY `IDX_DA70A877F7A2C2FC` (`BADGE_ID`),
  CONSTRAINT `FK_DA70A877F7A2C2FC` FOREIGN KEY (`BADGE_ID`) REFERENCES `BADGE` (`BADG_ID`),
  CONSTRAINT `FK_DA70A877FB88E14F` FOREIGN KEY (`UTILISATEUR_ID`) REFERENCES `UTILISATEUR` (`UTIL_ID`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. proposer_competence
CREATE TABLE IF NOT EXISTS `PROPOSER_COMPETENCE` (
  `COMP_ID` int(11) NOT NULL,
  `ORGA_ID` int(11) NOT NULL,
  PRIMARY KEY (`COMP_ID`,`ORGA_ID`),
  KEY `IDX_645943AA4A058331` (`COMP_ID`),
  KEY `IDX_645943AA90F8D05B` (`ORGA_ID`),
  CONSTRAINT `FK_645943AA4A058331` FOREIGN KEY (`COMP_ID`) REFERENCES `COMPETENCE` (`COMP_ID`),
  CONSTRAINT `FK_645943AA90F8D05B` FOREIGN KEY (`ORGA_ID`) REFERENCES `ORGANISATION` (`ORGA_ID`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
  
CREATE TABLE IF NOT EXISTS `HISTORIQUE_PERIODE` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `organisation_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F1D7728C9E6B1585` (`organisation_id`),
  CONSTRAINT `FK_F1D7728C9E6B1585` FOREIGN KEY (`organisation_id`) REFERENCES `organisation` (`ORGA_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
-- Export de la structure de la table youbyme. refresh_tokens
CREATE TABLE IF NOT EXISTS `refresh_tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `refresh_token` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `valid` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_9BACE7E1C74F2195` (`refresh_token`)
  ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Les données exportées n'étaient pas sélectionnées.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
