/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

/*!40000 ALTER TABLE `autorisation` DISABLE KEYS */;
INSERT INTO `autorisation` (`AUTO_ID`, `AUTO_NOM`, `AUTO_DESCRIPTION`) VALUES
	(3, 'ShowUtilisateursVotables', NULL),
	(4, 'ReadUtilisateurs', 'ReadUtilisateurs'),
	(5, 'CreateUtilisateurs', 'CreateUtilisateurs'),
	(6, 'UpdateUtilisateurs', 'UpdateUtilisateurs'),
	(7, 'DeleteUtilisateurs', 'DeleteUtilisateurs'),
	(8, 'ReadRoles', 'ReadRoles'),
	(9, 'CreateRoles', 'CreateRoles'),
	(10, 'UpdateRoles', 'UpdateRoles'),
	(11, 'DeleteRoles', 'DeleteRoles'),
	(12, 'ReadAutorisations', 'ReadAutorisations'),
	(13, 'CreateAutorisations', 'CreateAutorisations'),
	(14, 'UpdateAutorisations', 'UpdateAutorisations'),
	(15, 'DeleteAutorisations', 'DeleteAutorisations'),
	(16, 'ReadBadges', 'ReadBadges'),
	(17, 'CreateBadges', 'CreateBadges'),
	(18, 'UpdateBadges', 'UpdateBadges'),
	(19, 'DeleteBadges', 'DeleteBadges'),
	(20, 'ReadCompetences', 'ReadCompetences'),
	(21, 'CreateCompetences', 'CreateCompetences'),
	(22, 'UpdateCompetences', 'UpdateCompetences'),
	(23, 'DeleteCompetences', 'DeleteCompetences'),
	(24, 'ReadOrganisations', 'ReadOrganisations'),
	(25, 'CreateOrganisations', 'CreateOrganisations'),
	(26, 'UpdateOrganisations', 'UpdateOrganisations'),
	(27, 'DeleteOrganisations', 'DeleteOrganisations'),
	(28, 'ReadRegles', 'ReadRegles'),
	(29, 'CreateRegles', 'CreateRegles'),
	(30, 'UpdateRegles', 'UpdateRegles'),
	(31, 'DeleteRegles', 'DeleteRegles'),
	(32, 'ReadTypeOrgas', 'ReadTypeOrgas'),
	(33, 'CreateTypeOrgas', 'CreateTypeOrgas'),
	(34, 'UpdateTypeOrgas', 'UpdateTypeOrgas'),
	(35, 'DeleteTypeOrgas', 'DeleteTypeOrgas'),
	(36, 'ReadVotes', 'ReadVotes'),
	(37, 'CreateVotes', 'CreateVotes'),
	(38, 'UpdateVotes', 'UpdateVotes'),
	(39, 'DeleteVotes', 'DeleteVotes'),
	(40, 'ReadBadgeAssignations', NULL),
	(41, 'CreateBadgeAssignations', NULL),
	(42, 'UpdateBadgeAssignations', NULL),
	(43, 'DeleteBadgeAssignations', NULL);
/*!40000 ALTER TABLE `autorisation` ENABLE KEYS */;

/*!40000 ALTER TABLE `badge` DISABLE KEYS */;
INSERT INTO `badge` (`BADG_ID`, `BADG_NOM`, `BADG_IMAGE_URL`, `BADG_DESCRIPTION`, `BADG_EXPIRATION`, `BADG_PREUVE_URL`, `BADG_POINTS_REQUIS`, `COMP_ID`) VALUES
	(1, 'HTML', NULL, 'Badge web junior', '2019-03-03', NULL, 1, 1);
/*!40000 ALTER TABLE `badge` ENABLE KEYS */;

/*!40000 ALTER TABLE `competence` DISABLE KEYS */;
INSERT INTO `competence` (`COMP_ID`, `COMP_NOM`, `COMP_DESCRIPTION`, `COM_COMP_ID`) VALUES
	(1, 'HTML', 'Compétence web basique HTML/CSS', NULL);
/*!40000 ALTER TABLE `competence` ENABLE KEYS */;

/*!40000 ALTER TABLE `contenir_autorisation` DISABLE KEYS */;
INSERT INTO `contenir_autorisation` (`ROLE_ID`, `AUTO_ID`) VALUES
	(6, 4),
	(6, 5),
	(6, 6),
	(6, 7),
	(6, 8),
	(6, 9),
	(6, 10),
	(6, 11),
	(6, 12),
	(6, 13),
	(6, 14),
	(6, 15),
	(6, 16),
	(6, 17),
	(6, 18),
	(6, 19),
	(6, 20),
	(6, 21),
	(6, 22),
	(6, 23),
	(6, 24),
	(6, 25),
	(6, 26),
	(6, 27),
	(6, 28),
	(6, 29),
	(6, 30),
	(6, 31),
	(6, 32),
	(6, 33),
	(6, 34),
	(6, 35),
	(6, 36),
	(6, 37),
	(6, 38),
	(6, 39),
	(6, 40),
	(6, 41),
	(6, 42),
	(6, 43);
/*!40000 ALTER TABLE `contenir_autorisation` ENABLE KEYS */;

/*!40000 ALTER TABLE `definir_regle` DISABLE KEYS */;
INSERT INTO `definir_regle` (`REGL_ID`, `ORGA_ID`) VALUES
	(1, 1),
	(2, 1);
/*!40000 ALTER TABLE `definir_regle` ENABLE KEYS */;

/*!40000 ALTER TABLE `obtenir_role` DISABLE KEYS */;
INSERT INTO `obtenir_role` (`ROLE_ID`, `UTIL_ID`) VALUES
	(6, 1);
/*!40000 ALTER TABLE `obtenir_role` ENABLE KEYS */;

/*!40000 ALTER TABLE `organisation` DISABLE KEYS */;
INSERT INTO `organisation` (`ORGA_ID`, `ORGA_NOM`, `ORGA_ADRE_RUE`, `ORGA_ADRE_NUMERO`, `ORGA_ADRE_COMPLEMENT`, `ORGA_ADRE_VILLE`, `ORGA_ADRE_CP`, `ORGA_ADRE_PAYS`, `ORGA_TEL`, `ORGA_MAIL`, `ORGA_SIRET`, `ORG_ORGA_ID`, `TYPEORGA_ID`) VALUES
	(1, 'RIL', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1);
/*!40000 ALTER TABLE `organisation` ENABLE KEYS */;

/*!40000 ALTER TABLE `posseder_badge` DISABLE KEYS */;
/*!40000 ALTER TABLE `posseder_badge` ENABLE KEYS */;

/*!40000 ALTER TABLE `proposer_competence` DISABLE KEYS */;
INSERT INTO `proposer_competence` (`COMP_ID`, `ORGA_ID`) VALUES
	(1, 1);
/*!40000 ALTER TABLE `proposer_competence` ENABLE KEYS */;

/*!40000 ALTER TABLE `refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `refresh_tokens` ENABLE KEYS */;

/*!40000 ALTER TABLE `regle` DISABLE KEYS */;
INSERT INTO `regle` (`REGL_ID`, `REGL_NOM`, `REGL_VALEUR`, `REGL_TYPE`) VALUES
	(1, 'bonus', '4', 'i'),
	(2, 'pointsMinimumBadge', '1', 'i');
/*!40000 ALTER TABLE `regle` ENABLE KEYS */;

/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`ROLE_ID`, `ROLE_NOM`, `ROLE_DESCRIPTION`) VALUES
	(6, 'Admin', 'Role administrateur par défaut'),
	(7, 'Etudiant', 'Role etudiant');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

/*!40000 ALTER TABLE `typeorga` DISABLE KEYS */;
INSERT INTO `typeorga` (`TYPEORGA_ID`, `TYPEORGA_NOM`) VALUES
	(1, 'Promotion');
/*!40000 ALTER TABLE `typeorga` ENABLE KEYS */;

/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` (`UTIL_ID`, `UTIL_PSEUDO`, `UTIL_EMAIL`, `UTIL_PRENOM`, `UTIL_NOM`, `UTIL_MOT_DE_PASSE`, `UTIL_URL_AVATAR`, `UTIL_POINT_DISPO`, `UTIL_POINT_BONUS_DISPO`, `ORGA_ID`, `UTIL_SALT`) VALUES
	(1, 'Admin-default', 'benoit.berenger@cesi.fr', 'Benoît', 'Bérenger', '$2y$13$NWQUGy2rC/mjstynqTREEebWxvhZSgP6Sr4PNtzKDQ1azDxibcaTy', NULL, 0, 3, 1, 'benoit.berenger@cesi.fr1549025318');
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;

/*!40000 ALTER TABLE `vote` DISABLE KEYS */;
/*!40000 ALTER TABLE `vote` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
