<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190121181342 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE DEFINIR_REGLE RENAME INDEX fk_definir_regle2 TO IDX_2E74DB8190F8D05B');
        $this->addSql('ALTER TABLE ROLE CHANGE ROLE_NOM ROLE_NOM VARCHAR(40) NOT NULL');
        $this->addSql('ALTER TABLE CONTENIR_AUTORISATION RENAME INDEX fk_contenir_autorisation2 TO IDX_D52E6D6A1A5D01DF');
        $this->addSql('ALTER TABLE OBTENIR_ROLE RENAME INDEX fk_obtenir_role2 TO IDX_C5C587EBC855967A');
        $this->addSql('ALTER TABLE UTILISATEUR ADD UTIL_SALT VARCHAR(256) NOT NULL, DROP api_token');
        $this->addSql('ALTER TABLE POSSEDER_BADGE DROP DATE, DROP VALIDE');
        $this->addSql('ALTER TABLE POSSEDER_BADGE RENAME INDEX fk_posseder_badge2 TO IDX_DA70A87716A6E4A2');
        $this->addSql('ALTER TABLE BADGE CHANGE COMP_ID COMP_ID INT DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9A48AAD4A058331 ON BADGE (COMP_ID)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_9F4B754016A6E4A2 ON COMPETENCE (BADG_ID)');
        $this->addSql('ALTER TABLE PROPOSER_COMPETENCE RENAME INDEX fk_proposer_competence2 TO IDX_645943AA90F8D05B');
        $this->addSql('ALTER TABLE VOTE ADD VOTE_TYPE INT NOT NULL, DROP VOTE_BONUS, CHANGE COMP_ID COMP_ID INT DEFAULT NULL, CHANGE UTIL_ID UTIL_ID INT DEFAULT NULL, CHANGE UTI_UTIL_ID UTI_UTIL_ID INT DEFAULT NULL, CHANGE VOTE_DATE VOTE_DATE DATE NOT NULL, CHANGE VOTE_VALIDE VOTE_VALIDE TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE ORGANISATION CHANGE ORGA_NOM ORGA_NOM VARCHAR(40) NOT NULL');
        $this->addSql('ALTER TABLE TYPEORGA CHANGE TYPEORGA_NOM TYPEORGA_NOM VARCHAR(40) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX UNIQ_9A48AAD4A058331 ON BADGE');
        $this->addSql('ALTER TABLE BADGE CHANGE COMP_ID COMP_ID INT NOT NULL');
        $this->addSql('DROP INDEX UNIQ_9F4B754016A6E4A2 ON COMPETENCE');
        $this->addSql('ALTER TABLE CONTENIR_AUTORISATION RENAME INDEX idx_d52e6d6a1a5d01df TO FK_CONTENIR_AUTORISATION2');
        $this->addSql('ALTER TABLE DEFINIR_REGLE RENAME INDEX idx_2e74db8190f8d05b TO FK_DEFINIR_REGLE2');
        $this->addSql('ALTER TABLE OBTENIR_ROLE RENAME INDEX idx_c5c587ebc855967a TO FK_OBTENIR_ROLE2');
        $this->addSql('ALTER TABLE ORGANISATION CHANGE ORGA_NOM ORGA_NOM VARCHAR(40) DEFAULT NULL COLLATE utf8mb4_general_ci');
        $this->addSql('ALTER TABLE POSSEDER_BADGE ADD DATE DATE DEFAULT NULL, ADD VALIDE TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE POSSEDER_BADGE RENAME INDEX idx_da70a87716a6e4a2 TO FK_POSSEDER_BADGE2');
        $this->addSql('ALTER TABLE PROPOSER_COMPETENCE RENAME INDEX idx_645943aa90f8d05b TO FK_PROPOSER_COMPETENCE2');
        $this->addSql('ALTER TABLE ROLE CHANGE ROLE_NOM ROLE_NOM VARCHAR(40) DEFAULT NULL COLLATE utf8mb4_general_ci');
        $this->addSql('ALTER TABLE TYPEORGA CHANGE TYPEORGA_NOM TYPEORGA_NOM VARCHAR(40) DEFAULT NULL COLLATE utf8mb4_general_ci');
        $this->addSql('ALTER TABLE UTILISATEUR ADD api_token VARCHAR(255) NOT NULL COLLATE utf8mb4_general_ci, DROP UTIL_SALT');
        $this->addSql('ALTER TABLE VOTE ADD VOTE_BONUS INT DEFAULT NULL, DROP VOTE_TYPE, CHANGE VOTE_DATE VOTE_DATE DATE DEFAULT NULL, CHANGE VOTE_VALIDE VOTE_VALIDE TINYINT(1) DEFAULT NULL, CHANGE UTIL_ID UTIL_ID INT NOT NULL, CHANGE UTI_UTIL_ID UTI_UTIL_ID INT NOT NULL, CHANGE COMP_ID COMP_ID INT NOT NULL');
    }
}
